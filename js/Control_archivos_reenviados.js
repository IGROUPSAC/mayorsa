var service = new Service("webService/index.php");


$(function() 
{

iniciarArchivosReenviados();

});

var arrayUsuarios = [];

function iniciarArchivosReenviados(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $("#menu_archivos_reenviados").on('click', function(){
      cargaConsultasIniciales();  
    })

    $("#boton_menu_archivos_reenviados").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){
        //service.procesar("listarArchivosPendientes",cargaListarArchivosPendientes);
        //service.procesar("getListaEstados",cargaListaEstados);
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
        cargarListaArchivosReenviadosTodos();
    }

    function cargarListaArchivosReenviadosTodos(){
        service.procesar("listarArchivosReenviados",cargaListarArchivosReenviadosTodos);
        service.procesar("listarArchivosReenviadosProcesados",cargaListarArchivosReenviadosProcesados);
    }

    $("#listarArchivosReenviados").on('click',function(){
        service.procesar("listarArchivosReenviados",cargaListarArchivosReenviados);
    })

    function cargaListarArchivosReenviados(evt){
        resultado = evt;

        $("#tablaListaArchivosReenviados tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnremover.data("data",datoRegistro);
            btnCargar.data("data",datoRegistro);

            btnremover.on("click",removerArchivosReenviados);
            btnCargar.on("click",cargarArchivosReenviados);

            contenedorBotones.append(btnremover);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnCargar);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosReenviados tbody").append(fila);

            service.procesar("cargarArchivoReenviadoMasivo",datoRegistro.nombre,mensajeCargarArchivoReenviadoMasivo);

        }

    }


    function cargaListarArchivosReenviadosTodos(evt){
        resultado = evt;

        $("#tablaListaArchivosReenviados tbody").html("");

        //LLENADO DE INFORMACION EN EL AREA DE AVANCE
        //service.procesar("getListaAreaRango",resultadoAvanceCapturaInicial);

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnremover.data("data",datoRegistro);
            btnCargar.data("data",datoRegistro);

            btnremover.on("click",removerArchivosReenviados);
            btnCargar.on("click",cargarArchivosReenviados);

            contenedorBotones.append(btnremover);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnCargar);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosReenviados tbody").append(fila);

        }

    }



    function cargaListarArchivosReenviadosProcesados(evt){
        resultado = evt;

        $("#tablaListaArchivosReenviadosCargados tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].archivo +'</td><td>'+ resultado[i].registros +'</td><td>'+ resultado[i].contado +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnmostrar = $("<button class='btn btn-success btn-sm' title='Mostrar'>");
            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            //var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnmostrar.html('<span class="glyphicon glyphicon-eye-open"></span> MOSTRAR');
            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            //btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnmostrar.data("data",datoRegistro);
            btnremover.data("data",datoRegistro);
            //btnCargar.data("data",datoRegistro);

            btnmostrar.on("click",mostrarArchivosReenviadosCargados);
            btnremover.on("click",removerArchivosReenviadosCargados);
            //btnCargar.on("click",cargarArchivosReenviados);

            contenedorBotones.append(btnmostrar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnremover);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosReenviadosCargados tbody").append(fila);

        }

    }

    function mostrarArchivosReenviadosCargados(){
        var data = $(this).data("data");
        service.procesar("mostrarArchivosReenviadosCargados",data.archivo,mensajeArchivosReenviadosCargados);
    }

    function mensajeArchivosReenviadosCargados(evt){
        $("#modal_registros_archivo_seleccionado").modal("show");
        var clase = '';

        resultado = evt;
        $("#tablaRegistrosArchivoSeleccionado tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            clase = '';

            if(resultado[i].estado == 1){
                clase = "verde";
            }

            var fila = $("<tr class='"+clase+"'>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].area_cap +'</td><td>'+ resultado[i].usuario +'</td><td>'+ resultado[i].registros_reenviado +'</td><td>'+ resultado[i].contado_reenviado +'</td><td>'+ resultado[i].registros_captura +'</td><td>'+ resultado[i].contado_captura +'</td><td>'+ resultado[i].diferencia +'</td><td>'+ corregirNullString(resultado[i].estado_captura) +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnreemplazar = $("<button class='btn btn-info btn-sm' title='Reemplazar'>");
            //var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            //var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnreemplazar.html('<span class="glyphicon glyphicon-eye-open"></span> REGISTRAR');
            //btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            //btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnreemplazar.data("data",datoRegistro);
            //btnremover.data("data",datoRegistro);
            //btnCargar.data("data",datoRegistro);

            btnreemplazar.on("click",reemplazarArchivosReenviadosCargados);
            //btnremover.on("click",removerArchivosReenviadosCargados);
            //btnCargar.on("click",cargarArchivosReenviados);

            contenedorBotones.append(btnreemplazar);
            //contenedorBotones.append(" ");
            //contenedorBotones.append(btnremover);

            fila.append(contenedorBotones);

            $("#tablaRegistrosArchivoSeleccionado tbody").append(fila);

        }
    }

    function reemplazarArchivosReenviadosCargados(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE CARGAR LOS REGISTROS DE AREA "+ data.area_cap +" QUE SERAN REEMPLAZADOS EN LA TABLA CAPTURA ?", function (e) {
            if (e) {
                var objeto = new Object()
                    objeto.area_cap = data.area_cap;
                    objeto.descargado = data.descargado;
                    objeto.usuario = data.usuario;
                service.procesar("reemplazaRegistrosCaptura",objeto,mensajeRARC);
            }
        });
    }

    function mensajeRARC(evt){
        alertify.success("REGISTROS PROCESADOS");
        service.procesar("mostrarArchivosReenviadosCargados",evt,mensajeArchivosReenviadosCargados);
    }


    function removerArchivosReenviadosCargados(){
        var data = $(this).data("data");

    }


    function mensajeCargarArchivoReenviadoMasivo(evt){
        resultado = evt.registros;
        nombre = evt.nombre;

        if(resultado > 0){
            alertify.success("ARCHIVO CARGADO SATISFACTORIAMENTE");
            service.procesar("listarArchivosReenviados",cargaListarArchivosReenviadosMasivo);
            service.procesar("listarArchivosReenviadosProcesados",cargaListarArchivosReenviadosProcesados);
        }else{
            alertify.error("ARCHIVO NO CARGADO, INTENTARLO NUEVAMENTE");
        }

    }

    function cargaListarArchivosReenviadosMasivo(evt){
        resultado = evt;
        console.log(evt.length);

        if(evt.length > 0){
            $("#listarArchivosReenviados").show();
            $("#listarArchivosReenviados").html("( "+evt+" ) ARCHIVOS REENVIADOS PENDIENTE DE CARGA");
        }else{
            $("#listarArchivosReenviados").hide();
        }


        $("#tablaListaArchivosReenviados tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnremover = $("<button class='btn btn-danger btn-sm' title='Remover'>");
            var btnCargar = $("<button class='btn btn-success btn-sm' title='Cargar'>");

            btnremover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnCargar.html('<span class="glyphicon glyphicon-download-alt"></span> CARGAR');

            btnremover.data("data",datoRegistro);
            btnCargar.data("data",datoRegistro);

            btnremover.on("click",removerArchivosReenviados);
            btnCargar.on("click",cargarArchivosReenviados);

            contenedorBotones.append(btnremover);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnCargar);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosReenviados tbody").append(fila);

        }

    }

    function removerArchivosReenviados(){
        var data = $(this).data("data");
        nombre = data.nombre;
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ARCHIVO : " + nombre +" ?", function (e) {
            if (e) {
                service.procesar("eliminarArchivoReenviado",nombre,mensajeCopiarArchivoReenviado);
            }
        });
       
    }

    function cargarArchivosReenviados(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE CARGAR EL ARCHIVO : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("cargarArchivoReenviado",data.nombre,mensajeCargarArchivoReenviado);
            }
        });

    }

    function mensajeCargarArchivoReenviado(evt){
        resultado = evt.registros;
        nombre = evt.nombre;

        if(parseFloat(resultado) > 0){
            alertify.success("ARCHIVO CARGADO SATISFACTORIAMENTE");
            service.procesar("copiarArchivoReenviado",nombre,mensajeCopiarArchivoReenviado);
            //backupBaseDatos();
        }else{
            alertify.error("ARCHIVO NO CARGADO, INTENTARLO NUEVAMENTE");
        }
    }

    function mensajeCopiarArchivoReenviado(evt){
        if(evt > 0){
            $("#listarArchivosReenviados").show();
            $("#listarArchivosReenviados").html("( "+evt+" ) ARCHIVOS REENVIADOS PENDIENTES DE CARGA");
            service.procesar("listarArchivosReenviados",cargaListarArchivosReenviadosTodos);
        }else{
            $("#listarArchivosReenviados").hide();
            service.procesar("listarArchivosReenviados",cargaListarArchivosReenviadosTodos);
        }

    }

    function openModalGenerarArchivo(archivo){
        $("#modal_cerrar_inventario_progress").modal('show');
        $("#modal_cerrar_inventario_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_cerrar_inventario_progress").modal('hide');
        alertify.success("ARCHIVO GENERADO SATISFACTORIAMENTE");
    }





    function corregirNullString(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

}


