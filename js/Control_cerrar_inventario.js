var service = new Service("webService/index.php");


$(function() 
{

iniciarControlCerrarInventario();

});


function iniciarControlCerrarInventario(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var archivo = $("#archivoBackup");

    

    $("#cerrar_inventario .backup").on('click', function(){
            alertify.confirm("¿ SEGURO RESPALDAR LA INFORMACION DE LA BASE DE DATOS ?", function (e) {
                if (e) {
                    service.procesar("comprobarDispositivo",function(evt){
                        if(evt == 1){
                            alertify.success("DISPOSITIVO EN UNIDAD K: ENCONTRADO");
                            //SI RUTA FUE ENCONTRADA
                            var tablas = "area_rango,asistencia,auditoria,captura,huellas,usuario,justificacion,log,permisos,tienda,tipousuario,reporte,reportecuestionario,reportegrupo,reportepregunta,reporterespuesta,respuestasinforme,respuestaspreguntas";
                            //var tablas = "area_rango,tienda,diferencias";
                            //var tablas = "*";
                            openModalGenerarArchivo("BACKUP",tablas);
                            service.procesar("backupBaseDatos",function(evt){
                                alertify.success("BACKUP REALIZADA SATISFACTORIAMENTE");
                                closeModalGenerarArchivo("BACKUP GENERADO");
                            });

                        }else{
                            alertify.error("DISPOSITIVO EN UNIDAD K: NO ENCONTRADO");

                            alertify.confirm("¿ GUARDAR BACKUP EN UNIDAD LOCAL ?", function (e) {
                                if (e) {

                                    var tablas = "area_rango,asistencia,auditoria,captura,huellas,usuario,justificacion,log,permisos,tienda,tipousuario,reporte,reportecuestionario,reportegrupo,reportepregunta,reporterespuesta,respuestasinforme,respuestaspreguntas";
                                    //var tablas = "area_rango,tienda,diferencias";
                                    //var tablas = "*";
                                    openModalGenerarArchivo("BACKUP",tablas);
                                    service.procesar("backupBaseDatos",function(evt){
                                        alertify.success("BACKUP REALIZADA SATISFACTORIAMENTE");
                                        closeModalGenerarArchivo("BACKUP GENERADO");
                                    });

                                } else {
                                    alertify.error("HA CANCELADO EL PROCESO");
                                }
                            });
                            
                        }

                    });


                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });

    })

    $("#cerrar_inventario .restore").on('click', function(){
        $("#modalRestaurarInventario").modal("show");
    })

    $("#modalRestaurarInventario .submit-carga-backup").on('click', function(){
            alertify.confirm("¿ SEGURO RESTAURAR LA INFORMACION DE LA BASE DE DATOS ?", function (e) {
                if (e) {
                    $("#modalRestaurarInventario").modal("hide");
                    var valorArchivo = archivo[0].files[0];
                    subirArchivo(valorArchivo,0,0); 

                    openModalGenerarArchivo("BACKUP","");


                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
    })


    function subirArchivo(archivo,datos,funcion){
        var data = new FormData();
        data.append("file",archivo);
        $.ajax({
            type: "POST",
            contentType: false,
            url: "webService/guardarArchivoBackup.php",
            data: data,
            cache: false,
            processData: false,
            dataType:"json",
            success: function(evt){
                datos.link = evt.nombre;
                alertify.success("ARCHIVO CARGADO AL SERVIDOR");
                if(evt.response == 1){
                    //alertify.success("BACKUP REALIZADA SATISFACTORIAMENTE");
                    //$("#modalRestaurarInventario").modal("hide");
                    limpiarCargaBackup();
                    closeModalGenerarArchivo("BACKUP GENERADO");
                    window.location='index.php';
                }else{
                    //alertify.error("BACKUP NO REALIZADA");
                    closeModalGenerarArchivo("BACKUP NO GENERADO");
                }        

            },
            error: function(evt){
                alertify.error("ARCHIVO NO CARGADO");
            }
        });
    }







    $(".boton-carga-backup").on('click',function(){
        archivo.trigger("click");
        archivo.change(function(){
            $(".campo-carga-backup").val( (archivo.val()).split('\\').pop() );
            $(".submit-carga-backup").prop("disabled",false);
        });
        

    });

    $(".boton-limpia-backup").on('click',function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR LA INFORMACION ?", function (e) {
            if (e) {
                limpiarCargaBackup();
            }
        });
    })


    function limpiarCargaBackup(){
        archivo.val(null);
        $(".campo-carga-backup").val("");
        $(".submit-carga-backup").prop("disabled",true);
    }






























    $("#cerrar_inventario .nuevo").on('click', function(){

        $("#modalCerrarInventario").modal("show");
            /*
            alertify.confirm("¿ SEGURO DE ELIMINAR TODOS LOS REGISTROS DE LAS TABLAS EXCEPTO USUARIO, PERMISOS Y HUELLAS ?", function (e) {
                if (e) {
                    var tablas = "area_rango,auditoria,barra,captura,capturas,diferencias,justificacion,log,maestro,stock,stock_total,tienda,asistencia,gondola";
                    service.procesar("truncateBaseDatos",tablas,function(evt){
                        alertify.success("BASE DE DATOS LISTA PARA NUEVO INVENTARIO");
                    });
                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
            */

    })

    $("#modalCerrarInventario .si").on('click', function(){
        var tablas = "area_rango,auditoria,captura_reenviado,captura,capturas,diferencias,justificacion,log,maestro,stock,stock_total,tienda,asistencia,gondola";
        //var tablas = "area_rango,tienda,diferencias";
            service.procesar("truncateBaseDatos",tablas,function(evt){
                alertify.success("BASE DE DATOS LISTA PARA NUEVO INVENTARIO");
                $("#modalCerrarInventario").modal("hide");
            });
    })

    function openModalGenerarArchivo(mensaje,archivo){
        $("#modal_cerrar_inventario_progress").modal('show');
        $("#modal_cerrar_inventario_progress .descripcionarchivogenerado").html(mensaje+" ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(mensaje){
        $("#modal_cerrar_inventario_progress").modal('hide');
        alertify.success("ARCHIVO "+mensaje);
    }






    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


