var service = new Service("webService/index.php");


$(function() 
{

iniciarControlCaptura();

});

var arrayCapturas = [];

function iniciarControlCaptura(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    //$('#fechaFormularioUsuario').val(fecha);

    




    $("#menu_capturas").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_capturas").on('click', function(){
      cargaConsultasIniciales();  
    })


    function cargaConsultasIniciales(){

        $(".opcionesCaptura").css("display","none");

    }

    
    /*function cargaListaAreaCap(evt){
        resultado = evt;
        $("#areaCapFormularioCaptura").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#areaCapFormularioCaptura" ).append( "<option value='"+ resultado[i].area_cap +"'>"+ resultado[i].area_cap +"</option>" );
        }
    }*/

    function aplicarFiltroCaptura(){
        var area_captura = $("#areaCapFormularioCaptura").val();
        service.procesar("getListaCapturas",area_captura,cargaListaCapturas);
    }

    $('#botonBuscarFormularioCaptura').on('click',function(){
        if($('#areaCapFormularioCaptura').val() != ""){
            aplicarFiltroCaptura();    
        }else{
            alertify.error("INGRESE VALOR A CAMPO LOTE");
        }
        
    });





    function cargaListaCapturas(evt){
        
        listadeCapturas = evt.registros;


        $("#tablaResultadoFormularioCapturas thead").html("<tr><th>#</th><th>AREA_CAP</th><th>BARRA_CAP</th><th>DES_BARRA</th><th>CANT_CAP</th><th>TIP_CAP</th><th>USUARIO</th><th>DESCARGADO</th><th>NAMEPALM</th><th>MARCAR</th><th>OPCIONES</th></tr>");
        $("#tablaResultadoFormularioCapturas tbody").html("");
        $(".areaBusquedaAreaCapFormularioCaptura").css("display","none");
        $("#nombreAreaRango").html("");

        //LLENADO DE INFORMACION EN EL AREA DE AVANCE
        //service.procesar("getListaAreaRango",resultadoAvanceCapturaInicial);

        if ( listadeCapturas == undefined ) return;

        $("#nombreAreaRango").html(evt.areaRango);
        //console.log(evt.areaRango);

        $("#tablaResultadoFormularioCapturas thead").html("<tr><th># ( "+listadeCapturas.length+" )</th><th>AREA_CAP</th><th>BARRA_CAP ( "+evt.barras[0].cont_barra+" )</th><th>DES_BARRA</th><th>CANT_CAP ( "+parseFloat(evt.cantidad[0].sum_cant)+" )</th><th>TIP_CAP</th><th>USUARIO</th><th>DESCARGADO</th><th>NAMEPALM</th><th>MARCAR</th><th>OPCIONES</th></tr>");
        $(".areaBusquedaAreaCapFormularioCaptura").css("display","");
        //$("#registrosFormularioCaptura").html(); SE OCULTO POR NO SER USADO


        for(var i=0; i<listadeCapturas.length ; i++){

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = listadeCapturas[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeCapturas[i].area_cap +'</td><td>'+ listadeCapturas[i].barra_cap +'</td><td>'+ corregirNull(listadeCapturas[i].des_barra) +'</td><td>'+ listadeCapturas[i].cant_cap +'</td><td>'+ listadeCapturas[i].tip_cap +'</td><td>'+ corregirNull(listadeCapturas[i].nombreUsuario) +'</td><td>'+ listadeCapturas[i].descargado +'</td><td>'+ listadeCapturas[i].namepalm +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioCaptura);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            var contenedorBotones = $("<td>");

            

            var btnSeleccionar = $("<button class='btn btn-primary btn-sm botonesCaptura' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarFormularioCaptura);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesCaptura' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioCaptura);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaResultadoFormularioCapturas tbody").append(fila);

        }

    }

    function seleccionarFormularioCaptura(){
        var data = $(this).data("data");
        var id_captura = data.id_captura;
        var valor = $.inArray(id_captura, arrayCapturas);

        if(valor >= 0){
        	removeItemFromArr( arrayCapturas, id_captura );
        }else{
        	arrayCapturas.push(id_captura);
        }
        
        var contar = arrayCapturas.length;

        if(contar > 1){
        	$("#botonCancelarCapturas").css("display","");
            //$("#botonGuardarCapturas").html("MODIFICAR SELECCIONADOS");
            $("#botonCancelarCapturas").html("ELIMINAR SELECCIONADOS");

            $(".botonesCaptura").prop( "disabled", true );

            $("#codigoFormularioCaptura").val("");
            $("#barraCapFormularioCaptura").val("");
            $("#cantCapFormularioCaptura").val("");
        }else{
        	$(".opcionesCaptura").css("display","none");
            $("#botonGuardarCapturas").html("MODIFICAR");
            $("#botonCancelarCapturas").html("CANCELAR");

            $(".botonesCaptura").prop( "disabled", false );
        }
    }

    function editarFormularioCaptura(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL REGISTRO : " + data.id_captura +" ?", function (e) {
            if (e) {
                $("#codigoFormularioCaptura").val(data.id_captura);
                $("#barraCapFormularioCaptura").val(data.barra_cap);
                $("#cantCapFormularioCaptura").val(data.cant_cap);

                $("#botonGuardarCapturas").html("MODIFICAR");
                $("#botonCancelarCapturas").html("CANCELAR");

                $(".opcionesCaptura").css("display","");


                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarFormularioCaptura(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL REGISTRO : " + data.id_captura +" ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioCaptura").val();
                var objetoFormulario = new Object()
                    objetoFormulario.id_captura = data.id_captura;
                    objetoFormulario.responsable = responsable;

                service.procesar("deleteFormularioCaptura",objetoFormulario,resultadoDeleteFormularioCaptura);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioCaptura(evt){
        if(evt == 1){
            alertify.success("REGISTRO ELIMINADO SATISFACTORIAMENTE");
            var area_cap = $("#areaCapFormularioCaptura").val();
            service.procesar("getListaCapturas",area_cap,cargaListaCapturas);
            service.procesar("resolverConflictoBarras",resultadoResolverConflictoBarras);            
        }else{
            alertify.error("REGISTRO NO ELIMINADO");
        }

    }

    $("#botonGuardarCapturas").on('click', function(){ //PENDIENTES

        var procedimiento = $("#botonGuardarCapturas").html(); //TITULO DEL BOTON

        if(procedimiento == "MODIFICAR"){

            var id_captura = $("#codigoFormularioCaptura").val();
            var responsable = $("select#responsableFormularioCaptura").val();
            var barra_cap = $("#barraCapFormularioCaptura").val();
            var cant_cap  = $("#cantCapFormularioCaptura").val();

            if(id_captura != "" && barra_cap != "" && cant_cap != ""){

                alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                    if (e) {

                        var objetoFormulario = new Object()
                            objetoFormulario.procedimiento = procedimiento;
                            objetoFormulario.id_captura = id_captura;
                            objetoFormulario.responsable = responsable;
                            objetoFormulario.barra_cap = barra_cap;
                            objetoFormulario.cant_cap = cant_cap;
                            objetoFormulario.usuario = usuario;

                        service.procesar("saveFormularioCaptura",objetoFormulario,cargaFormularioCaptura);
                        alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                    } else {
                        alertify.error("HA CANCELADO EL PROCESO");
                    }
                });

            }else{
                alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
            }

        }else{

            //guardarMasivoFormularioCaptura();

        }





    })

    $("#botonCancelarCapturas").on('click',function(){
        var procedimiento = $("#botonCancelarCapturas").html();

        if(procedimiento == "CANCELAR"){
            alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
                if (e) {
    		        alertify.success("PROCESO CANCELADO");
                    $(".opcionesCaptura").css("display","none");
    		        formularioCapturaEstandar();
                }
            });
        }else{
            eliminarMasivoFormularioCaptura();
        }

    })

    //$("#botonGuardarEstadoMasivoFormularioCaptura").on('click',function(){
    /*function guardarMasivoFormularioCaptura(){
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS MASIVOS ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioCaptura").val();
                var objetoFormulario = new Object()
                    objetoFormulario.estado = $("select#estadoMasivoFormularioCaptura").val();
                    objetoFormulario.responsable = responsable;
                    objetoFormulario.barra_cap = $("#barraCapFormularioCaptura").val();
                    objetoFormulario.cant_cap = $("#cantCapFormularioCaptura").val();
                    objetoFormulario.capturas = arrayCapturas;
                    objetoFormulario.usuario = usuario;

                service.procesar("modificarMasivoFormularioCaptura",objetoFormulario,cargaModificarMasivoFormularioCaptura);

                alertify.success("HA ACEPTADO MODIFICAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }*/

    //$("#botonEliminarMasivoFormularioCaptura").on('click',function(){
    function eliminarMasivoFormularioCaptura(){
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS MASIVOS ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioCaptura").val();
                var objetoFormulario = new Object()
                    objetoFormulario.capturas = arrayCapturas;
                    objetoFormulario.responsable = responsable;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarMasivoFormularioCaptura",objetoFormulario,cargaEliminarMasivoFormularioCaptura);

                alertify.success("HA ACEPTADO ELIMINAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function cargaEliminarMasivoFormularioCaptura(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS ELIMINADOS SATISFACTORIAMENTE");
            //$(".opciones").css("display","");
        	//$(".opcionesmasivo").css("display","none");
        	arrayCapturas.length=0;
            $(".opcionesCaptura").css("display","none");
            var area_cap = $("#areaCapFormularioCaptura").val();
            service.procesar("getListaCapturas",area_cap,cargaListaCapturas);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    /*    if(contar > 1){
            $(".opcionesCaptura").css("display","none");
            $(".botonesCaptura").prop( "disabled", true );
        }else{
            $(".opcionesCaptura").css("display","");
            $(".botonesCaptura").prop( "disabled", false );
        }
    */
   
    function cargaModificarMasivoFormularioCaptura(evt){
    	if(evt == 1 ){
            alertify.success("REGISTROS GUARDADOS SATISFACTORIAMENTE");


            //$(".opcionesCaptura").css("display","");
            //$(".botonesCaptura").prop( "disabled", true );

            $("#codigoFormularioCaptura").val("");
            $("#barraCapFormularioCaptura").val("");
            $("#cantCapFormularioCaptura").val("");

        	arrayCapturas.length=0;
            $(".opcionesCaptura").css("display","none");
            service.procesar("getListaCapturas",cargaListaCapturas);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaFormularioCaptura(evt){
        var area_captura = $("#areaCapFormularioCaptura").val();        
        if(evt == 1 ){
            alertify.success("REGISTRO GUARDADO SATISFACTORIAMENTE");
            service.procesar("getListaCapturas",area_captura,cargaListaCapturas);
            formularioCapturaEstandar();
        }else if(evt == 2 ){
            alertify.success("REGISTRO MODIFICADO SATISFACTORIAMENTE");
            service.procesar("getListaCapturas",area_captura,cargaListaCapturas);
            service.procesar("resolverConflictoBarras",resultadoResolverConflictoBarras);
            $(".opcionesCaptura").css("display","none");
            formularioCapturaEstandar();
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }
    }

    function resultadoResolverConflictoBarras(evt){
        if(evt > 0){
            $("#listarConflictoBarras").css('display','');
        }else{
            $("#listarConflictoBarras").css('display','none');
        }
    }

    function formularioCapturaEstandar(){
        $("#barraCapFormularioCaptura").val("");
        $("#cantCapFormularioCaptura").val("");
        $("#codigoFormularioCaptura").val("");
        $("select#areaCapFormularioCaptura").val(0);
        $("#botonGuardarCapturas").html("GUARDAR");
    }







    $("#botonListarImprimirDetalladaCapturas").on('click',function(){
        var area_cap = $("#areaCapFormularioCaptura").val();
        $('#modal_imprimir_consolidada_capturas').modal('show');
        $("#modal_imprimir_consolidada_capturas input").val("Detallada");
        $("#modal_imprimir_consolidada_capturas textarea").val(area_cap);
    })

    $("#botonListarImprimirConsolidadaCapturas").on('click',function(){
        var area_cap = $("#areaCapFormularioCaptura").val();
        $('#modal_imprimir_consolidada_capturas').modal('show');
        $("#modal_imprimir_consolidada_capturas input").val("Consolidada");
        $("#modal_imprimir_consolidada_capturas textarea").val(area_cap);
        //window.open("reportes/reporteConsolidadaCapturasxArea.php?area_cap="+area_cap);
    })

    $("#botonListarImprimirCapturas").on('click',function(){
        var tipo = $("#modal_imprimir_consolidada_capturas input").val();
        var areas = $("#modal_imprimir_consolidada_capturas textarea").val();

        window.open("reportes/reporte"+tipo+"CapturasxArea.php?area_cap="+areas);

           

    })

    $("#modal_imprimir_consolidada_capturas .exportarCapturas").on('click',function(){
        var tipo = $("#modal_imprimir_consolidada_capturas input").val();
        var areas = $("#modal_imprimir_consolidada_capturas textarea").val();
        service.procesar("getDescargaReporteCaptura"+tipo,areas,function(evt){
            document.location = "webService/reportes/reportedeCaptura"+tipo+".xlsx"
        });

    })



    $("#botonEliminarAreaFormularioCaptura").on('click',function(){

        alertify.confirm("¿ SEGURO DE ELIMINAR LOTE ?", function (e) {
            if (e) {
                var responsable = $("select#responsableFormularioCaptura").val();
                var area_cap = $("#areaCapFormularioCaptura").val();
                var objetoFormulario = new Object()
                    objetoFormulario.area_cap = area_cap;
                    objetoFormulario.responsable = responsable;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarAreaCapFormularioCaptura",objetoFormulario,resultadoEliminarAreaCapFormularioCaptura);

                alertify.success("HA ACEPTADO ELIMINAR LOTE");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });

    })


    function resultadoEliminarAreaCapFormularioCaptura(evt){
        if($('#areaCapFormularioCaptura').val() != ""){
            aplicarFiltroCaptura();    
        }else{
            alertify.error("INGRESE VALOR A CAMPO LOTE");
        }
    }





    //LLENADO DE INFORMACION EN EL AREA DE AVANCE

    function resultadoAvanceCapturaInicial(evt){
        listadeAreaRango = corregirNullArreglo(evt.rangos);
        listaCapturas = corregirNullArreglo(evt.capturas);
        listaJustificados = corregirNullArreglo(evt.justificados);

        var cantidad = 0;
        var avance = 0;
        var porcentaje = 0;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);

            for (var z=filaInicio; z<=filaFinal; z++) {
                cantidad++;
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                    }
                }
            }

        }

        porcentaje = ((parseFloat(avance) / parseFloat(cantidad))*100).toFixed(2);

        $(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
        $(".progressAvanceCaptura").html(porcentaje+' %');      
    }


    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

    function corregirNull(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }


}


