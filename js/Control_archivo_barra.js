var service = new Service("webService/index.php");


$(function() 
{

iniciarControlArchivoBarra();

});

var arrayCapturas = [];

function iniciarControlArchivoBarra(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");

    var archivoBarra = $("#archivoBarra");
    //$('#fechaFormularioUsuario').val(fecha);

    




    $("#menu_archivo_barra").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_archivo_barra").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){
        service.procesar("listarArchivosBarraPendientes",cargaListarArchivosBarraPendientes);
        //service.procesar("getListaAreaCap",cargaListaAreaCap);

        //$(".opcionesCaptura").css("display","none");
        //$(".opcionesMasivoCaptura").css("display","none");
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    /*
    $("#fileCargaBodega").on("click",function(){
        $("#inputCargaBodega").trigger("click")
        alertify.success("PESO MAXIMO 2MB");
    })
    */


    $(".boton-carga-barra").on('click',function(){
        archivoBarra.trigger("click");
        archivoBarra.change(function(){
            $(".campo-carga-barra").val( (archivoBarra.val()).split('\\').pop() );
            $(".submit-carga-barra").prop("disabled",false);
        });
        

    });

    $(".boton-limpia-barra").on('click',function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR LA INFORMACION ?", function (e) {
            if (e) {
                limpiarCargaBarra();
            }
        });
    })

    $(".submit-carga-barra").on('click',function(){
        var lote = $(".campo-carga-lote").val();
        if(lote > 0 || lote != ""){
            alertify.confirm("¿ SEGURO DE GUARDAR INFORMACION DEL ARCHIVO ?", function (e) {
                if (e) {
                    //console.log(archivo[0].files[0].name);
                    var valorArchivo = archivoBarra[0].files[0];
                    subirArchivo(valorArchivo,0,0);            
                }
            });
        }else{
            alertify.error("INGRESAR LOTE");
        }

    })

    function limpiarCargaBarra(){
        archivoBarra.val(null);
        $(".campo-carga-barra").val("");
        $(".campo-carga-lote").val("");
        $(".submit-carga-barra").prop("disabled",true);
    }







    function subirArchivo(archivoBarra,datos,funcion){
        var data = new FormData();
        data.append("file",archivoBarra);
        $.ajax({
            type: "POST",
            contentType: false,
            url: "webService/guardarArchivoBarra.php",
            data: data,
            cache: false,
            processData: false,
            dataType:"json",
            success: function(evt){
                datos.link = evt.nombre;
                //funcion();
                //console.log("ARCHIVO GUARDADO...",evt.nombre);
                //service.procesar("listarArchivosMaestroPendientes",cargaListarArchivosMaestroPendientes);
                var lote = $(".campo-carga-lote").val();
                alertify.success("ARCHIVO CARGADO AL SERVIDOR");
                $(".progresoCargandoBarra").css("display","");

                var objDatos = new Object()
                    objDatos.archivo = evt.nombre;
                    objDatos.lote = lote;
                service.procesar("saveRegistrosBarra",objDatos,mensajeCargaBarra);

            },
            error: function(evt){
                alertify.error("ARCHIVO NO CARGADO");
            }
        });
    }

    function mensajeCargaBarra(evt){
        $(".progresoCargandoBarra").css("display","none");
        limpiarCargaBarra();
        service.procesar("listarArchivosBodegaPendientes",cargaListarArchivosBarraPendientes);
        alertify.success("INFORMACION PROCESADA SATISFACTORIAMENTE");
    }

    function cargaListarArchivosBarraPendientes(evt){
        resultado = evt;

        $("#tablaListaArchivosBarraPendientes tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnInfo = $("<button class='btn btn-info btn-sm' title='VER LOG'>");
            var btnRemover = $("<button class='btn btn-danger btn-sm' title='EIMINAR'>");

            btnInfo.html('<span class="glyphicon glyphicon-eye-open"></span> ARCHIVO LOG');
            btnRemover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR ARCHIVO');

            btnInfo.data("data",datoRegistro);
            btnRemover.data("data",datoRegistro);

            btnInfo.on("click",infoArchivosBarraPendientes);
            btnRemover.on("click",removerArchivosBarraPendientes);

            contenedorBotones.append(btnInfo);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnRemover);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosBarraPendientes tbody").append(fila);

        }

    }

    function infoArchivosBarraPendientes(){
        var data = $(this).data("data");
        window.open("archivos_sistema/archivos_barra/"+data.log, '_blank');
    }

    function removerArchivosBarraPendientes(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ARCHIVO : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("eliminarArchivoBarraPendiente",data.nombre,mensajeEliminarArchivoBarraPendiente);
            }
        });
    }

    function mensajeEliminarArchivoBarraPendiente(evt){
        if(evt > 0){
            service.procesar("listarArchivosBarraPendientes",cargaListarArchivosBarraPendientes);
            alertify.success("REGISTRO ELIMINADO CORRECTAMENTE");
        }else{
            alertify.error("REGISTRO NO ELIMINADO");
        }

    }
















    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


