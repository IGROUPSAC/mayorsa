var service = new Service("webService/index.php");


$(function() 
{

iniciarControlGenerarCierre();

});

var arrayTiendas = [];

var porcentaje = 0.00;

function iniciarControlGenerarCierre(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioTienda').val(fecha);

    



    $("#menu_generar_cierre").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_generar_cierre").on('click', function(){
      cargaConsultasIniciales();  
    })   


    function cargaConsultasIniciales(){
        service.procesar("getListaJerarquias",cargaListaJerarquias);
        //service.procesar("getListaEstados",cargaListaEstados);
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    function cargaListaJerarquias(evt){
        resultado = evt;
        var largo = 0;
        var porcentaje = 0;
        $("#generar_cierre .listadojerarquias").html("<option value='0'>---NINGUNO---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            largo = (resultado[i].porcentaje).length;
            if(largo == 4){
                porcentaje = '0'+resultado[i].porcentaje;
            }else{
                porcentaje = resultado[i].porcentaje;
            }
            $("#generar_cierre .listadojerarquias").append( "<option value='"+ resultado[i].dep_stk +"'>"+ resultado[i].dep_stk + ' - [ ' + porcentaje + ' % ] ' + resultado[i].jerarquia +"</option>" );
        }
    }

    $("#generar_cierre .listadojerarquias").change(function(){
        var jera = $("#generar_cierre .listadojerarquias").val();
        if(jera != '0'){
            service.procesar("getListaSubJerarquias",jera,cargaListaSubJerarquias);
        }else{
            $("#generar_cierre .listadosubjerarquias").html("<option value='0'>---NINGUNO---</option>");
        }
    })

    function cargaListaSubJerarquias(evt){
        resultado = evt;
        var largo = 0;
        var porcentaje = 0;
        $("#generar_cierre .listadosubjerarquias").html("<option value='0'>---NINGUNO---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            largo = (resultado[i].porcentaje).length;
            if(largo == 4){
                porcentaje = '0'+resultado[i].porcentaje;
            }else{
                porcentaje = resultado[i].porcentaje;
            }
            $("#generar_cierre .listadosubjerarquias").append( "<option value='"+ resultado[i].sub_dep_stk +"'>"+ resultado[i].sub_dep_stk + ' - [ ' + porcentaje + ' % ] ' + resultado[i].subjerarquia +"</option>" );
        }
    }

/*
    $("#generar_cierre .generarcierre").on('click', function(){

        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO DE CIERRE ?", function (e) {
            if (e) {
                //$("#modal_generar_cierre_progress").modal('show');
                openModalGenerarArchivo("CIERRE");
                service.procesar("generarCierre",function(evt){
                    $("#modal_generar_cierre_progress").modal('hide');
                    if(evt == 1){
                        alertify.success("DIFERENCIA GENERADA SATISFACTORIAMENTE");
                        service.procesar("getListaJerarquias",cargaListaJerarquias);
                    }else{
                        alertify.error("DIFERENCIA NO GENERADA, REVISAR CONFLICTOS");
                    }
                });
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });

    })
*/

    $("#generar_cierre .dercargarreportejerarquico").on('click', function(){

        window.open("reportes/reporteDiferenciaJerarquia.php");

    })

     $("#generar_cierre .dercargarreportesubjerarquico").on('click', function(){

        window.open("reportes/ReporteDiferenciaSubJerarquia.php");

    })



    $("#generar_cierre .dercargarreportegrupos").on('click', function(){

        $("#modal_reporte_grupos").modal("show");

        service.procesar("getRegistrosDiferenciaGrupos",function(evt){

            var resultado = evt;
            $("#modal_reporte_grupos .tablagrupos tbody").html("");

            if ( resultado == undefined ) return;

            for(var i=0; i<resultado.length ; i++){

                var fila = $("<tr>");

                fila.html('<td>'+ (i + 1) +'</td><td class="idGrupo">'+ resultado[i].idGrupo +'</td>');

                fila.append("<td><input class='form-control input-sm jerarquias' value='"+corregirNull(resultado[i].jerarquias)+"'></td>");

                fila.append("<td><input class='form-control input-sm division' value='"+corregirNull(resultado[i].division)+"'></td>");

                $("#modal_reporte_grupos .tablagrupos tbody").append(fila);

            }

        })

    })

    $("#modal_reporte_grupos .guardarformulario").on('click', function(){
        var filas = $(".tablagrupos tr").length;
        for(var i=0; i<(filas-1) ; i++){
            var idGrupo = document.getElementsByClassName('idGrupo')[i].innerHTML
            var jerarquias = document.getElementsByClassName('jerarquias')[i].value;
            var division = document.getElementsByClassName('division')[i].value;

            //if(jerarquias != "" && division != ""){
                var objetoFormulario = new Object()
                    objetoFormulario.idGrupo = idGrupo;
                    objetoFormulario.jerarquias = jerarquias;
                    objetoFormulario.division = division;
                service.procesar("saveGrupo",objetoFormulario,function(evt){
                    if(evt.estado == "OK"){
                        alertify.success("GRUPO GUARDADO : "+evt.grupo);
                    }else{
                        alertify.error("GRUPO NO GUARDADO : "+evt.grupo);
                    }
                });
            //}
        }
    })

    $("#modal_reporte_grupos .imprimirreporte").on('click', function(){
        window.open("reportes/reporteDiferenciaGrupos.php");
    })



    $("#generar_cierre .mostrargraficoreportejerarquico").on('click', function(){

        $("#modal_grafico_reporte_jerarquico").modal('show');
        
        service.procesar("getGraficoDiferenciaJerarquica",function(evt){
            //console.log( arregloItemNum ( evt.valores, "diferencia_soles" ) );
            graficoReporteJerarquico(evt);
        })

    })

    function resultadoGenerarCierre(evt){

    }



    $("#generar_cierre .dercargarreportediferencias").on('click', function(){
        var monto = $("#generar_cierre .montoreportediferencias").val();
        var jera = $("#generar_cierre .listadojerarquias").val();
        var subjera = $("#generar_cierre .listadosubjerarquias").val();
        if(monto != ""){
            window.open("reportes/reporteDiferencia.php?monto="+monto+"&jera="+jera+"&subjera="+subjera);
        }else{
            alertify.error("INGRESAR MONTO PARA EL REPORTE");
        }
    })

    $("#generar_cierre .dercargarreportediferenciassku").on('click', function(){
        var limite = $("#generar_cierre .limitereportediferencias").val();
        if(limite != "" || limite > 0){
            window.open("reportes/reporteDiferenciasku.php?limite="+limite);
        }else{
            alertify.error("INGRESAR LIMITE PARA EL REPORTE");
        }
    })

    $("#generar_cierre .excelgraficoreportejerarquico").on('click',function(){
        service.procesar("getDescargaReporteDiferenciaJerarquica",function(evt){
            document.location = "webService/reportes/reportediferenciajerarquica.xlsx"
        });
    })

    $("#generar_cierre .excelgraficoreportediferencias").on('click',function(){
        var monto = $("#generar_cierre .montoreportediferencias").val();
        var jera = $("#generar_cierre .listadojerarquias").val();
        var subjera = $("#generar_cierre .listadosubjerarquias").val();
        if(monto != ""){
            service.procesar("getDescargaReporteDiferencia",monto,jera,subjera,function(evt){
                document.location = "webService/reportes/reportediferencia.xlsx"
            });
        }else{
            alertify.error("INGRESAR MONTO PARA EL REPORTE");
        }
    })

    $("#generar_cierre .dercargarreportediferenciastotal").on('click',function(){
        service.procesar("getDescargaReporteDiferenciaTotal",function(evt){
            document.location = "webService/reportes/reportediferenciatotal.xlsx"
        })
    })

    $("#generar_cierre .excelgraficoreportediferenciassku").on('click', function(){
        var limite = $("#generar_cierre .limitereportediferencias").val();
        if(limite != "" || limite > 0){
            service.procesar("getDescargaReporteDiferenciaSku",limite,function(evt){
                document.location = "webService/reportes/reportediferenciasku.xlsx"
            });
        }else{
            alertify.error("INGRESAR LIMITE PARA EL REPORTE");
        }
    })

    $("#generar_cierre .excelreportelotizacion").on('click', function(){

        service.procesar("getDescargaReporteLotizacion",function(evt){
                document.location = "webService/reportes/reporte_lotizacion.xlsx"
            });

    })

    $("#generar_cierre .excelreportediferencias").on('click', function(){

        service.procesar("getDescargaReporteDiferecias",function(evt){
                document.location = "webService/reportes/reporte_diferencias.xlsx"
            });

    })

    $("#generar_cierre .dercargarreportetotalcontado").on('click', function(){
        window.open("reportes/reporteTotalContado.php");
    })



    //MODIFICAR AQUI Y EL PHP

    $("#generar_cierre .generarReporteProductividad").on('click',function(){
        alertify.confirm("¿ SEGURO DE GENERAR ARCHIVO DE REPORTE DE PRODUCTIVIDAD ?", function (e) {
            if (e) {
                var estado = 1;
                service.procesar("generarReporteProductividad",estado,resultadoGenerarArchivoReporteProductividad);
                openModalGenerarArchivo("REPORTE DE PRODUCTIVIDAD");
            } else {
                alertify.error("HA CANCELADO EL PROCESO");
            }
        });
    });

    function resultadoGenerarArchivoReporteProductividad(evt){
        var resultado = evt;
        closeModalGenerarArchivo();
    }
    /*
    $("#generar_archivos .archivo-reporte-productividad .descargararchivo").on('click', function(){
        var file = 'reporte_productividad';
        open("webService/descargarArchivoGenerado.php?file="+file);
    })
    */
    $("#generar_cierre .imprimirReporteProductividad").on('click', function(){
        var estado = 1;
        window.open("reportes/reporteProductividad.php?estado="+estado);
    })

    $("#generar_cierre .imprimirReporteProductividadActualizado").on('click', function(){
        var estado = 1;
        window.open("reportes/reporteProductividadActualizado.php");
        //service.procesar('getReporteProductividadActualizado',function(evt){
        //    console.log(evt);
        //})
    })


    function openModalGenerarArchivo(archivo){
        $("#modal_generar_cierre_progress").modal('show');
        $("#modal_generar_cierre_progress .descripcionarchivogenerado").html("GENERANDO ARCHIVO "+ archivo );
    }

    function closeModalGenerarArchivo(){
        $("#modal_generar_cierre_progress").modal('hide');
        alertify.success("ARCHIVO GENERADO SATISFACTORIAMENTE");
    }




    $("#generar_cierre .excelreportegrafico").on('click', function(){

        service.procesar("getDescargaReporteGrafico",function(evt){
                document.location = "webService/reportes/reporte_grafico_uni_sol.xlsx"
        });

    })














    $("#modal_grafico_reporte_jerarquico .imprimirgrafico").on('click', function(){
        var chart = $("#greportejerarquico").getKendoChart();
        chart.exportPDF({ paperSize: "auto", margin: { left: "1cm", top: "1cm", right: "1cm", bottom: "1cm" } }).done(function(data) {
            kendo.saveAs({
                dataURI: data,
                fileName: "grafico_reporte_jerarquico.pdf"
            });
        });
    })


    function graficoReporteJerarquico(evt) {

        rangos = arregloItem ( evt.rangos, "jerarquias" );
        valores = arregloItemNum ( evt.valores, "diferencia_soles" );
        porcentajes = evt.porcentajes;
        
        tienda = evt.tienda[0].nombreTienda;
        numero = evt.tienda[0].numeroTienda;
        porcentaje = evt.porcentaje;

        $("#greportejerarquico").kendoChart({
            title: {
                text: "IGROUP SAC - REPORTE JERARQUICO ("+porcentaje+" %) \n "+numero+" - "+tienda

            },
                legend: {
                    position: "top"
                },
                series: [{
                    type: "bar",
                    name: "DIFERENCIA",
                    data: valores,
                    labels: {
                        template: "S/. #= kendo.toString(kendo.parseFloat(value), 'N2') #",
                        visible: true
                    },
                    axis: "diferencia"
                }, {
                    type: "bar",
                    name: "PORCENTAJE",
                    data: porcentajes,
                    labels: {
                        template: "#= kendo.toString(kendo.parseFloat(value), 'N2') # %",
                        visible: true
                    },
                    axis: "porcentaje"
                }],
                valueAxes: [{
                    name: "diferencia",
                    title: { text: "Diferencia Soles" }
                }, {
                    name: "porcentaje",
                    title: { text: "%" },
                    min: 0,
                    max: 100
                }],
                categoryAxis: {
                    categories: rangos
                }
            });
        }

/*            
            legend: {
                position: "top"
            },
            seriesDefaults: {
                type: "bar",
                stack: true
            },
            series: [{
                name: "DIFERENCIA SOLES",
                data: valores,
                labels: {
                    template: "S/. #= stackValue #",
                    visible: true
                }
            },{
                name: "PORCENTAJE",
                data: valores
            }],
            categoryAxis:{
                categories: rangos,
                line: {
                    visible: false
                },
                labels: {
                    padding: {top: 0}
                }
            }
        });

    }
*/

    function corregirNull(valor){
        var nuevovalor = "";
        if(valor == null){
          nuevovalor = "";
        } else {
          nuevovalor = valor;
        }
        return nuevovalor;
    }

    function arregloItem ( array, item ) {
        var nuevoArray = []
        if ( array == undefined ) return;
        for ( i = 0; i < array.length; i++) {
            valor = array[i][item];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function arregloItemNum ( array, item ) {
        var nuevoArray = []
        if ( array == undefined ) return;
        for ( i = 0; i < array.length; i++) {
            valor = parseFloat(array[i][item]);
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


