var service = new Service("webService/index.php");


$(function() 
{

iniciarControlCambiaLote();

});

var arrayTiendas = [];

function iniciarControlCambiaLote(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioTienda').val(fecha);

    



    $("#menu_cambia_lote").on('click', function(){
      //cargaConsultasIniciales();  
    })
    $("#boton_menu_cambia_lote").on('click', function(){
      //cargaConsultasIniciales();  
    })

    
    function cargaConsultasIniciales(){
        //service.procesar("getListaCambiaLote",cargaListaCambiaLote);
    }

    function aplicarFiltroCambiaLote(){
        var area_captura = $("#loteActualFormularioCambiaLote").val();
        service.procesar("getListaCambiaLote",area_captura,cargaListaCambiaLote);
    }

    $('#botonBuscarFormularioCambiaLote').on('click',function(){
        if($('#loteActualFormularioCambiaLote').val() != ""){
            aplicarFiltroCambiaLote();    
        }else{
            alertify.error("INGRESE VALOR DE LOTE");
        }
        
    });

    function cargaListaCambiaLote(evt){
        
        listadeCapturas = evt.registros;

        $("#tablaResultadoFormularioCambiaLote thead").html("<th>#</th><th>AREA_CAP</th><th>BARRA_CAP</th><th>DES_BARRA</th><th>CANT_CAP</th><th>TIP_CAP</th><th>USUARIO</th><th>DESCARGADO</th><th>NAMEPALM</th>");
        $("#tablaResultadoFormularioCambiaLote tbody").html("");
        $(".areacambiodelote").css("display","none");

        //service.procesar("getListaAreaRango",resultadoAvanceCapturaInicial);

        if ( listadeCapturas == undefined ) return;

        $("#tablaResultadoFormularioCambiaLote thead").html("<th>#</th><th>AREA_CAP</th><th>BARRA_CAP</th><th>DES_BARRA</th><th>CANT_CAP ( "+parseFloat(evt.cantidad[0].sum_cant)+" )</th><th>TIP_CAP</th><th>USUARIO</th><th>DESCARGADO</th><th>NAMEPALM</th>");
        $(".areacambiodelote").css("display","");
        $("#registrosFormularioCaptura").html();


        for(var i=0; i<listadeCapturas.length ; i++){
            var fila = $("<tr>");
            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeCapturas[i].area_cap +'</td><td>'+ listadeCapturas[i].barra_cap +'</td><td>'+ listadeCapturas[i].des_barra +'</td><td>'+ listadeCapturas[i].cant_cap +'</td><td>'+ listadeCapturas[i].tip_cap +'</td><td>'+ listadeCapturas[i].nombreUsuario +'</td><td>'+ listadeCapturas[i].descargado +'</td><td>'+ listadeCapturas[i].namepalm +'</td>');
            $("#tablaResultadoFormularioCambiaLote tbody").append(fila);
        }

    }

    $('#botonCambiarFormularioCambiaLote').on('click',function(){
        var valorActual = $('#loteActualFormularioCambiaLote').val();
        var valorNuevo = $('#loteNuevoFormularioCambiaLote').val();
        if(valorActual != "" && valorNuevo != "" && valorActual != valorNuevo){
            
            alertify.confirm("¿ SEGURO DE LOTE LOS REGISTROS ?", function (e) {
                if (e) {
                    var objetoFormulario = new Object()
                        objetoFormulario.valorActual = valorActual;
                        objetoFormulario.valorNuevo = valorNuevo;
                    service.procesar("modificaLoteFormularioCambiaLote",objetoFormulario,resultadoModificaLoteFormularioCambiaLote);
                    alertify.success("HA ACEPTADO MODIFICAR LOTE A LOS REGISTROS");


                } else {
                    alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
                }
            });

        }else{
            alertify.error("REVISE LOS VALORES INGRESADOS");
        }
        
    });

    function resultadoModificaLoteFormularioCambiaLote(evt){
        if(evt == 1 ){
            alertify.success("REGISTROS MODIFICADOS SATISFACTORIAMENTE");
            var valorNuevo = $("#loteNuevoFormularioCambiaLote").val();
            $("#loteNuevoFormularioCambiaLote").val("");
            $("#loteActualFormularioCambiaLote").val( valorNuevo );
            aplicarFiltroCambiaLote(); 
        }else{
            alertify.error("PROCESO NO EJECUTADO");
        }
    }



    //LLENADO DE INFORMACION EN EL AREA DE AVANCE

    function resultadoAvanceCapturaInicial(evt){
        listadeAreaRango = corregirNullArreglo(evt.rangos);
        listaCapturas = corregirNullArreglo(evt.capturas);
        listaJustificados = corregirNullArreglo(evt.justificados);

        var cantidad = 0;
        var avance = 0;
        var porcentaje = 0;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);

            for (var z=filaInicio; z<=filaFinal; z++) {
                cantidad++;
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                    }
                }
            }

        }

        porcentaje = ((parseFloat(avance) / parseFloat(cantidad))*100).toFixed(2);

        $(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
        $(".progressAvanceCaptura").html(porcentaje+' %');      
    }


    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function corregirNullArreglo(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

}


