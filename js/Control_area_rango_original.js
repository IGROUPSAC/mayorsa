var service = new Service("webService/index.php");


$(function() 
{

iniciarControlAreaRango();

});

var arrayAreaRango = [];
var listadeAreaRango = [];
var listaCapturas = [];
var listaJustificados = [];

var evtRegistros = [];
var evtResultado = [];

var rangosGrafico = [];
var valoresGrafico = [];
var grafico = [];

function iniciarControlAreaRango(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");


    $('#fechaFormularioUsuario').val(fecha);

    

    $("#menu_area_rango").on('click', function(){
      cargaConsultasIniciales();  
    })


    function cargaConsultasIniciales(){ //CORREGIDO
        //service.procesar("getListaAreaRango",cargaListaAreaRango);
        //cargarPeriodos();
    }


    function cargaListaAreaRango(evt){ //CORREGIDO
        //resultadoAvanceCaptura(evt);
        listadeAreaRango = evt.rangos;
        listaCapturas = evt.capturas;
        listaJustificados = corregirNull(evt.justificados);

        rangosGrafico = [];
        valoresGrafico = [];
        grafico = [];
        //console.log(evt);

        $("#tablaResultadoFormularioAreaRango tbody").html("");

        if ( listadeAreaRango == undefined ) return;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);
            var cantidad = ( filaFinal - filaInicio ) + 1;
            var avance = 0;
            var porcentaje = 0;
            var activado = "";

            var areaRangoDetalle = [];
            var lotesEncontrados = [];
            var lotes = [];

            var colorImportante = "btn-default";

            for (var z=filaInicio; z<=filaFinal; z++) { 
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                        lotesEncontrados.push(z);
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                        //lotesEncontrados.push(z);
                    }
                }
                
            }

            //console.log(lotesEncontrados);
            for (var x = filaInicio; x <= filaFinal; x++) {
                //console.log(x);
                var encontrado = lotesEncontrados.indexOf(x);
                if(encontrado < 0){
                    lotes.push({lote:x});
                }
            }

            porcentaje = ((avance / cantidad)*100).toFixed(2);

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = listadeAreaRango[i];

            areaRangoDetalle.push(lotes);
            areaRangoDetalle.push(datoRegistro);

            rango = listadeAreaRango[i].area_ini_ran + " - " + listadeAreaRango[i].area_fin_ran;

            rangosGrafico.push(listadeAreaRango[i].des_area_ran);
            valoresGrafico.push(porcentaje);

            fila.html('<td>'+ (i + 1) +'</td><td>'+ listadeAreaRango[i].area_ini_ran +'</td><td>'+ listadeAreaRango[i].area_fin_ran +'</td><td>'+ listadeAreaRango[i].des_area_ran +'</td><td>'+ listadeAreaRango[i].fecha +'</td><td>'+ listadeAreaRango[i].idUsuario +'</td><td>'+ cantidad +'</td><td>'+ avance +'</td><td>'+ porcentaje +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioAreaRango);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);



            var contenedorImportante = $("<td>");

            if(avance < cantidad){
                colorImportante = "btn-info";
                //activado = "disabled";
            }

            var btnJustificar = $("<button class='btn "+ colorImportante +" btn-sm botonesAreaRango' href='#modal_area_rango' data-toggle='modal' title='Justificar' "+ activado +">");
            btnJustificar.html('JUSTIFICAR');
            btnJustificar.data("data",areaRangoDetalle);
            btnJustificar.on("click",justificarFormularioAreaRango);

            contenedorImportante.append(btnJustificar);

            fila.append(contenedorImportante);






            var contenedorBotones = $("<td>");

            
            var btnSeleccionar = $("<button class='btn btn-primary btn-sm botonesAreaRango' title='Editar'>");
            btnSeleccionar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnSeleccionar.data("data",datoRegistro);
            btnSeleccionar.on("click",editarFormularioAreaRango);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesAreaRango' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioAreaRango);

            contenedorBotones.append(btnSeleccionar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#tablaResultadoFormularioAreaRango tbody").append(fila);

        }
        grafico.push(rangosGrafico);
        grafico.push(valoresGrafico);

        graficoAvanceCaptura(grafico);

    }

    function justificarFormularioAreaRango(){
        var registroArea = $(this).data("data");

        //console.log(registroArea);

        service.procesar("getListaJustificacionAreaRango",function(evt){
            cargarListaPendientesJustificacion(registroArea,evt);
            evtRegistros = registroArea;
        });

    }

    function listarLotesJustificados(){
        service.procesar("getListaJustificacionAreaRango",function(evt){
            cargarListaPendientesJustificacion(evtRegistros,evt);
        });
    }


    function cargarListaPendientesJustificacion(regis,justi){
        var lotes = regis[0];
        var registros = regis[1];

        $("#modal_area_rango input").val(registros.idAreaRango);

        if(justi == undefined){
            var justi = [];
        }
        //var cantidadJustificados = justi.length;

        var justificados = arregloItem(justi,"lote");

        




        $("#tablaModalJustificacionFormularioAreaRango tbody").html("");

        if ( lotes == undefined ) return;

        for(var i=0; i<lotes.length ; i++){

            var fila = $("<tr>");
            var loteDetalle = [];
            var valLote = lotes[i];
            var lote = lotes[i].lote;

            var colorJustificacion = "btn-default";
            var descrJustificacion = "";
            var tipoJustificacion = "JUSTIFICAR";
            var loteJustificado = "";

            if ( justi != undefined ){
                loteJustificado = justificados.indexOf( lote.toString() );
            }
            

            if (loteJustificado >= 0){
                colorJustificacion = "btn-primary";

                if(justi.length > 0){
                    descrJustificacion = justi[loteJustificado].justificacion;
                    tipoJustificacion = justi[loteJustificado].tipo;
                }
                

            }
            //var listaJustificados = existeEnArray ( justificados , lote.toString() );
            


            loteDetalle.push(i+1);
            loteDetalle.push(valLote);
            loteDetalle.push(justi[loteJustificado]);
            loteDetalle.push(registros);

            fila.html('<td>'+ (i + 1) +'</td><td>'+ lote +'</td><td>'+ registros.des_area_ran +'</td>');

            var contenedor = $("<td>");

            var btnGuardar = $("<button class='btn "+ colorJustificacion +" btn-block btn-sm botonGuardarJustificacion' title='Justificar'>");
            btnGuardar.html(tipoJustificacion);
            btnGuardar.data("data",loteDetalle);
            btnGuardar.on("click",justificarValorLote);

            contenedor.append(btnGuardar);
            fila.append(contenedor);

            fila.append("<td><label class='form-control input-sm desJustificacion'>"+descrJustificacion+"</label></td>");

            $("#tablaModalJustificacionFormularioAreaRango tbody").append(fila);

        }

    }


    function justificarValorLote(){
        var data = $(this).data("data");
        var posicion = data[0];
        var lote = data[1].lote;
        var datos = data[2];
        var registros = data[3];

        //console.log(registros);

        //alertify.prompt('Modal: true').set('modal', true);
        if ( datos == undefined ){
            $('#modal_area_rango_confirmacion').modal('show');
            $('#modal_area_rango_confirmacion label').html(lote);
            $('#modal_area_rango_confirmacion input').val(registros.des_area_ran);
            $('#modal_area_rango_confirmacion select').val(0);
            $('#modal_area_rango_confirmacion textarea').val("");
            $('#boton_guardar_modal_area_rango_confirmacion').html("GUARDAR");
            $('#boton_guardar_modal_area_rango_confirmacion').removeClass('btn-warning');
            $('#boton_guardar_modal_area_rango_confirmacion').addClass('btn-primary');  

        }else{
            $('#modal_area_rango_confirmacion').modal('show');
            $('#modal_area_rango_confirmacion label').html(datos.lote);
            $('#modal_area_rango_confirmacion input').val(registros.des_area_ran);
            $('#modal_area_rango_confirmacion select').val(datos.tipo);
            $('#modal_area_rango_confirmacion textarea').val(datos.justificacion);
            $('#boton_guardar_modal_area_rango_confirmacion').html("MODIFICAR");
            $('#boton_guardar_modal_area_rango_confirmacion').removeClass('btn-primary');
            $('#boton_guardar_modal_area_rango_confirmacion').addClass('btn-warning');
        }



        //$(".botonesJustificacion").css("display","");
        //document.getElementsByClassName('desJustificacion')[parseInt(posicion)-1].disabled = false;
        //document.getElementsByClassName('botonGuardarJustificacion')[parseInt(posicion)-1].style.display = "";
        //document.getElementsByClassName('botonCancelarJustificacion')[parseInt(posicion)-1].style.display = "";
    }


    $("#boton_guardar_modal_area_rango_confirmacion").on('click', function(){
        var lote = $('#modal_area_rango_confirmacion label').html();
        var tipo = $('#modal_area_rango_confirmacion select').val();
        var ubicacion = $('#modal_area_rango_confirmacion input').val();
        var justificacion = $('#modal_area_rango_confirmacion textarea').val();
        var procedimiento = $("#boton_guardar_modal_area_rango_confirmacion").html();

        if( (tipo == "COMBINADO" && justificacion != "") || (tipo != "COMBINADO" && tipo != "0" ) ){
            alertify.confirm("¿ SEGURO DE GUARDAR JUSTIFICACION PARA LOTE : " + lote +" ?", function (e) {
                if (e) {
                    var objetoFormulario = new Object()
                        objetoFormulario.lote = lote;
                        objetoFormulario.tipo = tipo;
                        objetoFormulario.ubicacion = ubicacion;
                        objetoFormulario.justificacion = justificacion;
                        objetoFormulario.procedimiento = procedimiento;
                    service.procesar("saveJustificacionLoteAreaRango",objetoFormulario,respuestaJustificacionLoteAreaRango);
                    alertify.success("HA ACEPTADO GUARDAR LA JUSTIFICACION");
                } else {
                    //$('#modal_area_rango_confirmacion').modal('hide');
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });
        }else{
            alertify.error("CAMPOS INCOMPLETOS");
        }

    })

    function respuestaJustificacionLoteAreaRango(evt){
        if(evt == 1 ){
            alertify.success("JUSTIFICACION GUARDADA SATISFACTORIAMENTE");
            $('#modal_area_rango_confirmacion').modal('hide');
            //service.procesar("getListaAreaRango",cargaListaAreaRango);
            listarLotesJustificados();
        }else if(evt == 2 ){
            alertify.success("JUSTIFICACION MODIFICADA SATISFACTORIAMENTE");
            $('#modal_area_rango_confirmacion').modal('hide');
            //service.procesar("getListaAreaRango",cargaListaAreaRango);
            listarLotesJustificados();
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }

    }

    function seleccionarFormularioAreaRango(){ //corregido
        var data = $(this).data("data");
        var idAreaRango = data.idAreaRango;
        var valor = $.inArray(idAreaRango, arrayAreaRango);

        if(valor >= 0){
        	removeItemFromArr( arrayAreaRango, idAreaRango );
        }else{
        	arrayAreaRango.push(idAreaRango);
        }
        
        var contar = arrayAreaRango.length;

        if(contar > 1){
        	$(".opcionesAreaRango").css("display","none");
            $(".botonesAreaRango").prop( "disabled", true );
        	$(".opcionesAreaRangoMasivo").css("display","");
        }else{
        	$(".opcionesAreaRango").css("display","");
            $(".botonesAreaRango").prop( "disabled", false );
        	$(".opcionesAreaRangoMasivo").css("display","none");
        }
    }

    function editarFormularioAreaRango(){ //MODIFICADO
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DEL AREA RANGO : " + data.idAreaRango +" ?", function (e) {
            if (e) {
                $("#codigoFormularioAreaRango").val(data.idAreaRango);
                $("#rangoInicioFormularioAreaRango").val(data.area_ini_ran);
                $("#rangoFinalFormularioAreaRango").val(data.area_fin_ran);
                $("#descripcionFormularioAreaRango").val(data.des_area_ran);


                $("#botonGuardarAreaRango").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }

    function eliminarFormularioAreaRango(){ //CORREGIDO
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS DEL AREA RANGO : " + data.idAreaRango +" ?", function (e) {
            if (e) {
                service.procesar("deleteFormularioAreaRango",data.idAreaRango,resultadoDeleteFormularioAreaRango);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioAreaRango(evt){ //CORREGIDO
        //service.procesar("getListaAreaRango",cargaListaAreaRango);
    }

    $("#botonGuardarAreaRango").on('click', function(){ //CORREGIDO

        var procedimiento = $("#botonGuardarAreaRango").html();
        var idAreaRango = $("#codigoFormularioAreaRango").val();
        var noincluir = $("#codigoFormularioAreaRango").val();

        var area_inicio  = parseFloat($("#rangoInicioFormularioAreaRango").val());
        var area_final = parseFloat($("#rangoFinalFormularioAreaRango").val());
        var area_descripcion = $("#descripcionFormularioAreaRango").val();
        var coincide = 0;
        //FORMULA PARA QUE NO ENCUENTRE COINCIDENCIAS
        if(listadeAreaRango != null){
            for(var x=0; x<listadeAreaRango.length ; x++){
                var filaInicio =parseFloat(listadeAreaRango[x].area_ini_ran);
                var filaFinal = parseFloat(listadeAreaRango[x].area_fin_ran);

                    if(listadeAreaRango[x].idAreaRango != noincluir){

                        for (var z=filaInicio; z<=filaFinal; z++) { 

                            for (var y=area_inicio; y<=area_final; y++) {

                                if (z == y){

                                    coincide = 1;
                                }

                            }

                        }

                    }

            }
        }






        if(area_inicio != "" && area_final != "" && area_descripcion != "" && coincide == 0 && area_inicio <= area_final){

            alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                if (e) {

                    var objetoFormulario = new Object()
                        objetoFormulario.procedimiento = procedimiento;
                        objetoFormulario.idAreaRango = idAreaRango;
                        objetoFormulario.inicio = area_inicio;
                        objetoFormulario.final = area_final;
                        objetoFormulario.descripcion = area_descripcion;
                        objetoFormulario.usuario = usuario;


                    service.procesar("saveFormularioAreaRango",objetoFormulario,cargaFormularioAreaRango);

                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });

        }else{
            if(coincide == 0 && area_inicio < area_final){
                alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
            }else{
                alertify.error("EXISTE CONFLICTO DE RANGO");
            }
        }



    })

    $("#botonCancelarAreaRango").on('click',function(){ //CORREGIDO
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
            if (e) {
		        alertify.success("PROCESO CANCELADO");
		        formularioAreaRangoEstandar();
            }
        });
    })

    $("#botonEliminarMasivoFormularioAreaRango").on('click',function(){ //CORREGIDO
        alertify.confirm("¿ SEGURO DE ELIMINAR DATOS MASIVOS ?", function (e) {
            if (e) {

                var objetoFormulario = new Object()
                    objetoFormulario.areaRango = arrayAreaRango;
                    objetoFormulario.usuario = usuario;

                service.procesar("eliminarMasivoFormularioAreaRango",objetoFormulario,cargaEliminarMasivoFormularioUAreaRango);

                alertify.success("HA ACEPTADO ELIMINAR LOS REGISTROS");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    })

    function cargaEliminarMasivoFormularioUAreaRango(evt){ //CORREGIDO
    	if(evt == 1 ){
            alertify.success("REGISTROS ELIMINADOS SATISFACTORIAMENTE");
            $(".opcionesAreaRango").css("display","");
        	$(".opcionesAreaRangoMasivo").css("display","none");
        	arrayAreaRango.length=0;
            //service.procesar("getListaAreaRango",cargaListaAreaRango);
        }else{
        	alertify.error("PROCESO NO EJECUTADO");
        }
    }

    function cargaFormularioAreaRango(evt){ //CORREGIDO
        if(evt == 1 ){
            alertify.success("REGISTRO GUARDADO SATISFACTORIAMENTE");
            //service.procesar("getListaAreaRango",cargaListaAreaRango);
            formularioAreaRangoEstandar();
        }else if(evt == 2 ){
            alertify.success("REGISTRO MODIFICADO SATISFACTORIAMENTE");
            //service.procesar("getListaAreaRango",cargaListaAreaRango);
            formularioAreaRangoEstandar();
        }else{
            alertify.error("REGISTRO NO GUARDADO"); 
        }
    }

    function formularioAreaRangoEstandar(){ //CORREGIDO
        $("#rangoInicioFormularioAreaRango").val("");
        $("#codigoFormularioAreaRango").val("");
        $("#rangoFinalFormularioAreaRango").val("");
        $("#descripcionFormularioAreaRango").val("");
        $("#botonGuardarAreaRango").html("GUARDAR");
    }


    $("#botonListarImprimirAreaRango").on('click',function(){ //CORREGIDO
        //service.procesar("imprimirReporteUsuariosActivos",0,mensajeImprimirReporteUsuariosActivos);
        window.open("reportes/reporteAreaRango.php");
    })

    $("#botonListarPendientesImprimirAreaRango").on('click',function(){
        window.open("reportes/reportePendientesAreaRango.php");
    })

    $("#imprimirGraficoAvanceCaptura").on('click', function(){
        window.open("reportes/reporteGraficoAreaRango.php");
        //$("#greportes").printThis();
    })






    function resultadoAvanceCaptura(evt){
        listadeAreaRango = evt.rangos;
        listaCapturas = evt.capturas;
        listaJustificados = corregirNull(evt.justificados);

        var cantidad = 0;
        var avance = 0;
        var porcentaje = 0;

        for(var i=0; i<listadeAreaRango.length ; i++){
            var filaInicio =parseFloat(listadeAreaRango[i].area_ini_ran);
            var filaFinal = parseFloat(listadeAreaRango[i].area_fin_ran);

            for (var z=filaInicio; z<=filaFinal; z++) {
                cantidad++;
                for (var y=0; y<listaCapturas.length; y++) {
                    var valorArea = parseFloat(listaCapturas[y].area_cap);
                    if (z == valorArea){
                        avance++;
                    }
                }
                for (var y=0; y<listaJustificados.length; y++) {
                    var valorArea = parseFloat(listaJustificados[y].lote);
                    if (z == valorArea){
                        avance++;
                    }
                }
            }

        }

        porcentaje = ((parseFloat(avance) / parseFloat(cantidad))*100).toFixed(2);

        $(".progressAvanceCaptura").css('width', porcentaje+'%').attr('aria-valuenow', porcentaje);
        $(".progressAvanceCaptura").html(porcentaje+' %');       
    }

    $("#imprimirJustificacionFormularioAreaRango").on('click', function(){
        var areaRango = $("#modal_area_rango input").val();
        window.open("reportes/reporteModalAreaRango.php?idAreaRango="+areaRango);
    })


    function graficoAvanceCaptura(evt) {

        rangos = evt[0];
        valores = evt[1];

        //console.log(evt);

        $("#greportes").kendoChart({
            title: {
                text: "IGROUP SAC - AVANCE DE CAPTURA"
            },
            legend: {
                position: "top"
            },
            series: [ {
                name: "RANGOS",
                data: valores
            }],
            categoryAxis:{
                categories: rangos
            },
            valueAxis: {
                max: 100,
                majorUnit: 10,
                labels: {
                    format: "{0}%"
                }
            },
            tooltip: {
                visible: true,
                template: "AVANCE: ${value} %"
            }
        });
    }

    //$(document).ready(crearGraficoReportes);
    //$(document).bind("kendo:skinChange", crearGraficoReportes);







    function existeEnArray (arr,item){
        var valor = "0";
        var i = arr.indexOf( item );
        if ( i !== -1 ) {
            console.log("encontrado");
           valor = i;
        }
        return valor;
    }


    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

    function arregloItem ( array, item ) {
        var nuevoArray = []
        if ( array == undefined ) return;
        for ( i = 0; i < array.length; i++) {
            valor = array[i][item];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function corregirNull(valor){
        var resultado = valor;
        if(valor == undefined || valor == null){
            resultado = "";
        }
        return resultado;
    }

}


