var service = new Service("webService/index.php");


$(function() 
{

iniciarControlInforme();

});

var arrayPreguntas = [];

function iniciarControlInforme(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    $("#almacenBodega .fechaAlmacenBodega").val(fecha);

    $("#menu_informe").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_informe").on('click', function(){
      cargaConsultasIniciales();  
    })


    var arreglo_preguntas_AB = [];
    var arreglo_preguntas_PV = [];
    var reporte = 1;
    function cargaConsultasIniciales(){
        service.procesar("getCuestionarioAlmacenBodega",resultadoCuestionarioAlmacenBodega);
        service.procesar("getCuestionarioPisoVenta",resultadoCuestionarioPisoVenta);
        service.procesar("getDatosCuestionarioAB",1,cargaInformeAlmacenBodega);
        service.procesar("getDatosCuestionarioPV",2,cargaInformePisoVenta);

        service.procesar("getPreguntas",cargaPreguntas);

        service.procesar("getCuestionarios",cargaCuestionarios);
        service.procesar("getGrupos",cargaGrupos);

        
    }

    function cargaCuestionarios(evt){
        resultado = evt;
        $("#configurarPreguntas .cuestionarioPregunta").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#configurarPreguntas .cuestionarioPregunta" ).append( "<option value='"+ resultado[i].idCuestionario +"'>"+ resultado[i].cuestionario +"</option>" );
        }
    } 

    function cargaGrupos(evt){
        resultado = evt;
        $("#configurarPreguntas .grupoPregunta").html("<option value='0'>---SELECCIONAR---</option>");
        if ( resultado == undefined ) return;
        for(var i=0; i<resultado.length ; i++){
            $("#configurarPreguntas .grupoPregunta" ).append( "<option value='"+ resultado[i].idGrupo +"'>"+ resultado[i].grupo +"</option>" );
        }
    } 

    function resultadoCuestionarioAlmacenBodega(resultado){
        var cuestionarios = resultado.cuestionario;
        //resultado = evt;
        $("#almacenBodega .cuestionario").html("");

        if ( cuestionarios == undefined ) return;

        $("#almacenBodega .numeroTiendaAlmacenBodega").val(resultado.numeroTienda);
        $("#almacenBodega .nombreTiendaAlmacenBodega").val(resultado.nombreTienda);

        for(var c=0; c<cuestionarios.length ; c++){

            $("#almacenBodega .cuestionario").append("<div class='row'><div class='col-md-12 texto negrita'>"+cuestionarios[c].cuestionario.toUpperCase()+"</div></div>");
            $("#almacenBodega .cuestionario").append("<div class='preguntas_cuestionario_"+cuestionarios[c].idCuestionario+"'></div>");
            idCuestionario = cuestionarios[c].idCuestionario;
            service.procesar("getPreguntasInforme",idCuestionario,preguntasCuestionarioInforme);

        }

    }

    function resultadoCuestionarioPisoVenta(cuestionarios){
        //resultado = evt;
        $("#pisoVenta .cuestionario").html("");

        if ( cuestionarios == undefined ) return;

        for(var c=0; c<cuestionarios.length ; c++){

            $("#pisoVenta .cuestionario").append("<div class='row'><div class='col-md-12 texto negrita'>"+cuestionarios[c].cuestionario.toUpperCase()+"</div></div>");
            $("#pisoVenta .cuestionario").append("<div class='preguntas_cuestionario_"+cuestionarios[c].idCuestionario+"'></div>");
            idCuestionario = cuestionarios[c].idCuestionario;
            service.procesar("getPreguntasInforme",idCuestionario,preguntasCuestionarioInforme);

        }

    }

    function preguntasCuestionarioInforme(resultado){
        cuestionario = resultado.cuestionario;
        preguntas = resultado.preguntas;
        //arreglo_preguntas_AB = [];
        //tablacuestionario
        $("#informe .preguntas_cuestionario_"+cuestionario).html("<div class='row'><table id='tabla_cuestionario_"+cuestionario+"' class='table table-striped table-bordered table-hover tablacuestionario'><thead><tr><th class='centro' width='5%'>N</th><th class='centro' width='75%'>DESCRIPCION</th><th class='centro' width='20%'>OPCIONES</th></tr></thead><tbody></tbody></table></div>");
        
        if ( preguntas == undefined ) return;

        //$("#informe .preguntas_cuestionario_"+cuestionario).append("<div class='row celdapregunta'><div class='col-md-8 texto negrita centro preguntas'>DESCRIPCION</div><div class='col-md-4 texto negrita centro preguntas'>OPCIONES</div></div>");

        for(var p=0; p<preguntas.length ; p++){
            var fila_celdas = "";
            var fila = $("<tr>");
            var pregunta = preguntas[p].idPregunta;

            datoRegistro = preguntas[p];

            if(preguntas[p].idReporte == 1){
                arreglo_preguntas_AB.push("optC"+cuestionario+"P"+pregunta);
            }

            if(preguntas[p].idReporte == 2){
                arreglo_preguntas_PV.push("optC"+cuestionario+"P"+pregunta);
            }
            

            fila.html("<td class='centro'>"+ (p + 1) +"</td><td>"+ preguntas[p].pregunta.toUpperCase() +"</td>");

            var contenedorRespuestas = $("<td>");

            var optRespuesta = $("<select id='optC"+cuestionario+"P"+pregunta+"' class='form-control input-sm preguntaInforme'>");

            optRespuesta.html("<option value='0'>---SELECCIONAR---</option>");
            optRespuesta.data("data",datoRegistro);
            optRespuesta.on("change",seleccionarRespuestaPregunta);
            contenedorRespuestas.append(optRespuesta);

            fila.append(contenedorRespuestas);

            $("#tabla_cuestionario_"+cuestionario+" tbody").append(fila);

            service.procesar("getRespuestasInforme",cuestionario,pregunta,respuestasPregunta);
        }

    }

    function respuestasPregunta(evt){
        cuestionario = evt.cuestionario;
        pregunta = evt.pregunta;
        respuestas = evt.respuestas;

        $("#optC"+cuestionario+"P"+pregunta).html("<option value='0'>---SELECCIONAR---</option>");
        if ( respuestas == undefined ) return;
        for(var i=0; i<respuestas.length ; i++){
            $("#optC"+cuestionario+"P"+pregunta).append( "<option value='"+ respuestas[i].idRespuesta +"' "+respuestas[i].seleccionado+">"+ respuestas[i].respuesta +"</option>" );
        }

    }

    function seleccionarRespuestaPregunta(){
        var data = $(this).data("data");
        //console.log(data);
    }




    $("#botonFirmaSupervidorIgroupAlmacenBodega").on('click', function(){
        $("#modalFirmaAlmacenBodega").modal("show");
    })

    $("#informe .botonGuardarAlmacenBodega").on('click', function(){
        alertify.confirm("¿ SEGURO DE GUARDAR INFORME ALMACEN BODEGA ?", function (e) {
            if (e) {
                var fecha = $("#almacenBodega .fechaAlmacenBodega").val();
                var responsableIgroup = $("#almacenBodega .responsableIgroupAlmacenBodega").val();
                var responsableCliente = $("#almacenBodega .responsableClienteAlmacenBodega").val();
                var encargadoCliente = $("#almacenBodega .encargadoClienteAlmacenBodega").val();
                var inicioConteo = $("#almacenBodega .inicioConteoAlmacenBodega").val();
                var supervisorIgroup = $("#almacenBodega .supervisorIgroupAlmacenBodega").val();
                var finConteo = $("#almacenBodega .finConteoAlmacenBodega").val();
                var observaciones = $("#almacenBodega .observacionesAlmacenBodega").val();

                var objeto = new Object()
                    objeto.fecha = fecha;
                    objeto.responsableIgroup = responsableIgroup;
                    objeto.responsableCliente = responsableCliente;
                    objeto.tienda = tienda;
                    objeto.encargadoCliente = encargadoCliente;
                    objeto.inicioConteo = inicioConteo;
                    objeto.supervisorIgroup = supervisorIgroup;
                    objeto.finConteo = finConteo;
                    objeto.observaciones = observaciones;

                service.procesar("saveInformeAlmacenBodega",objeto,resultadoSaveInformeAlmacenBodega);

            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });

    })

    $("#informe .botonCancelarAlmacenBodega").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR CAMBIOS EN EL INFORME ?", function (e) {
            if (e) {
                service.procesar("getDatosCuestionarioAB",1,cargaInformeAlmacenBodega);
                service.procesar("getCuestionarioAlmacenBodega",resultadoCuestionarioAlmacenBodega);
            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });
    })

    function resultadoSaveInformeAlmacenBodega(evt){
            alertify.success("INFORME ALMACEN BODEGA GUARDADO SATISFACTORIAMENTE");
            for(var i=0; i<arreglo_preguntas_AB.length ; i++){
                var idRespuesta = $("#"+arreglo_preguntas_AB[i]).val();
                var valores = $("#"+arreglo_preguntas_AB[i]).data();
                var idPregunta = valores.data["idPregunta"];

                service.procesar("savePreguntasInforme",idPregunta,idRespuesta,function(evt){
                    //console.log(evt);
                });
                //console.log(idPregunta + " - " + idRespuesta);
            }

    }

    function cargaInformeAlmacenBodega(evt){
        var resultado = evt[0];
        $("#almacenBodega .fechaAlmacenBodega").val(resultado.fecha);
        $("#almacenBodega .responsableIgroupAlmacenBodega").val(resultado.responsableIgroup);
        $("#almacenBodega .responsableClienteAlmacenBodega").val(resultado.responsableCliente);
        $("#almacenBodega .encargadoClienteAlmacenBodega").val(resultado.encargadoCliente);
        $("#almacenBodega .inicioConteoAlmacenBodega").val(resultado.inicioConteo);
        $("#almacenBodega .supervisorIgroupAlmacenBodega").val(resultado.supervisorIgroup);
        $("#almacenBodega .finConteoAlmacenBodega").val(resultado.finConteo);
        $("#almacenBodega .observacionesAlmacenBodega").val(resultado.observaciones);
    }

    $("#informe .botonGuardarPisoVenta").on('click', function(){
        alertify.confirm("¿ SEGURO DE GUARDAR INFORME PISO VENTA ?", function (e) {
            if (e) {
                var encargadoCliente = $("#pisoVenta .encargadoClientePisoVenta").val();
                var inicioConteo = $("#pisoVenta .inicioConteoPisoVenta").val();
                var supervisorIgroup = $("#pisoVenta .supervisorIgroupPisoVenta").val();
                var finConteo = $("#pisoVenta .finConteoPisoVenta").val();
                var mesaControl = $("#pisoVenta .mesaControlPisoVenta").val();
                var cierreInventario = $("#pisoVenta .cierreInventarioPisoVenta").val();
                var observaciones = $("#pisoVenta .observacionesPisoVenta").val();

                var objeto = new Object()
                    objeto.encargadoCliente = encargadoCliente;
                    objeto.inicioConteo = inicioConteo;
                    objeto.supervisorIgroup = supervisorIgroup;
                    objeto.finConteo = finConteo;
                    objeto.mesaControl = mesaControl;
                    objeto.cierreInventario = cierreInventario;
                    objeto.observaciones = observaciones;

                service.procesar("saveInformePisoVenta",objeto,resultadoSaveInformePisoVenta);

            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });

    })

    $("#informe .botonCancelarPisoVenta").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR CAMBIOS EN EL INFORME ?", function (e) {
            if (e) {
                service.procesar("getDatosCuestionarioPV",2,cargaInformePisoVenta);
                service.procesar("getCuestionarioPisoVenta",resultadoCuestionarioPisoVenta);
            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });
    })

    function resultadoSaveInformePisoVenta(evt){
            alertify.success("INFORME PISO VENTA GUARDADO SATISFACTORIAMENTE");
            for(var i=0; i<arreglo_preguntas_PV.length ; i++){
                var idRespuesta = $("#"+arreglo_preguntas_PV[i]).val();
                var valores = $("#"+arreglo_preguntas_PV[i]).data();
                var idPregunta =valores.data["idPregunta"];

                service.procesar("savePreguntasInforme",idPregunta,idRespuesta,function(evt){
                    //console.log(evt);
                });
                //console.log(idPregunta + " - " + idRespuesta);
            }

    }

    function cargaInformePisoVenta(evt){
        var resultado = evt[0];
        $("#pisoVenta .encargadoClientePisoVenta").val(resultado.encargadoCliente);
        $("#pisoVenta .inicioConteoPisoVenta").val(resultado.inicioConteo);
        $("#pisoVenta .supervisorIgroupPisoVenta").val(resultado.supervisorIgroup);
        $("#pisoVenta .finConteoPisoVenta").val(resultado.finConteo);
        $("#pisoVenta .mesaControlPisoVenta").val(resultado.mesaControl);
        $("#pisoVenta .cierreInventarioPisoVenta").val(resultado.cierreInventario);
        $("#pisoVenta .observacionesPisoVenta").val(resultado.observaciones);
    }


    $("#informe .botonLimpiarAlmacenBodega").on('click', function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR INFORME ?", function (e) {
            if (e) {
                $("#almacenBodega .input-sm").val("");
                $("#almacenBodega .preguntaInforme").val(0);
            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });
    })

    $("#informe .botonLimpiarPisoVenta").on('click', function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR INFORME ?", function (e) {
            if (e) {
                $("#pisoVenta .input-sm").val("");
                $("#pisoVenta .preguntaInforme").val(0);
            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });
    })


    $("#informe .botonImprimirAlmacenBodega").on('click', function(){
        alertify.confirm("¿ SEGURO DE IMPRIMIR INFORME ?", function (e) {
            if (e) {
                window.open("reportes/reporteInformeAlmacenBodega.php");
            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });
    })

    $("#informe .botonImprimirPisoVenta").on('click', function(){
        alertify.confirm("¿ SEGURO DE IMPRIMIR INFORME ?", function (e) {
            if (e) {
                window.open("reportes/reporteInformePisoVenta.php");
            } else {
                alertify.error("PROCEDIMIENTO CANCELADO");
            }
        });
    })

    //AREA DE PREGUNTAS Y RSPUESTAS 
    
    function cargaPreguntas(evt){
        var resultado = evt;
        $("#configurarPreguntas .table tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){

            var fila = $("<tr>");
            //var celdaBotones = $("<td>");
            datoRegistro = resultado[i];
            fila.html('<td>'+ (i + 1) +'</td><td>'+ resultado[i].pregunta +'</td><td>'+ resultado[i].orden +'</td><td>'+ resultado[i].cuestionario +'</td><td>'+ resultado[i].grupo +'</td>');

            var contenedorSelectores = $("<td><div class='input-group-btn'>");

            var chkSeleccionar = $("<input type='checkbox' title='Seleccionar'>");
            //chkSeleccionar.html('<span class="glyphicon glyphicon-edit"></span');
            chkSeleccionar.data("data",datoRegistro);
            chkSeleccionar.on("change",seleccionarFormularioPregunta);

            contenedorSelectores.append(chkSeleccionar);

            fila.append(contenedorSelectores);


            var contenedorBotones = $("<td>");

            

            var btnEditar = $("<button class='btn btn-primary btn-sm botonesPregunta' title='Editar'>");
            btnEditar.html('<span class="glyphicon glyphicon-edit"></span> EDITAR');
            btnEditar.data("data",datoRegistro);
            btnEditar.on("click",editarFormularioPregunta);

            var btnEliminar = $("<button class='btn btn-danger btn-sm botonesPregunta' title='Eliminar'>");
            btnEliminar.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR');
            btnEliminar.data("data",datoRegistro);
            btnEliminar.on("click",eliminarFormularioPregunta);

            contenedorBotones.append(btnEditar);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnEliminar);

            fila.append(contenedorBotones);

            $("#configurarPreguntas .table tbody").append(fila);

        }

    }

    function seleccionarFormularioPregunta(){
        var data = $(this).data("data");
        var idPregunta = data.idPregunta;
        var valor = $.inArray(idPregunta, arrayPreguntas);

        if(valor >= 0){
            removeItemFromArr( arrayPreguntas, idPregunta );
        }else{
            arrayPreguntas.push(idPregunta);
        }
        
        var contar = arrayPreguntas.length;

        if(contar > 1){
            $("#configurarPreguntas .opcionesPregunta").css("display","none");
            $("#configurarPreguntas .botonesPregunta").prop( "disabled", true );
            $("#configurarPreguntas .opcionesPreguntaMasivo").css("display","");
        }else{
            $("#configurarPreguntas .opcionesPregunta").css("display","");
            $("#configurarPreguntas .botonesPregunta").prop( "disabled", false );
            $("#configurarPreguntas .opcionesPreguntaMasivo").css("display","none");
        }
    }
    function editarFormularioPregunta(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE MODIFICAR DATOS DE LA PREGUNTA : " + data.pregunta.toUpperCase() +" ?", function (e) {
            if (e) {
                $("#configurarPreguntas .codigoPregunta").val(data.idPregunta);
                $("#configurarPreguntas .nombrePregunta").val(data.pregunta.toUpperCase());
                $("#configurarPreguntas .ordenPregunta").val(data.orden);
                $("#configurarPreguntas .cuestionarioPregunta").val(data.idCuestionario);
                $("#configurarPreguntas .grupoPregunta").val(data.idGrupo);

                $("#configurarPreguntas .guardarPregunta").html("MODIFICAR");

                alertify.success("HA ACEPTADO MODIFICAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE MODIFICACION");
            }
        });
    }
    function eliminarFormularioPregunta(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR LA PREGUNTA : " + data.pregunta.toUpperCase() +" ?", function (e) {
            if (e) {
                service.procesar("deleteFormularioPregunta",data.idPregunta,resultadoDeleteFormularioPregunta);
                alertify.success("HA ACEPTADO ELIMINAR EL REGISTRO");
            } else {
                alertify.error("HA CANCELADO EL PROCESO DE ELIMINACION");
            }
        });
    }

    function resultadoDeleteFormularioPregunta(evt){
        cargaConsultasIniciales();
    }


    $("#configurarPreguntas .cancelarPregunta").on('click', function(){
        alertify.confirm("¿ SEGURO DE CANCELAR EL PROCEDO ?", function (e) {
            if (e) {
                alertify.success("PROCESO CANCELADO");
                formularioPreguntaEstandar();
            }
        });
    })

    function formularioPreguntaEstandar(){
        $("#configurarPreguntas .codigoPregunta").val("");
        $("#configurarPreguntas .nombrePregunta").val("");
        $("#configurarPreguntas .ordenPregunta").val("");
        $("#configurarPreguntas .cuestionarioPregunta").val(0);
        $("#configurarPreguntas .grupoPregunta").val(0);

        $("#configurarPreguntas .guardarPregunta").html("GUARDAR");
    }


    $("#configurarPreguntas .guardarPregunta").on('click', function(){

        var procedimiento = $("#configurarPreguntas .guardarPregunta").html();
        var idPregunta = $("#configurarPreguntas .codigoPregunta").val();

        var nombrePregunta = $("#configurarPreguntas .nombrePregunta").val();
        var ordenPregunta  = $("#configurarPreguntas .ordenPregunta").val();
        var cuestionarioPregunta = $("#configurarPreguntas .cuestionarioPregunta").val();
        var grupoPregunta = $("#configurarPreguntas .grupoPregunta").val();

        if(nombrePregunta != "" && ordenPregunta != ""){

            alertify.confirm("¿ SEGURO DE GUARDAR EL REGISTRO ?", function (e) {
                if (e) {

                    var objetoFormulario = new Object()
                        objetoFormulario.procedimiento = procedimiento;
                        objetoFormulario.idPregunta = idPregunta;
                        objetoFormulario.nombrePregunta = nombrePregunta;
                        objetoFormulario.ordenPregunta = ordenPregunta;
                        objetoFormulario.cuestionarioPregunta = cuestionarioPregunta;
                        objetoFormulario.grupoPregunta = grupoPregunta;

                    service.procesar("saveFormularioPregunta",objetoFormulario,cargaFormularioPregunta);

                    alertify.success("HA ACEPTADO GUARDAR EL REGISTRO");

                } else {
                    alertify.error("HA CANCELADO EL PROCESO");
                }
            });

        }else{
            alertify.error("COMPLETAR INFORMACION DEL FORMULARIO");
        }

    })

    function cargaFormularioPregunta(evt){
        formularioPreguntaEstandar();
        cargaConsultasIniciales();
    }









    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


