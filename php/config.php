<?php

define('ZONA_HORARIA','America/Lima');

define("PRODUCTION_SERVER", false);

if(PRODUCTION_SERVER){
	
	define('CANVAS_URL', 'http://igroupsac.com/sistema' );
	define('PHPDIR', dirname(__FILE__).'/');
	define('INCDIR', PHPDIR.'includes/');
	define('REPDIR', 'webservice/reportes/');
}else{
	define('CANVAS_URL', 'http://localhost/mayorsa' );
	define('PHPDIR', dirname(__FILE__).'/');
	define('INCDIR', PHPDIR.'includes/');
	define('REPDIR', 'webservice/reportes/');
}


//--
//-- AMFPHP stuff
//--

define('AMFCORE', INCDIR.'amfphp/core/'); //  directorio de las clases de amfphp
define('SRVPATH', PHPDIR.'services/'); // carpeta donde estan los servicios del amfphp
define('VOPATH', PHPDIR.'services/vo/'); // carpeta donde estan los vo que usan los servicios

//--
//-- MySQL stuff
//--

if(PRODUCTION_SERVER)
{

	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASS", "icount01072014$");
	define("DB_NAME", "mayorsa");

}
else
{
	
	define("DB_HOST", "localhost");
	define("DB_USER", "root");
	define("DB_PASS", ""); //icount01072014$
	define("DB_NAME", "mayorsa");

	define('DB_BACKUP_TABLES', 'area_rango,asistencia,auditoria,captura,huellas,usuario,justificacion,log,permisos,tienda,tipousuario,reporte,reportecuestionario,reportegrupo,reportepregunta,reporterespuesta,respuestasinforme,respuestaspreguntas');
	define('DB_BACKUP_DEVICE_USB', 'K'); //AQUI LA UNIDAD DEL USB EN DONDE SE GUARDARA EL BACKUP
	define('DB_BACKUP_DEVICE_LOCAL', 'E'); //UNIDAD LOCAL SI NO ENCUENTRA EL USB

	define('FILE_PENDING', '../../proyecto/descarga');
	define('FILE_FORWARD', '../../proyecto/descargasold');
	define('FILE_PENDING_0', '../proyecto/descarga');
}

//--
//-- Log stuff
//--

// la carpeta logs debe tener permisos de escritura si se va utilizar un archivo de log
define('LOG_DIR', PHPDIR.'logs' );
define('LOG_BASE', 'log_app' );

?>