<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceReportes extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

	function getListaUsuariosxEstado($dato){
		$sql = "SELECT A.*, B.descripcionEstadoUsuario, C.descripcionTipoUsuario FROM usuario A LEFT JOIN estadousuario B
				ON A.estadoUsuario = B.idEstadoUsuario LEFT JOIN tipousuario C
				ON A.tipoUsuario = C.idTipoUsuario
				WHERE A.estadoUsuario = $dato AND A.tipoUsuario != 4";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("tipoUsuario","claveUsuario","nombreUsuario","descripcionEstadoUsuario","descripcionTipoUsuario"));
		return $res;
	}

	function getListaDetalladaCapturasxArea($dato){
		$area_cap = $dato;
		$condicion = "";

		if ($area_cap > 0){
			$condicion = "WHERE area_cap IN ($dato)";
		}

		if (strpos($dato, '-') !== false) {
		    $inicio = substr($dato,0,strpos($dato, '-'));
		    $final = substr($dato,strpos($dato, '-')+1,10);
		    $condicion = "WHERE area_cap BETWEEN $inicio AND $final";
		}else{
			$condicion = "WHERE area_cap IN ($dato)";
		}

		$sqlRegistros = "SELECT A.*, B.nombreUsuario, C.des_barra FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
				ORDER BY A.id_captura ASC";
		$resRegistros = $this->db->get_results($sqlRegistros);
		$this->_codificarObjeto($resRegistros,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

		$sqlAreas = "SELECT area_cap, SUM(cant_cap) sum_cant FROM captura 
					$condicion
					GROUP BY area_cap
					ORDER BY area_cap ASC";
		$resAreas = $this->db->get_results($sqlAreas);

		$reporte = new stdClass();
        $reporte->registros = $resRegistros;
        $reporte->areas = $resAreas;

		return $reporte;
	}

	function getListaConsolidadaCapturasxArea($dato){

		$area_cap = $dato;
		$condicion = "";

		if ($area_cap > 0){
			$condicion = "WHERE area_cap IN ($dato)";
		}

		if (strpos($dato, '-') !== false) {
		    $inicio = substr($dato,0,strpos($dato, '-'));
		    $final = substr($dato,strpos($dato, '-')+1,10);
		    $condicion = "WHERE area_cap BETWEEN $inicio AND $final";
		}else{
			$condicion = "WHERE area_cap IN ($dato)";
		}

		$sqlRegistros = "SELECT A.*, SUM(A.cant_cap) sum_cant, B.nombreUsuario, C.des_barra FROM captura A LEFT JOIN usuario B
                ON A.usuario = B.dniUsuario LEFT JOIN maestro C
                ON A.barra_cap = C.cod_barra $condicion
                GROUP BY A.area_cap, A.barra_cap
				ORDER BY A.id_captura ASC";

		$resRegistros = $this->db->get_results($sqlRegistros);
		$this->_codificarObjeto($resRegistros,array("fecha","hora","cant_cap","nombreUsuario","des_barra"));

		$sqlAreas = "SELECT area_cap, SUM(cant_cap) sum_cant FROM captura 
					$condicion 
					GROUP BY area_cap
					ORDER BY area_cap ASC";

		$resAreas = $this->db->get_results($sqlAreas);

		$reporte = new stdClass();
        $reporte->registros = $resRegistros;
        $reporte->areas = $resAreas;


		return $reporte;
	}	

	function getListaReporteAreaRango(){
		$sqlRangos = "SELECT * FROM area_rango ORDER BY area_ini_ran ASC";
		$resRangos = $this->db->get_results($sqlRangos);
		$this->_codificarObjeto($resRangos,array("des_area_ran"));

		$sqlCaptura = "SELECT DISTINCT area_cap FROM captura ORDER BY area_cap ASC";
		$resCaptura = $this->db->get_results($sqlCaptura);

		$sqlJustificados = "SELECT * FROM justificacion
							WHERE lote NOT IN (SELECT DISTINCT area_cap FROM captura)";
		$resJustificados = $this->db->get_results($sqlJustificados);
		$this->_codificarObjeto($resJustificados,array("tipo","justificacion"));

		$resultado = new stdClass();
        $resultado->rangos = $resRangos;
        $resultado->capturas = $resCaptura;
        $resultado->justificados = $resJustificados;

		return $resultado;
	}

	function getPorcentajeAvance(){

		$sqlavance = "	SELECT ((area_fin_ran - area_ini_ran)+1) cantidad,COUNT(b.lote) avance FROM area_rango a LEFT JOIN
						(
						SELECT DISTINCT area_cap AS lote FROM captura
						UNION DISTINCT
						SELECT lote FROM justificacion
						) b
						ON b.lote BETWEEN a.area_ini_ran AND a.area_fin_ran
						GROUP BY a.idAreaRango";
		$resavance = $this->db->get_results($sqlavance);

		$avance = 0;
		$cantidad = 0;
		$porcentaje = 0;

		foreach ($resavance as $registro) {
            $avance = $avance + $registro->avance;
            $cantidad = $cantidad + $registro->cantidad;
        }
		/*
		$sqlRangos = "SELECT * FROM area_rango ORDER BY area_ini_ran ASC";
		$resRangos = $this->db->get_results($sqlRangos);
		$this->_codificarObjeto($resRangos,array("des_area_ran"));

		$sqlCaptura = "SELECT DISTINCT area_cap FROM captura ORDER BY area_cap ASC";
		$resCaptura = $this->db->get_results($sqlCaptura);

		$sqlJustificados = "SELECT * FROM justificacion
							WHERE lote NOT IN (SELECT DISTINCT area_cap FROM captura)";
		$resJustificados = $this->db->get_results($sqlJustificados);
		$this->_codificarObjeto($resJustificados,array("tipo","justificacion"));

    	$dataRangos = $resRangos;
    	$dataCapturas = $resCaptura;
    	$dataJustificados = $resJustificados;

    	$cantidad_total = 0;
    	$avance_total = 0;

    	$cuentaRangos = count($dataRangos);

        for($i=0 ; $i < $cuentaRangos ; $i++){
        	$filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
            $filaDescripcion = $dataRangos[$i]->des_area_ran;
            $cantidad = ( (int)$filaFinal - (int)$filaInicio ) + 1;
            $avance = 0;
            $porcentaje = 0;
            for ($z=$filaInicio; $z<=$filaFinal; $z++) { 
                for ($y=0; $y<count($dataCapturas); $y++) {
                    $valorArea = (int)$dataCapturas[$y]->area_cap;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }
                for ($x=0; $x<count($dataJustificados); $x++) {
                    $valorArea = (int)$dataJustificados[$x]->lote;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }
            }

            $cantidad_total = $cantidad_total + $cantidad;
            $avance_total = $avance_total + $avance;

            $porcentaje = round(($avance / $cantidad)*100,2);


            if($valor_avance_total != ""){
            	$valor_avance_total .= ",";
            }

            $valor_avance_total .= "('$filaDescripcion',$porcentaje,'$cliente', '$tienda','$fecha')"; 

        }

        $valor_avance_total .= ";";
		*/
	
        $porcentaje_total = round(($avance / $cantidad)*100,2);


        return $porcentaje_total;

	}


	function getListaModalReporteAreaRango($areaRango){
		$condicion = "";
		if($areaRango != ""){
			$condicion = "WHERE idAreaRango = $areaRango";
		}

		$sqlRangos = "SELECT * FROM area_rango
						$condicion
						ORDER BY area_ini_ran ASC";
		$resRangos = $this->db->get_results($sqlRangos);
		$this->_codificarObjeto($resRangos,array("des_area_ran"));

		$sqlCaptura = "SELECT DISTINCT area_cap FROM captura ORDER BY area_cap ASC";
		$resCaptura = $this->db->get_results($sqlCaptura);

		$sqlJustificados = "SELECT * FROM justificacion
							WHERE lote NOT IN (SELECT DISTINCT area_cap FROM captura)";
		$resJustificados = $this->db->get_results($sqlJustificados);
		$this->_codificarObjeto($resJustificados,array("tipo","justificacion"));

		$resultado = new stdClass();
        $resultado->rangos = $resRangos;
        $resultado->capturas = $resCaptura;
        $resultado->justificados = $resJustificados;

		return $resultado;
	}

	function getListaReporteTienda(){
		$sql = "SELECT * FROM tienda ORDER BY idTienda ASC LIMIT 1";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("nombreTienda"));
		return $res;
	}

	function getReporteDiferenciaJerarquia(){
		$sql="	SELECT CONCAT(TRIM(dep_stk),' - ',des_dep_stk) AS jerarquias,
				ROUND(SUM(stock),3) AS unidades_cliente, ROUND(SUM(total_stock_sol),3) AS soles_cliente,
				ROUND(SUM(contado),3) AS unidades_igroup, ROUND(SUM(total_cap_sol),3) soles_igroup,
				ROUND((SUM(contado) - SUM(stock)),3) AS diferencia_unidades,
				ROUND((SUM(total_cap_sol) - SUM(total_stock_sol)),3) AS diferencia_soles FROM diferencias
				GROUP BY dep_stk";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("jerarquias"));
		return $res;
	}

	function getReporteDiferenciaSubJerarquia(){
		$sql="SELECT CONCAT(TRIM(sub_dep_stk),' - ',des_sub_dep_stk) AS jerarquias,
				ROUND(SUM(stock),3) AS unidades_cliente, ROUND(SUM(total_stock_sol),3) AS soles_cliente,
				ROUND(SUM(contado),3) AS unidades_igroup, ROUND(SUM(total_cap_sol),3) soles_igroup,
				ROUND((SUM(contado) - SUM(stock)),3) AS diferencia_unidades,
				ROUND((SUM(total_cap_sol) - SUM(total_stock_sol)),3) AS diferencia_soles FROM diferencias
				GROUP BY sub_dep_stk";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("subjerarquias"));
		return $res;
	}

	function getReporteDiferenciaGrupos(){
/*		$sql="	SELECT CASE 
	WHEN SUBSTR(dep_stk,2,2)<03 THEN 'PGC'
	WHEN SUBSTR(dep_stk,2,2)>02 AND SUBSTR(dep_stk,2,2)<08 THEN 'PERECIBLES '	
	WHEN SUBSTR(dep_stk,2,2)>07 AND SUBSTR(dep_stk,2,2)<12 THEN 'NON FOOD'
END AS JERARQUIAS
,
CASE
	WHEN SUBSTR(dep_stk,1,3)='J01' THEN 'J01 - PGC COMESTIBLE'
	WHEN SUBSTR(dep_stk,1,3)='J02' THEN 'J02 - PGC NO COMESTIBLE'
	WHEN SUBSTR(dep_stk,1,3)='J03' THEN 'J03 - CARNES Y PESCADOS'
	WHEN SUBSTR(dep_stk,1,3)='J04' THEN 'J04 - FRUTAS Y VERDURAS'
	WHEN SUBSTR(dep_stk,1,3)='J05' THEN 'J05 - FLC'
	WHEN SUBSTR(dep_stk,1,3)='J06' THEN 'J06 - PANADERIA Y PASTELERIA'
	WHEN SUBSTR(dep_stk,1,3)='J07' THEN 'J07 - PLATOS PREPARADOS'
	WHEN SUBSTR(dep_stk,1,3)='J08' THEN 'J08 - VESTUARIO'
	WHEN SUBSTR(dep_stk,1,3)='J09' THEN 'J09 - HOGAR'
	WHEN SUBSTR(dep_stk,1,3)='J10' THEN 'J10 - BAZAR'
	WHEN SUBSTR(dep_stk,1,3)='J11' THEN 'J11 - ELECTROHOGAR'
END AS DIVISION,
				ROUND(SUM(stock),3) AS unidades_cliente, ROUND(SUM(total_stock_sol),3) AS soles_cliente,
				ROUND(SUM(contado),3) AS unidades_igroup, ROUND(SUM(total_cap_sol),3) soles_igroup,
				ROUND((SUM(contado) - SUM(stock)),3) AS diferencia_unidades,
				ROUND((SUM(total_cap_sol) - SUM(total_stock_sol)),3) AS diferencia_soles FROM diferencias
				WHERE SUBSTR(dep_stk,2,2)<12
				GROUP BY SUBSTR(dep_stk,1,3)";*/
		$sql="	SELECT SUBSTR(D.dep_stk,1,3) AS idGrupo, G.jerarquias, G.division,ROUND(SUM(D.stock),3) AS unidades_cliente,ROUND(SUM(D.total_stock_sol),3) AS soles_cliente,ROUND(SUM(D.contado),3) AS unidades_igroup,ROUND(SUM(D.total_cap_sol),3) AS soles_igroup,ROUND(SUM(D.dif_cant),3) AS diferencia_unidades,ROUND(SUM(D.dif_sol),3) AS diferencia_soles FROM diferencias D LEFT JOIN grupos G
				ON SUBSTR(D.dep_stk,1,3) = G.idGrupo
				WHERE G.jerarquias IS NOT NULL
				GROUP BY SUBSTR(D.dep_stk,1,3)
				ORDER BY SUBSTR(D.dep_stk,1,3) ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("idGrupo","jerarquias","division"));
		return $res;
	}

	function getReporteDiferencia($monto,$jera,$subjera){
		$jerarquia = "";
		if($jera != '0'){
			$jerarquia = " AND dep_stk = '".$jera."'";
		}

		$subjerarquia = "";
		if($subjera != '0'){
			$subjerarquia = " AND sub_dep_stk = '".$subjera."'";
		}

		$sql="	SELECT sku_stk, barra_cap, des_sku_stk, stock, contado, dif_cant, dif_sol FROM diferencias
				WHERE dif_sol_pos >= $monto $jerarquia $subjerarquia
				ORDER BY dif_sol_pos DESC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_sku_stk"));
		return $res;
	}

	function getReporteDiferenciaSku($limite){
		$sql="	SELECT * FROM diferencias
				ORDER BY dif_sol_pos DESC
				LIMIT $limite";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_sku_stk","des_dep_stk"));
		return $res;
	}

	function getReporteDiferenciaCapturaDetalle($barra){
		$sql="	SELECT barra_cap, area_cap, SUM(cant_cap) cant_cap FROM captura
				WHERE sku_cap = '$barra'
				GROUP BY sku_cap,area_cap
				ORDER BY area_cap ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getReporteDiferenciaAreaRango(){
		$sql="	SELECT * FROM area_rango";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_area_ran"));
		return $res;
	}

	function getReporteProductividad($estado){
		$sql="	SELECT U.dniUsuario, U.nombreUsuario,
				(SELECT inicioAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia ASC LIMIT 1) inicioAsistencia, 
				(SELECT terminoAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia DESC LIMIT 1) terminoAsistencia,
				SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60) horas_conteo,
				(SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) total_conteo,
				((SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) / SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60)) conteo_x_hora 
				FROM usuario U LEFT JOIN asistencia A
				ON U.dniUsuario = A.dniUsuario
				WHERE U.estadoUsuario = $estado AND U.tipoUsuario != 4
				GROUP BY U.dniUsuario";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getReporteProductividadActualizado(){
		$sql="	SELECT C.usuario, U.nombreUsuario,
				COUNT(C.id_captura) registros, 
				SUM(C.cant_cap) unidades, 
				MIN(CONCAT(C.fecha,' ',C.hora)) fecha_inicio, MAX(CONCAT(C.fecha,' ',C.hora)) fecha_final,
				TIMEDIFF(MAX(CONCAT(C.fecha,' ',C.hora)),MIN(CONCAT(C.fecha,' ',C.hora))) tiempo_total,
				ROUND((TIME_TO_SEC(TIMEDIFF(MAX(CONCAT(C.fecha,' ',C.hora)),MIN(CONCAT(C.fecha,' ',C.hora))))/60)+1,0) minutos_total
				FROM captura C LEFT JOIN usuario U
				ON C.usuario = U.idUsuario
				GROUP BY C.usuario";
		$res = $this->db->get_results($sql);

		$resultado = Array();
		for ($i=0; $i < count($res); $i++) { 
			$usuario = $res[$i]->usuario;
			$minutos_trabajados = 0;
			$minutos_no_trabajados = 0;

			$sql_minutos_trabajados = "	SELECT CONCAT(fecha,' ',CONCAT(SUBSTR(hora,1,5),':00')) minuto_trabajado 
										FROM captura
										WHERE usuario = '$usuario'
										GROUP BY CONCAT(fecha,' ',CONCAT(SUBSTR(hora,1,5),':00'))
										ORDER BY CONCAT(fecha,' ',CONCAT(SUBSTR(hora,1,5),':00')) ASC";
			$res_minutos_trabajados = $this->db->get_results($sql_minutos_trabajados);

			$fila = new stdClass();
	        $fila->usuario = $res[$i]->usuario;
	        $fila->nombreUsuario = $res[$i]->nombreUsuario;
	        $fila->registros = $res[$i]->registros;
	        $fila->unidades = $res[$i]->unidades;
	        $fila->fecha_inicio = $res[$i]->fecha_inicio;
	        $fila->fecha_final = $res[$i]->fecha_final;
	        $fila->tiempo_total = $this->horaConverter($res[$i]->minutos_total);
	        $fila->minutos_total = $res[$i]->minutos_total;

	        $fila->minutos_trabajados = $this->horaConverter(count($res_minutos_trabajados));
	        $fila->minutos_no_trabajados = $this->horaConverter($res[$i]->minutos_total - count($res_minutos_trabajados));

	        $fila->registros_por_hora = ROUND(($res[$i]->registros / $res[$i]->minutos_total)*60,2);
	        $fila->unidades_por_hora = ROUND(($res[$i]->unidades / $res[$i]->minutos_total)*60,2);

	        $resultado[] = $fila;

		}

		return $resultado;
	}

	function horaConverter($tiempo){
		$horas = floor($tiempo / 60);
		$minutos = floor($tiempo - ($horas * 60));

		if(strlen($horas) == 1){ $horas = "0".$horas; }
		if(strlen($minutos) == 1){ $minutos = "0".$minutos; }
 
		return $horas . ':' . $minutos;
	}

	function getTotalContado(){
		$sql_contados = "	SELECT SUM(C.cant_cap) contado, ROUND(SUM(C.cant_cap * M.Prec_barra),3) valorado 
							FROM captura C LEFT JOIN maestro M
							ON C.barra_cap = M.cod_barra";
		$res_contados = $this->db->get_results($sql_contados);


		$sql_eliminados = "	SELECT SUM(IF(A.cant_cap_ant < 0,(A.cant_cap_ant * (-1)),A.cant_cap_ant)) contado, ROUND(SUM((IF(A.cant_cap_ant < 0,(A.cant_cap_ant * (-1)),A.cant_cap_ant)) * M.Prec_barra),3) valorado 
							FROM auditoria A LEFT JOIN maestro M
							ON A.barra_cap = M.cod_barra
							WHERE A.tipo = 'ELIMINADO'";
		$res_eliminados = $this->db->get_results($sql_eliminados);

		$sql_editados = "	SELECT SUM(IF( (A.cant_cap_ant - A.cant_cap_act) < 0,( (A.cant_cap_ant - A.cant_cap_act) * (-1)), (A.cant_cap_ant - A.cant_cap_act) )) contado, ROUND(SUM((IF( (A.cant_cap_ant - A.cant_cap_act) < 0,( (A.cant_cap_ant - A.cant_cap_act) * (-1)), (A.cant_cap_ant - A.cant_cap_act) )) * M.Prec_barra),3) valorado 
							FROM auditoria A LEFT JOIN maestro M
							ON A.barra_cap = M.cod_barra
							WHERE A.tipo = 'EDITADO'";
		$res_editados = $this->db->get_results($sql_editados);


		$sql_eliminados_cliente = "	SELECT SUM(IF(A.cant_cap_ant < 0,(A.cant_cap_ant * (-1)),A.cant_cap_ant)) contado, ROUND(SUM((IF(A.cant_cap_ant < 0,(A.cant_cap_ant * (-1)),A.cant_cap_ant)) * M.Prec_barra),3) valorado 
									FROM auditoria A LEFT JOIN maestro M
									ON A.barra_cap = M.cod_barra
									WHERE A.tipo = 'ELIMINADO' AND responsable = 'CLIENTE'";
		$res_eliminados_cliente = $this->db->get_results($sql_eliminados_cliente);

		$sql_editados_cliente = "	SELECT SUM(IF( (A.cant_cap_ant - A.cant_cap_act) < 0,( (A.cant_cap_ant - A.cant_cap_act) * (-1)), (A.cant_cap_ant - A.cant_cap_act) )) contado, ROUND(SUM((IF( (A.cant_cap_ant - A.cant_cap_act) < 0,( (A.cant_cap_ant - A.cant_cap_act) * (-1)), (A.cant_cap_ant - A.cant_cap_act) )) * M.Prec_barra),3) valorado 
									FROM auditoria A LEFT JOIN maestro M
									ON A.barra_cap = M.cod_barra
									WHERE A.tipo = 'EDITADO' AND responsable = 'CLIENTE'";
		$res_editados_cliente = $this->db->get_results($sql_editados_cliente);


		$resultado = new stdClass();
        $resultado->contados = $res_contados;
        $resultado->editados = $res_editados;
        $resultado->eliminados = $res_eliminados;
        $resultado->editados_cliente = $res_editados_cliente;
        $resultado->eliminados_cliente = $res_eliminados_cliente;

		return $resultado;

	}

	function getPreguntasCuestionario($reporte,$cuestionario){
		$sql = "SELECT RP.idPregunta, UPPER(RP.pregunta) pregunta, RRR.idRespuesta, UPPER(RRR.respuesta) respuesta FROM reportepregunta RP LEFT JOIN reportecuestionario RC
				ON RP.idCuestionario = RC.idCuestionario LEFT JOIN respuestaspreguntas RPP
				ON RP.idPregunta = RPP.idPregunta LEFT JOIN reporterespuesta RRR
				ON RPP.idRespuesta = RRR.idRespuesta
				WHERE RC.idReporte = $reporte AND RP.idCuestionario = $cuestionario
				ORDER BY RP.orden ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("pregunta","respuesta"));
		return $res;
	}

	function getDatosInforme($informe){
		$sql = "SELECT * FROM respuestasinforme
				WHERE idInforme = $informe";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("responsableIgroup","responsableCliente","encargadoCliente","supervisorIgroup","observaciones"));
		return $res;	
	}

	function getAvanceTotal(){
		$sql = "SELECT ROUND(SUM(stock),2) stock, ROUND(SUM(contado),2) captura, ROUND(SUM(total_stock_sol),2) stock_sol, ROUND(SUM(total_cap_sol),2) captura_sol FROM diferencias";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getDiferenciaTotal(){
		$sql = "SELECT IF(SUM(dif_sol) < 0,CONCAT(des_dep_stk,' (-)'),des_dep_stk) departamento, des_dep_stk departamento_original, ROUND(IF(SUM(dif_sol) < 0,SUM(dif_sol) * (-1),SUM(dif_sol)),0) diferencia_soles, ROUND(IF(SUM(dif_cant) < 0,SUM(dif_cant) * (-1),SUM(dif_cant)),0) diferencia_cantidad FROM diferencias
				GROUP BY des_dep_stk
				ORDER BY SUM(dif_sol) DESC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("departamento"));
		return $res;
	}

	function getRankingProductos($departamento){
		$sql = "SELECT sku_stk, des_sku_stk,costo,stock,contado,dif_cant, dif_sol, ROUND(dif_sol_pos,2) dif_sol_pos, des_dep_stk, des_sub_dep_stk,des_clas_stk FROM diferencias
				WHERE des_dep_stk = '$departamento'
				ORDER BY dif_sol_pos DESC
				LIMIT 20";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("sku_stk","des_sku_stk","des_dep_stk","des_sub_dep_stk","des_clas_stk"));
		return $res;
	}

	function getSubDiferenciaTotal(){
		$sql = "SELECT des_dep_stk departamento,IF(SUM(dif_sol) < 0,CONCAT(des_sub_dep_stk,' (-)'),des_sub_dep_stk) sub_departamento, ROUND(IF(SUM(dif_sol) < 0,SUM(dif_sol) * (-1),SUM(dif_sol)),0) diferencia_soles, ROUND(IF(SUM(dif_cant) < 0,SUM(dif_cant) * (-1),SUM(dif_cant)),0) diferencia_cantidad FROM diferencias
				GROUP BY des_dep_stk, des_sub_dep_stk
				ORDER BY SUM(dif_sol) DESC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("departamento","sub_departamento"));
		return $res;
	}


}	
?>