<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');
require_once("Service.php");

class ServiceAdministrativo extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

//PARTE INICIAL DE ADMINISTRATIVO	

	function getMenu($permisos){

		$sql = "SELECT idPerfil,enlace,titulo FROM permisos WHERE idPerfil IN ($permisos) ORDER BY idPerfil ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function getControles($permisos){

		$sql = "SELECT distinct control FROM permisos WHERE idPerfil IN ($permisos) ORDER BY idPerfil ASC";
		$res = $this->db->get_results($sql);
		return $res;
	}

	function archivosPendientes(){

		$total_archivos = count(glob(FILE_PENDING_0.'/'.'{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function archivosDescargados(){

		$total_pendientes = count(glob(FILE_PENDING_0.'/'.'{*.txt}',GLOB_BRACE));
		$total_procesados = count(glob('archivos_sistema/archivos_procesados/{*.txt}',GLOB_BRACE));
		$total_archivos = $total_pendientes + $total_procesados;

		$resultado = new stdClass();
		$resultado->pendientes = $total_pendientes;
		$resultado->procesados = $total_procesados;
		$resultado->total = $total_archivos;

		return $resultado;

	}


	function archivosReenviados(){

		$total_archivos = count(glob(FILE_FORWARD.'/'.'{*.txt}',GLOB_BRACE));
		return $total_archivos;

	}

	function conflictoBarras(){

		$sql = "UPDATE captura C SET C.sku_cap = ( SELECT M.sku_barra FROM maestro M WHERE M.cod_barra = C.barra_cap LIMIT 1) WHERE C.sku_cap = ''";
		$res=$this->db->query($sql);

		$sql_consulta = "SELECT COUNT(*) AS cuenta FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_var($sql_consulta);

		return $res_consulta;

	}

	function listarConflictoBarras(){

		$sql_consulta = "SELECT area_cap,barra_cap,cant_cap FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_results($sql_consulta);
		return $res_consulta;

	}

	function getAvanceCaptura(){
		$sqlavance = "	SELECT ((area_fin_ran - area_ini_ran)+1) cantidad,COUNT(b.lote) avance FROM area_rango a LEFT JOIN
						(
						SELECT DISTINCT area_cap AS lote FROM captura
						UNION DISTINCT
						SELECT lote FROM justificacion
						) b
						ON b.lote BETWEEN a.area_ini_ran AND a.area_fin_ran
						GROUP BY a.idAreaRango";
		$resavance = $this->db->get_results($sqlavance);

		$avance = 0;
		$cantidad = 0;
		$porcentaje = 0;

		foreach ($resavance as $registro) {
            $avance = $avance + $registro->avance;
            $cantidad = $cantidad + $registro->cantidad;
        }
		//$this->_codificarObjeto($resRangos,array("des_area_ran"));
		/*
		$sqlRangos = "SELECT * FROM area_rango ORDER BY area_ini_ran ASC";
		$resRangos = $this->db->get_results($sqlRangos);
		$this->_codificarObjeto($resRangos,array("des_area_ran"));

		$sqlCaptura = "SELECT DISTINCT area_cap FROM captura ORDER BY area_cap ASC";
		$resCaptura = $this->db->get_results($sqlCaptura);

		$sqlJustificados = "SELECT * FROM justificacion";
		$resJustificados = $this->db->get_results($sqlJustificados);
		$this->_codificarObjeto($resJustificados,array("ubicacion","justificacion"));

		$dataRangos = $resRangos;
        $dataCapturas = $resCaptura;
        $dataJustificados = $resJustificados;



        $cuentaRangos = count($dataRangos);


        $porcentaje = 0;
        $cantidad = 0;
		$avance = 0;

        for($i=0 ; $i < $cuentaRangos ; $i++){

            $filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
                        
            for ($z=$filaInicio; $z<=$filaFinal; $z++) { 
            	$cantidad = $cantidad + 1;

                for ($y=0; $y<count($dataCapturas); $y++) {
                    $valorArea = (int)$dataCapturas[$y]->area_cap;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }

                for ($x=0; $x<count($dataJustificados); $x++) {
                    $valorArea = (int)$dataJustificados[$x]->lote;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }


            }

        }
        */

        $porcentaje = round(($avance / $cantidad)*100,2);

		return $porcentaje;
	}


	function buscarLote($lote){

		$sql = "SELECT C.barra_cap, SUBSTRING(M.des_barra,1,15) descripcion, C.cant_cap 
				FROM captura C LEFT JOIN maestro M
				ON C.barra_cap = M.cod_barra
				WHERE C.area_cap = $lote";
		$res = $this->db->get_results($sql);
		return $res;
	}


}	
?>