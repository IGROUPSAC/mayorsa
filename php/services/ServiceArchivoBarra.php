<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceArchivoBarra extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function saveRegistrosBarra($data){
        $c = $g = $f = 0;
        $detalle = "RESUMEN DE CARGA ARCHIVO BARRA \r\n";
        $detalle .= "LINEAS NO CARGADAS \r\n";

        $barra = file("../archivos_sistema/archivos_barra/".$data->archivo);
        $barra_log = fopen("../archivos_sistema/archivos_barra/log_".$data->archivo, "w");

        //$registros = array_count_values($maestro);
        
        foreach ($barra as $fila => $valor){
            $c++;

            $valor = str_replace("'","",$valor);
            //$valor = str_replace("|","','",$valor);
            //echo $valor;
            //$registro = "('".$valor."');";
            $cadena = explode("|",$valor);

            $registro = "(".$data->$lote.",'".implode("','", $cadena)."',6,'99999999','".date("Y-m-d")."','".date("H:i:s")."','9999999999999','999999')";

            $sql = "INSERT INTO captura (area_cap, barra_cap, cant_cap, tip_cap, usuario, fecha, hora, descargado, namepalm) VALUES $registro";
            $res=$this->db->query($sql);

            if($res){
                $g++;
            }else{
                $f++;
                $detalle .= "LINEA : ".$c." - DETALLE : ".$registro." \r\n";
            }

        }

        //$sql = "UPDATE captura C SET C.sku_cap = ( SELECT M.sku_barra FROM maestro M WHERE M.cod_barra = C.barra_cap LIMIT 1)";
        //$res=$this->db->query($sql);

        $detalle .= " \r\n";
        $detalle .= "FILAS RECORRIDAS : ".$c." \r\n";
        $detalle .= "REGISTROS GUARDADOS : ".$g." \r\n";
        $detalle .= "REGISTROS FALLIDOS : ".$f;

        fwrite($barra_log, $detalle);
        fclose($barra_log);

        return $c;
    }

    function listarArchivosBarraPendientes(){

        $archivos = array();

        $directorio = opendir("../archivos_sistema/archivos_barra"); //ruta actual
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (is_dir($archivo))//verificamos si es o no un directorio
            {
                //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
            }
            else
            {

                $esArchivo = strpos($archivo, "log");

                if ($esArchivo === false) {

                    $bytes = filesize("../archivos_sistema/archivos_barra/".$archivo);
                    $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
                    for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
                    $peso = ( round( $bytes, 2 ) . " " . $label[$i] );


                    $file = new stdClass();
                    $file->nombre = $archivo;
                    $file->log = "log_".$archivo;
                    $file->peso = $peso;
                    $file->fecha = date("Y-m-d", filectime("../archivos_sistema/archivos_barra/".$archivo));

                    $archivos[] = $file;

                }
            }

            
        }

        return $archivos;

    }

    function eliminarArchivoBarraPendiente($dato){
        unlink("../archivos_sistema/archivos_barra/".$dato);
        unlink("../archivos_sistema/archivos_barra/log_".$dato);
        return 1;

    }




}	
?>