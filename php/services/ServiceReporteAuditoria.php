
<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceReporteAuditoria extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getDescargaReporteAuditoria($responsable,$tipo){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");
    	$qresponsable = "";
    	$qtipo = "";
    	if($responsable != ""){ $qresponsable = "AND a.responsable = '$responsable'"; }
    	if($tipo != ""){ $qtipo = "AND a.tipo = '$tipo'"; }

		$sql="	SELECT a.idAuditoria, a.area_cap, a.barra_cap, a.cant_cap_ant, a.cant_cap_act, a.tip_cap, a.tipo, a.usuario, a.responsable, a.fecha,
				c.usuario, m.jerar, m.des_jerar, ar.des_area_ran
				FROM auditoria a LEFT JOIN captura c
				ON a.area_cap = c.area_cap LEFT JOIN maestro m
				ON c.barra_cap = m.cod_barra LEFT JOIN area_rango ar
				ON a.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran
				WHERE a.idAuditoria > 0 $qresponsable $qtipo
				GROUP BY a.idAuditoria

				UNION DISTINCT 

				SELECT a.idAuditoria, a.area_cap, a.barra_cap, a.cant_cap_ant, a.cant_cap_act, a.tip_cap, a.tipo, a.usuario, a.responsable, a.fecha,
				c.usuario, m.jerar, m.des_jerar, ar.des_area_ran
				FROM auditoria a LEFT JOIN captura c
				ON a.area_cap = c.area_cap LEFT JOIN maestro m
				ON c.sku_cap = m.sku_barra LEFT JOIN area_rango ar
				ON a.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran
				WHERE a.idAuditoria > 0 $qresponsable $qtipo
				GROUP BY a.idAuditoria ";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_jerar","des_area_ran"));

		if($res){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel");

			$tituloReporte = "REPORTE DE AUDITORIA";
			$titulosColumnas = array('N','AREA_CAP','BARRA_CAP','CANT_CAP_ANT','CANT_CAP_ACT','DIFERENCIA','TIP_CAP','TIPO','RESPONSABLE','USUARIO','JERAR','DES_JERAR','AREA_RANGO');
			
			$objPHPExcel->setActiveSheetIndex(0)
	        		    ->mergeCells('A1:I1');
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1',$tituloReporte)
						->setCellValue('G1','FECHA : ')
						->setCellValue('H1',$fecha)
						->setCellValue('G2','HORA : ')
						->setCellValue('H2',$hora)
	        		    ->setCellValue('A3',  $titulosColumnas[0])
			            ->setCellValue('B3',  $titulosColumnas[1])
	        		    ->setCellValue('C3',  $titulosColumnas[2])
	            		->setCellValue('D3',  $titulosColumnas[3])
	            		->setCellValue('E3',  $titulosColumnas[4])
	            		->setCellValue('F3',  $titulosColumnas[5])
	            		->setCellValue('G3',  $titulosColumnas[6])
	            		->setCellValue('H3',  $titulosColumnas[7])
	            		->setCellValue('I3',  $titulosColumnas[8])
	            		->setCellValue('J3',  $titulosColumnas[9])
	            		->setCellValue('K3',  $titulosColumnas[10])
	            		->setCellValue('L3',  $titulosColumnas[11])
	            		->setCellValue('M3',  $titulosColumnas[12]);
			
			//Se agregan los datos de los alumnos
			$i = 4;

			for( $x = 0; $x < count($res); $x++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
	        		    ->setCellValue('A'.$i,  ($x + 1))
			            ->setCellValue('B'.$i,  $res[$x]->area_cap)
	        		    ->setCellValue('C'.$i,  $res[$x]->barra_cap)
	        		    ->setCellValue('D'.$i,  $res[$x]->cant_cap_ant)
	        		    ->setCellValue('E'.$i,  $res[$x]->cant_cap_act)
	        		    ->setCellValue('F'.$i,  ((float)$res[$x]->cant_cap_ant - (float)$res[$x]->cant_cap_act))
	        		    ->setCellValue('G'.$i,  $res[$x]->tip_cap)
	        		    ->setCellValue('H'.$i,  $res[$x]->tipo)
	        		    ->setCellValue('I'.$i,  $res[$x]->responsable)
	        		    ->setCellValue('J'.$i,  $res[$x]->usuario)
	        		    ->setCellValue('K'.$i,  $res[$x]->jerar)
	        		    ->setCellValue('L'.$i,  $res[$x]->des_jerar)
	        		    ->setCellValue('M'.$i,  $res[$x]->des_area_ran);
						$i++;
			}

			
			$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));
			 
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			for($i = 'A'; $i <= 'H'; $i++){
				$objPHPExcel->setActiveSheetIndex(0)			
					->getColumnDimension($i)->setAutoSize(TRUE);
			}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reportedeauditoria.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reportedeauditoria.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}


}	
?>