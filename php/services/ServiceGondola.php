<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceGondola extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function saveRegistrosGondola($data){
        $c = $g = $f = 0;
        $detalle = "RESUMEN DE CARGA ARCHIVO GONDOLA \r\n";
        $detalle .= "LINEAS NO CARGADAS \r\n";

        $gondola = file("../archivos_sistema/archivos_gondola/".$data->archivo);
        $gondola_log = fopen("../archivos_sistema/archivos_gondola/log_".$data->archivo, "w");

        $sql_truncate = "TRUNCATE TABLE gondola";
        $this->db->query($sql_truncate);
        
        foreach ($gondola as $fila => $valor){
            $c++;

            $valor = str_replace("'","",$valor);
            $valor = str_replace("\n","",$valor);
            $valor = str_replace("\r","",$valor);
            $valor = str_replace("\n\r","",$valor);
            //echo $valor;
            //$registro = "('".$valor."');";
            $cadena = explode("\t",$valor);

            $registro = "('".implode("','", $cadena)."')";
            
            $sql = "INSERT INTO gondola (sku_gondola, subclase, descripcion, proveedor, estado_gondola) VALUES $registro";
            
            $res=$this->db->query($sql);

            if($res){
                $g++;
            }else{
                $f++;
                $detalle .= "LINEA : ".$c." - DETALLE : ".$registro." \r\n";
            }

        }
        $detalle .= " \r\n";
        $detalle .= "FILAS RECORRIDAS : ".$c." \r\n";
        $detalle .= "REGISTROS GUARDADOS : ".$g." \r\n";
        $detalle .= "REGISTROS FALLIDOS : ".$f;

        fwrite($gondola_log, $detalle);
        fclose($gondola_log);

        return $c;
    }

    function comprobarInformacionArchivo(){
        $archivo = "../archivos_sistema/archivos_gondola/archivo_gondola.txt";
        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
        $conteo = count(file($archivo));

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

        return $archivos;
    }

    function comprobarInformacionArchivosGenerados($file){
        $archivo = "../archivos_sistema/archivos_gondola_generados/".$file.".txt";
        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
        $conteo = count(file($archivo));

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = ($conteo - 1);
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

        return $archivos;
    }

    function generarareadetalle(){
        $sql="  SELECT C.area_cap, M.sku_barra, C.barra_cap, M.des_barra, C.fecha, C.hora, C.cant_cap 
                FROM captura C LEFT JOIN maestro M
                ON C.barra_cap = M.cod_barra ";
        $res = $this->db->get_results($sql);

        $archivo = "../archivos_sistema/archivos_gondola_generados/area_detalle.txt";
        unlink('$archivo');

        $cadena="\r\n";
        $conteo=1;

        $cadena.="AREA"."\t"."SKU"."\t"."EAN"."\t"."DESCRIPCION"."\t"."FECHA"."\t"."HORA"."\t"."PISTOLEO";
        for($i=0;$i<count($res);$i++){
            $conteo++;

            //SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
            $cadena.="\r\n";
            $cadena.= $res[$i]->area_cap."\t".$res[$i]->sku_barra."\t".$res[$i]->barra_cap."\t".$res[$i]->des_barra."\t".$res[$i]->fecha."\t".$res[$i]->hora."\t".ROUND($res[$i]->cant_cap);
        }

        $cadenax = substr($cadena,1,strlen($cadena));
        $fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
        fwrite($fch, $cadenax); // Grabas
        fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

        return $archivos;
    }

    function generarcontadosurtido(){
        //LIMPIAR BASES TEMPORALES
        $sql="TRUNCATE capturas";
        $res=$this->db->query($sql);

        //GENERAR TABLA CAPTURAS
        $sql="  INSERT INTO capturas
                    SELECT 
                    A.id_captura,A.area_cap,A.barra_cap,
                    B.sku_barra AS sku_cap,
                    SUM(A.cant_cap) AS cant_cap,
                    A.tip_cap,A.usuario,A.fecha,A.hora,
                    B.jerar,B.des_jerar,B.sub_dep_mst,B.des_sub_dep_mst,B.clas_mst,B.des_clas_mst,B.sub_clas_mst,B.des_sub_clas_mst,
                    B.Prec_barra,B.des_barra
                    FROM captura A LEFT JOIN maestro B
                    ON A.barra_cap = B.cod_barra
                    GROUP BY B.sku_barra";
        $res=$this->db->query($sql);




        $sql="  SELECT C.sku_cap, C.sub_clas_mst, C.des_barra, G.estado_gondola, C.cant_cap 
                FROM capturas C LEFT JOIN gondola G
                ON C.sku_cap = G.sku_gondola ";
        $res = $this->db->get_results($sql);

        $archivo = "../archivos_sistema/archivos_gondola_generados/contado_surtido.txt";
        unlink('$archivo');

        $cadena="\r\n";
        $conteo=1;

        $cadena.="SKU"."\t"."SUB CLASE"."\t"."DESCRIPCION"."\t"."ESTADO"."\t"."PISTOLEO";
        for($i=0;$i<count($res);$i++){
            $conteo++;

            //SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
            $cadena.="\r\n";
            if($res[$i]->estado_gondola == ""){
                $estado = "SIN ESTADO";
            }else{
                $estado = $res[$i]->estado_gondola;
            }

            $cadena.= $res[$i]->sku_cap."\t".$res[$i]->sub_clas_mst."\t".$res[$i]->des_barra."\t".$estado."\t".ROUND($res[$i]->cant_cap);
        }

        $cadenax = substr($cadena,1,strlen($cadena));
        $fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
        fwrite($fch, $cadenax); // Grabas
        fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

        return $archivos;
    }

}	
?>