
<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceGenerarArchivos extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function generarArchivoUsuario($dato){
		$condicion = "";
		if($dato != ""){
			$condicion = "WHERE estadoUsuario = $dato";
		}

		$sql = "SELECT dniUsuario,claveUsuario,estadoUsuario,tipoUsuario FROM usuario $condicion";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/usuario.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->dniUsuario."|".$res[$i]->claveUsuario."|".$res[$i]->estadoUsuario;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}

	function generarArchivoMaestro(){

		$sql = "SELECT DISTINCT cod_barra FROM maestro";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}

	function generarArchivoMaestroDetalle(){

		$sql = "SELECT cod_barra, des_barra FROM maestro GROUP BY cod_barra";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/maestro_detalle.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->cod_barra."|".$res[$i]->des_barra;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;

	}


	function comprobarInformacionArchivoGen($file){
		$archivo = "../archivos_sistema/archivos_generados/".$file.".txt";
		$bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );
    	$conteo = count(file($archivo));

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = ($conteo - 1);
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}


	function generarprimerarchivo(){
		$sql="	SELECT CAST(M.nro_cont_mst AS UNSIGNED) nro_cont_mst,CAST(S.nro_cont_stk AS UNSIGNED) nro_cont_stk,M.loc_mst,S.loc_stk,
		RIGHT(CONCAT('00000000',C.sku_cap),8) sku_cap,
		RIGHT(CONCAT('0000000000000',C.barra_cap),13) barra_cap,
		RIGHT(CONCAT('00',C.tip_cap),2) tip_cap,
		C.cant_cap,
		CONCAT(SUBSTRING(M.fec_cong_mst, 7,4),SUBSTRING(M.fec_cong_mst, 4,2),SUBSTRING(M.fec_cong_mst, 1,2)) fec_cong_mst,
		CONCAT(SUBSTRING(S.fec_cong_stk, 7,4),SUBSTRING(S.fec_cong_stk, 4,2),SUBSTRING(S.fec_cong_stk, 1,2)) fec_cong_stk
				FROM maestro M INNER JOIN capturas C
				ON M.sku_barra = C.sku_cap LEFT JOIN stock S
				ON M.sku_barra = S.sku_stk
				group by c.sku_cap ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/IF_CARGA.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;

		for($i=0;$i<count($res);$i++){
			$cant_cap = $res[$i]->cant_cap;
			if($cant_cap < 0){
				$n_cant_cap = "-".SUBSTR("00000000000000000".($cant_cap * (-1)),-17);
			}else{
				$n_cant_cap = SUBSTR("000000000000000000".$cant_cap,-18);
			}
			$conteo++;

			//SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
			$cadena.="\r\n";
			$cadena.= $res[$i]->nro_cont_stk."|".$res[$i]->loc_stk."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->tip_cap."|".$n_cant_cap."|".$res[$i]->fec_cong_stk;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarclienteestructura(){
		$sql="	SELECT sku_cap,0 n1,barra_cap,0 descripcion,cant_cap,'UN' un,'5019' str,'1' flag,area_cap,id_captura,'1' mes,'2019' anio,'IGROUP' igroup,1 n2,'' v1,1 n3,1 n4,'20190108' fecha,'234036' hora FROM captura ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/C_ESTRUCTURA.txt";
		unlink('$archivo');

		//$cadena="\r\n";
		$conteo=0;
		$cadena.="\r\n";
		$cadena.="INTERNAL CODE|0|BARCODE|DESCRIPTION|QTY|UN|STR#|FLAG|AREA|RECORD|MONTH|YEAR|IGROUP|0001| |1|1|DATE|TIME";
		for($i=0;$i<count($res);$i++){

			$conteo++;

			//SI EL VALOR DE STOCK TIENE DATO ENTONCES COLOCAR EL VALOR SINO DEJAR EL VALOR DE MAESTRO
			$cadena.="\r\n";
			$cadena.= $res[$i]->sku_cap."|".$res[$i]->n1."|".$res[$i]->barra_cap."|".$res[$i]->descripcion."|".$res[$i]->cant_cap."|".$res[$i]->un."|".$res[$i]->str."|".$res[$i]->flag."|".$res[$i]->area_cap."|".$res[$i]->id_captura."|".$res[$i]->mes."|".$res[$i]->anio."|".$res[$i]->igroup."|".$res[$i]->n2."|".$res[$i]->v1."|".$res[$i]->n3."|".$res[$i]->n4."|".$res[$i]->fecha."|".$res[$i]->hora;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}

	function generarsegundoarchivo(){
		$sql="	SELECT c.area_cap, c.barra_cap, c.sku_cap, c.precio_barra, c.jerar, 
				IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as BODEGA, 
				'0.000' as PISO_VENTA 
				FROM capturas c, area_rango r where c.area_cap between r.area_ini_ran and r.area_fin_ran 
				group by c.sku_cap, c.area_cap
				UNION DISTINCT
				SELECT c.area_cap, c.barra_cap, c.sku_cap, c.precio_barra, c.jerar,  
				IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as PISO_VENTA,
				'0.000' as BODEGA
				FROM capturas c,area_rango r where c.area_cap between r.area_ini_ran and r.area_fin_ran 
				group by c.sku_cap, c.area_cap order by sku_cap, area_cap ";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/BOD_SV.txt";
		unlink('$archivo');

		$cadena="\r\n";
		$conteo=1;

		$cadena.="LOTE|SKU|DESCRIPCION|EAN|COSTO|STOCK|JERARQUIA|BODEGA|SALA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $res[$i]->area_cap."|".$res[$i]->sku_cap."|".$res[$i]->barra_cap."|".$res[$i]->precio_barra."|".$res[$i]->jerar."|".$res[$i]->BODEGA."|".$res[$i]->PISO_VENTA;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}
/*
	function generarReporteProductividad($estado){
		$sql="	SELECT U.dniUsuario, U.nombreUsuario,
				(SELECT inicioAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia ASC LIMIT 1) inicioAsistencia, 
				(SELECT terminoAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia DESC LIMIT 1) terminoAsistencia,
				SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60) horas_conteo,
				(SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) total_conteo,
				((SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) / SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60)) conteo_x_hora 
				FROM usuario U LEFT JOIN asistencia A
				ON U.dniUsuario = A.dniUsuario
				WHERE U.estadoUsuario = $estado
				GROUP BY U.dniUsuario";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/archivo_reporte_productividad.txt";
		unlink('$archivo');

		$conteo=1;
		$cadena.="\r\n";
		$cadena.="N|DNI|NOMBRE COMPLETO|INICIO CONTEO|FIN CONTEO|HORAS CONTEO|TOTAL CONTADO|CONTEO X HORA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $conteo."|".$res[$i]->dniUsuario."|".$res[$i]->nombreUsuario."|".$res[$i]->inicioAsistencia."|".$res[$i]->terminoAsistencia."|".$res[$i]->horas_conteo."|".$res[$i]->total_conteo."|".$res[$i]->conteo_x_hora;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}
*/

}	
?>