<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceMaestro extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function saveRegistrosMaestro($data){
        $c = $g = $f = 0;
        $detalle = "RESUMEN DE CARGA ARCHIVO MAESTRO \r\n";
        $detalle .= "LINEAS NO CARGADAS \r\n";

        $maestro = file("../archivos_sistema/archivos_maestro/".$data->archivo);
        $maestro_log = fopen("../archivos_sistema/archivos_maestro/log_".$data->archivo, "w");

        $sql_truncate = "TRUNCATE TABLE maestro";
        $this->db->query($sql_truncate);

        //$registros = array_count_values($maestro);
        
        foreach ($maestro as $fila => $valor){
            $c++;

            $valor = str_replace("'","",$valor);
            //$valor = str_replace("|","','",$valor);
            //echo $valor;
            //$registro = "('".$valor."');";
            $cadena = explode("|",$valor);

            $registro = "('".implode("','", $cadena)."')";

            $sql = "INSERT INTO maestro (cod_barra,sku_barra,des_barra,meins,factor,Prec_barra,envase,jerar,des_jerar,sub_dep_mst,des_sub_dep_mst,clas_mst,des_clas_mst,sub_clas_mst,des_sub_clas_mst) VALUES $registro";
            $res=$this->db->query($sql);

            if($res){
                $g++;
            }else{
                $f++;
                $detalle .= "LINEA : ".$c." - DETALLE : ".$registro." \r\n";
            }

        }
        $detalle .= " \r\n";
        $detalle .= "FILAS RECORRIDAS : ".$c." \r\n";
        $detalle .= "REGISTROS GUARDADOS : ".$g." \r\n";
        $detalle .= "REGISTROS FALLIDOS : ".$f;

        fwrite($maestro_log, $detalle);
        fclose($maestro_log);

        return $c;
    }

    function listarArchivosMaestroPendientes(){

        $archivos = array();

        $directorio = opendir("../archivos_sistema/archivos_maestro"); //ruta actual
        while ($archivo = readdir($directorio)) //obtenemos un archivo y luego otro sucesivamente
        {
            if (is_dir($archivo))//verificamos si es o no un directorio
            {
                //echo "[".$archivo . "]<br />"; //de ser un directorio lo envolvemos entre corchetes
            }
            else
            {

                $esArchivo = strpos($archivo, "log");

                if ($esArchivo === false) {

                    $bytes = filesize("../archivos_sistema/archivos_maestro/".$archivo);
                    $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
                    for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
                    $peso = ( round( $bytes, 2 ) . " " . $label[$i] );


                    $file = new stdClass();
                    $file->nombre = $archivo;
                    $file->log = "log_".$archivo;
                    $file->peso = $peso;
                    $file->fecha = date("Y-m-d", filectime("../archivos_sistema/archivos_maestro/".$archivo));

                    $archivos[] = $file;

                }
            }

            
        }

        return $archivos;

    }

    function eliminarArchivoMaestroPendiente($dato){
        unlink("../archivos_sistema/archivos_maestro/".$dato);
        unlink("../archivos_sistema/archivos_maestro/log_".$dato);
        return 1;

    }




}	
?>