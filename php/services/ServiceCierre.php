<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceCierre extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function generarCierre(){

		$sql_consulta = "SELECT COUNT(*) AS cuenta FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_var($sql_consulta);

		if($res_consulta == 0){
			//LIMPIAR BASES TEMPORALES
			$sql="TRUNCATE capturas";
			$res=$this->db->query($sql);
			$sql="TRUNCATE diferencias";
			$res=$this->db->query($sql);
			$sql="TRUNCATE stock_total";
			$res=$this->db->query($sql);

			$archivo = "../archivos_sistema/archivo_cierre.txt";
			unlink($archivo);

			//GENERAR TABLA CAPTURAS
			$sql="	INSERT INTO capturas
					SELECT 
					A.id_captura,A.area_cap,A.barra_cap,
					B.sku_barra AS sku_cap,
					SUM(A.cant_cap) AS cant_cap,
					A.tip_cap,A.usuario,A.fecha,A.hora,
					B.jerar,B.des_jerar,B.sub_dep_mst,B.des_sub_dep_mst,B.clas_mst,B.des_clas_mst,B.sub_clas_mst,B.des_sub_clas_mst,
					B.Prec_barra,B.des_barra
					FROM captura A LEFT JOIN maestro B
					ON A.barra_cap = B.cod_barra
					GROUP BY B.sku_barra";
			$res=$this->db->query($sql);


			$sql="INSERT into stock_total
SELECT 0,s.loc_stk,s.ceros,s.sku_stk,s.cant_cer_stk,s.espacio,s.um,
m.cod_barra,m.des_barra,m.meins,m.factor,m.Prec_barra,m.envase,m.jerar,
m.des_jerar,m.sub_dep_mst,m.des_sub_dep_mst,m.clas_mst,m.des_clas_mst,m.sub_clas_mst,m.des_sub_clas_mst
FROM stock s LEFT JOIN maestro m ON s.sku_stk=m.sku_barra GROUP BY s.sku_stk";
$res=$this->db->query($sql);




			$sql="	INSERT INTO diferencias
				
					SELECT s.sku_stk, s.des_barra, ROUND(s.prec_barra,3) AS costo, 
					IFNULL( SUM(ROUND( s.cant_cer_stk,3 )) , 0 ) AS stock,
					IFNULL(ROUND(c.cant_cap,3), 0)  AS contado, 
					ROUND((IFNULL(c.cant_cap,0) - IFNULL(SUM(s.cant_cer_stk),0)),3) AS dif_cant, 
					ROUND(IFNULL(SUM(s.cant_cer_stk) * s.prec_barra,0),3) AS total_stock_sol, 
					ROUND(IFNULL((c.cant_cap * s.prec_barra),0),3) AS total_cap_sol, 
					ROUND(IFNULL((c.cant_cap * s.prec_barra),0),3) - ROUND(IFNULL(SUM(s.cant_cer_stk) * s.prec_barra,0),3) AS dif_sol,
					IF(ROUND(IFNULL((c.cant_cap * s.prec_barra),0),3) - ROUND(IFNULL(SUM(s.cant_cer_stk) * s.prec_barra,0),3) < 0, 
					ROUND(IFNULL((c.cant_cap * s.prec_barra),0),3) - ROUND(IFNULL(SUM(s.cant_cer_stk) * s.prec_barra,0),3) * (-1),
					ROUND(IFNULL((c.cant_cap * s.prec_barra),0),3) - ROUND(IFNULL(SUM(s.cant_cer_stk) * s.prec_barra,0),3)) AS dif_sol_pos,
					s.jerar, s.des_jerar,s.sub_dep_mst,s.des_sub_dep_mst,s.clas_mst,s.des_clas_mst,s.cod_barra
					FROM capturas c RIGHT JOIN stock_total s ON s.sku_stk=c.sku_cap
					GROUP BY s.sku_stk

					UNION DISTINCT

					SELECT c.sku_cap, c.des_barra, ROUND(c.precio_barra,3) AS costo, 
					ROUND(IFNULL(SUM(s.cant_cer_stk),0),3) AS stock, 
					ROUND(IFNULL(c.cant_cap,0),3)  AS contado,
					ROUND(IFNULL(c.cant_cap,0) - IFNULL(SUM(s.cant_cer_stk),0),3) AS dif_cant, 
					ROUND(IFNULL(s.cant_cer_stk * c.precio_barra,0),3) AS total_stock_sol, 
					ROUND(IFNULL((c.cant_cap * c.precio_barra),0),3) AS total_cap_sol,
					ROUND(IFNULL(IFNULL((c.cant_cap * c.precio_barra),0) - (IFNULL(SUM(s.cant_cer_stk),0) * c.precio_barra),0),3) AS dif_sol,
					IF(ROUND(IFNULL(IFNULL((c.cant_cap * c.precio_barra),0) - (IFNULL(SUM(s.cant_cer_stk),0) * c.precio_barra),0),3) < 0,
					ROUND(IFNULL(IFNULL((c.cant_cap * c.precio_barra),0) - (IFNULL(SUM(s.cant_cer_stk),0) * c.precio_barra),0),3) * (-1),
					ROUND(IFNULL(IFNULL((c.cant_cap * c.precio_barra),0) - (IFNULL(SUM(s.cant_cer_stk),0) * c.precio_barra),0),3)) AS dif_sol_pos, 
					c.jerar, c.des_jerar,c.sub_dep_mst,c.des_sub_dep_mst,c.clas_mst,c.des_clas_mst,c.barra_cap
					FROM capturas c LEFT JOIN stock_total s ON c.sku_cap=s.sku_stk where s.sku_stk is null
					GROUP BY c.sku_cap
					";
			$res=$this->db->query($sql);


			$cadena ="ARCHIVO QUE INDICA LA FECHA DE CREACION";
			$cadenax = substr($cadena,1,strlen($cadena));
			$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
			fwrite($fch, $cadenax); // Grabas
			fclose($fch); // Cierras el archivo.

			return 1;			
		}else{
			return 0;
		}

	}

	function sincronizarInventario(){

	$cliente = $this->getDato("cliente","tienda","idTienda > 0 limit 1");
	$tienda = $this->getDato("numeroTienda","tienda","idTienda > 0 limit 1");

	$fecha = date("Y-m-d H:i:s");
	$fecha_archivo = date("Y_m_d H_i_s");
	$array_avance = array();
    $array_avance_total = array();
    $array_diferencia_total = array();
    $array_sub_diferencia_total = array();

    $valor_avance = '';
    $valor_avance_total = '';
    $valor_diferencia_total = '';
    $valor_sub_diferencia_total = '';

    $valor_ranking_productos = '';


    $service = new ServiceReportes();

    $resultado = $service->getListaReporteAreaRango();
    $dataRangos = $resultado->rangos;
    $dataCapturas = $resultado->capturas;
    $dataJustificados = $resultado->justificados;

    $cantidad_total = 0;
    $avance_total = 0;

    $cuentaRangos = count($dataRangos);

        for($i=0 ; $i < $cuentaRangos ; $i++){
        	$filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
            $filaDescripcion = $dataRangos[$i]->des_area_ran;
            $cantidad = ( (int)$filaFinal - (int)$filaInicio ) + 1;
            $avance = 0;
            $porcentaje = 0;
            for ($z=$filaInicio; $z<=$filaFinal; $z++) { 
                for ($y=0; $y<count($dataCapturas); $y++) {
                    $valorArea = (int)$dataCapturas[$y]->area_cap;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }
                for ($x=0; $x<count($dataJustificados); $x++) {
                    $valorArea = (int)$dataJustificados[$x]->lote;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }
            }

            $cantidad_total = $cantidad_total + $cantidad;
            $avance_total = $avance_total + $avance;

            $porcentaje = round(($avance / $cantidad)*100,2);


            if($valor_avance_total != ""){
            	$valor_avance_total .= ",";
            }

            //$array_avance_total[] = array('area_rango' => $filaDescripcion, 'avance' => $porcentaje, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);

            $valor_avance_total .= "('$filaDescripcion',$porcentaje,'$cliente', '$tienda','$fecha')"; 

        }

        $valor_avance_total .= ";";

        $porcentaje_total = round(($avance_total / $cantidad_total)*100,2);

        //AVANCE
        $avance = $service->getAvanceTotal();
        for($i=0 ; $i < count($avance) ; $i++){
        	//$array_avance[] = array('porcentaje' => $porcentaje_total, 'stock' => $avance[$i]->stock, 'captura' => $avance[$i]->captura, 'stock_sol' => $avance[$i]->stock_sol, 'captura_sol' => $avance[$i]->captura_sol, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);
        	$valor_avance .= "($porcentaje_total,".$avance[$i]->stock.", ".$avance[$i]->captura.",".$avance[$i]->stock_sol.", ".$avance[$i]->captura_sol.",'$cliente', '$tienda','$fecha')";
        }
        //FIN AVANCE

        //DIFERENCIA
        $diferenciaTotal = $service->getDiferenciaTotal();
        for($i=0 ; $i < count($diferenciaTotal) ; $i++){
        	if($valor_diferencia_total != ""){
            	$valor_diferencia_total .= ",";
            }
        	//$array_diferencia_total[] = array('departamento' => $diferenciaTotal[$i]->departamento, 'diferencia_soles' => $diferenciaTotal[$i]->diferencia_soles, 'diferencia_cantidad' => $diferenciaTotal[$i]->diferencia_cantidad, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);
        	$valor_diferencia_total .= "('".$diferenciaTotal[$i]->departamento."','".$diferenciaTotal[$i]->departamento_original."',".$diferenciaTotal[$i]->diferencia_soles.", ".$diferenciaTotal[$i]->diferencia_cantidad.",'$cliente', '$tienda','$fecha')";

        	$rankingProductos = $service->getRankingProductos($diferenciaTotal[$i]->departamento_original);
	        for($x=0 ; $x < count($rankingProductos) ; $x++){
	        	if($valor_ranking_productos != ""){
	            	$valor_ranking_productos .= ",";
	            }
	        	$valor_ranking_productos .= "('".$rankingProductos[$x]->sku_stk."','".$rankingProductos[$x]->des_sku_stk."','".$rankingProductos[$x]->costo."','".$rankingProductos[$x]->stock."','".$rankingProductos[$x]->contado."',".$rankingProductos[$x]->dif_cant.", ".$rankingProductos[$x]->dif_sol.", ".$rankingProductos[$x]->dif_sol_pos.", '".$rankingProductos[$x]->des_dep_stk."', '".$rankingProductos[$x]->des_sub_dep_stk."', '".$rankingProductos[$x]->des_clas_stk."','$cliente', '$tienda','$fecha')";
	        }

        }
        $valor_diferencia_total .= ";";

       	$valor_ranking_productos .= ";";


        $subDiferenciaTotal = $service->getSubDiferenciaTotal();
        for($i=0 ; $i < count($subDiferenciaTotal) ; $i++){
        	if($valor_sub_diferencia_total != ""){
            	$valor_sub_diferencia_total .= ",";
            }	
        	//$array_sub_diferencia_total[] = array('departamento' => $subDiferenciaTotal[$i]->departamento,'sub_departamento' => $subDiferenciaTotal[$i]->sub_departamento, 'diferencia_soles' => $subDiferenciaTotal[$i]->diferencia_soles, 'diferencia_cantidad' => $subDiferenciaTotal[$i]->diferencia_cantidad, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);
        	$valor_sub_diferencia_total .= "('".$subDiferenciaTotal[$i]->departamento."','".$subDiferenciaTotal[$i]->sub_departamento."', ".$subDiferenciaTotal[$i]->diferencia_soles.", ".$subDiferenciaTotal[$i]->diferencia_cantidad.", '$cliente', '$tienda','$fecha')";
        }
        $valor_sub_diferencia_total .= ";";

        //FIN DIFERENCIA

        //GENERAR ARCHIVOS PARA CARGA
        $service = new ServiceGenerarArchivos();
    	$primerArchivo = $service->generarprimerarchivo();
    	$segundoArchivo = $service->generarsegundoarchivo();
        //CERRAR GENERAR ARCHIVOS

    	$this->generarReporteGrafico();
    	//COMPRIMIR ARCHIVOS
		require('../php/includes/pclzip/pclzip.lib.php');
		$files_to_zip = array('IF_CARGA.txt','BOD_SV.txt','reporte_grafico_uni_sol.xlsx');
		copy("../webService/reportes/reporte_grafico_uni_sol.xlsx","../archivos_sistema/archivos_generados/reporte_grafico_uni_sol.xlsx");
		$dir = '../archivos_sistema/archivos_generados/'; 
		$new_files = array();
		  foreach($files_to_zip as $value){
		   $new_files[] = $dir.$value;
		}

		$archive = new PclZip("archivos_generados_".$cliente."".$tienda."".$fecha_archivo.".zip");
		$files_archive = $archive->add($new_files, PCLZIP_OPT_REMOVE_PATH, $dir, PCLZIP_OPT_ADD_PATH, 'archivos_generados');
		if ($files_archive == 0) {
			$archivo_zip = false;
		} else {
			$archivo_zip = true;
		}
    	//FINAL COMPRESION

        //CARGAR LOS RESULTADOS AL SISTEMA WEB
        
        $dbWeb = new mysqli("mysql.igroupsac.com", "user_igroupsac", "User210382!?", "database_igroupsac");

        $sqlAvance ="INSERT INTO avance (porcentaje, stock, captura, stock_sol, captura_sol, cliente, tienda, fecha) VALUES $valor_avance";
		$resAvance = $dbWeb->query($sqlAvance);

		$sqlAvanceTotal ="INSERT INTO avance_total (area_rango, avance, cliente, tienda, fecha) VALUES $valor_avance_total";
		$resAvanceTotal = $dbWeb->query($sqlAvanceTotal);

		$sqlDiferenciaTotal ="INSERT INTO diferencia_total (departamento, departamento_original, diferencia_soles, diferencia_cantidad, cliente, tienda, fecha) VALUES $valor_diferencia_total";
		$resDiferenciaTotal = $dbWeb->query($sqlDiferenciaTotal);

		$sqlSubDiferenciaTotal ="INSERT INTO sub_diferencia_total (departamento, sub_departamento, diferencia_soles, diferencia_cantidad, cliente, tienda, fecha) VALUES $valor_sub_diferencia_total";
		$resSubDiferenciaTotal = $dbWeb->query($sqlSubDiferenciaTotal);

		$sqlRankingProductos ="INSERT INTO ranking_productos (sku_stk, des_sku_stk, costo, stock, contado, dif_cant, dif_sol, dif_sol_pos, departamento, sub_departamento, producto, cliente, tienda, fecha) VALUES $valor_ranking_productos";
		$resRankingProductos = $dbWeb->query($sqlRankingProductos);

		//SUBIR ARCHIVO AL SERVIDOR
		$servidor_ftp = "cloverleaf.dreamhost.com";
		$conexion_id = ftp_connect($servidor_ftp);
		$ftp_usuario = "igroupsac";
		$ftp_clave = "Igroup210382!?";
		$ftp_carpeta_local =  "";
		$ftp_carpeta_remota = "/igroupsac.com/sistema/archivos_sistema/";
		$mi_nombredearchivo = "archivos_generados_".$cliente."".$tienda."".$fecha_archivo.".zip";
		$nombre_archivo = $ftp_carpeta_local.$mi_nombredearchivo;
		$archivo_destino = $ftp_carpeta_remota.$mi_nombredearchivo;
		$resultado_login = ftp_login($conexion_id, $ftp_usuario, $ftp_clave);
		if ((!$conexion_id) || (!$resultado_login)) {
		       $archivo_zip_cargado = false;
		   } else {
		       $archivo_zip_cargado = true;
		   }
		$upload = ftp_put($conexion_id, $archivo_destino, $nombre_archivo, FTP_BINARY);
		if (!$upload) {
		       $archivo_zip_cargado = false;
		   } else {
		       $archivo_zip_cargado = true;
		   }
		ftp_close($conexion_id);
		//FINAL SUBIR ARCHIVO


		//$resultado = new stdClass();
		//$resultado->avance = $valor_avance;
        //$resultado->avance_total = $valor_avance_total; //$array_avance_total;
        //$resultado->diferencia_total = $valor_diferencia_total;
        //$resultado->sub_diferencia_total = $valor_sub_diferencia_total;
        if($resAvance && $resAvanceTotal && $resDiferenciaTotal && $resSubDiferenciaTotal && $archivo_zip && $archivo_zip_cargado && $resRankingProductos){
        	return 1;
        }else{
	        $sqlAvance ="DELETE FROM avance WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlAvance);

			$sqlAvanceTotal ="DELETE FROM avance_total WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlAvanceTotal);

			$sqlDiferenciaTotal ="DELETE FROM diferencia_total WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlDiferenciaTotal);

			$sqlSubDiferenciaTotal ="DELETE FROM sub_diferencia_total WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlSubDiferenciaTotal);

        	return 2;
        }

	}



	function comprobarInformacionCierre(){
		$archivo = "../archivos_sistema/archivo_cierre.txt";
		return "FECHA : ".date("Y-m-d", filemtime($archivo))." - HORA : ".date("H:i:s", filemtime($archivo));
	}


	function consultarRegistrosStock(){
		$sql = "SELECT COUNT(stock_id) cantidad FROM stock";
		$res = $this->db->get_var($sql);

		return $res;	
	}

	function generarReporteGrafico(){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

    	$sqlTrucate =  "TRUNCATE TABLE grafico_tot";
    	$resTrucate=$this->db->query($sqlTrucate);

    	$sqlTrucate =  "TRUNCATE TABLE reporte_area_rango";
    	$resTrucate=$this->db->query($sqlTrucate);

    	$sqlInsert = "	INSERT INTO grafico_tot

						SELECT 0 idGrafico, m.jerar, c.cant_cap, m.Prec_barra prec_barra, g.njerarquias jerarquia, GROUP_CONCAT(DISTINCT ar.ubicacion SEPARATOR ',') ubicacion 
						FROM captura c LEFT JOIN area_rango ar
						ON c.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran LEFT JOIN maestro m
						ON c.barra_cap = m.cod_barra LEFT JOIN grupos g
						ON SUBSTR(m.jerar,1,3) = g.idGrupo
						GROUP BY c.id_captura ";
		$resInsert=$this->db->query($sqlInsert);

    	$sqlInsert = "	INSERT INTO reporte_area_rango
						SELECT
						GROUP_CONCAT(DISTINCT ar.ubicacion SEPARATOR '-') ubicacion, c.sku_cap FROM captura c LEFT JOIN area_rango ar
						ON c.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran
						GROUP BY c.sku_cap ";
		$resInsert=$this->db->query($sqlInsert);


		$sql = "SELECT g.ubicacion, g.jerarquia, u.ubicacion, SUM(g.cant_cap) unidades, SUM(g.cant_cap * g.prec_barra) soles FROM grafico_tot g LEFT JOIN ubicacion u
				ON g.ubicacion = u.idUbicacion
				WHERE g.jerarquia IN ('NON FOOD','PERECEBLES','PGC','INSTITUCIONAL')
				GROUP BY g.jerarquia, u.ubicacion
				ORDER BY u.ubicacion, g.jerarquia ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("jerarquia","ubicacion"));

		$sqlTotal="	SELECT g.jerarquia, SUM(g.cant_cap) unidades, SUM(g.cant_cap * g.prec_barra) soles FROM grafico_tot g LEFT JOIN ubicacion u
					ON g.ubicacion = u.idUbicacion
					WHERE g.jerarquia IN ('NON FOOD','PERECEBLES','PGC','INSTITUCIONAL')
					GROUP BY g.jerarquia
					ORDER BY u.ubicacion, g.jerarquia ASC";
		$resTotal = $this->db->get_results($sqlTotal);
		$this->_codificarObjeto($resTotal,array("jerarquia"));

		$sqlPrincipal="	SELECT us.descripcion, COUNT(rar.sku) sku FROM reporte_area_rango rar LEFT JOIN ubicacion_separado us
					ON rar.ubicacion = us.ubicacion
					GROUP BY us.descripcion";
		$resPrincipal = $this->db->get_results($sqlPrincipal);
		$this->_codificarObjeto($resPrincipal,array("descripcion"));


			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objReader->setIncludeCharts(TRUE);
			$objPHPExcel = $objReader->load("formatos/reporte_grafico_uni_sol.xlsx");


			//DAR VUELTA A TODOS LOS DATOS PARA QUE SE CARGUE LA GRILLA
			$suma_bodega = 0;
			$suma_sala = 0;
			$suma_bodega_sala = 0;
			$suma_sku_total = 0;
			
			$suma_uni_nonfood = 0;
			$suma_uni_perecederos = 0;
			$suma_uni_pgc = 0;
			$suma_uni_ins = 0;
			$suma_total_uni = 0;

			$suma_sol_nonfood = 0;
			$suma_sol_perecederos = 0;
			$suma_sol_pgc = 0;
			$suma_sol_ins = 0;
			$suma_total_sol = 0;

			$uni_b_nonfood = $uni_s_nonfood = $sol_b_nonfood = $sol_s_nonfood = 0;
			$uni_b_perecederos = $uni_s_perecederos = $sol_b_perecederos = $sol_s_perecederos = 0;
			$uni_b_pgc = $uni_s_pgc = $sol_b_pgc = $sol_s_pgc = 0;
			$uni_b_ins = $uni_s_ins = $sol_b_ins = $sol_s_ins = 0;
			$uni_total_b = $uni_total_s = $sol_total_b = $sol_total_s = 0;

			//$i = 6;
			for ($y=0; $y < count($resPrincipal); $y++){
				if ($resPrincipal[$y]->descripcion == "BODEGA"){
					$suma_bodega = $suma_bodega + $resPrincipal[$y]->sku;
				}
				if ($resPrincipal[$y]->descripcion == "SALA"){
					$suma_sala = $suma_sala + $resPrincipal[$y]->sku;
				}
				if ($resPrincipal[$y]->descripcion == "BODEGA Y SALA"){
					$suma_bodega_sala = $suma_bodega_sala + $resPrincipal[$y]->sku;
				}
					$suma_sku_total = $suma_sku_total + $resPrincipal[$y]->sku;
			}

			for ($i=0; $i < count($resTotal); $i++){
				if ($resTotal[$i]->jerarquia == "NON FOOD"){
					$suma_uni_nonfood = $suma_uni_nonfood + $resTotal[$i]->unidades;
					$suma_sol_nonfood = $suma_sol_nonfood + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "PERECEBLES"){
					$suma_uni_perecederos = $suma_uni_perecederos + $resTotal[$i]->unidades;
					$suma_sol_perecederos = $suma_sol_perecederos + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "PGC"){
					$suma_uni_pgc = $suma_uni_pgc + $resTotal[$i]->unidades;
					$suma_sol_pgc = $suma_sol_pgc + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "INSTITUCIONAL"){
					$suma_uni_ins = $suma_uni_ins + $resTotal[$i]->unidades;
					$suma_sol_ins = $suma_sol_ins + $resTotal[$i]->soles;
				}
					$suma_total_uni = $suma_total_uni + $resTotal[$i]->unidades;
					$suma_total_sol = $suma_total_sol + $resTotal[$i]->soles;
			}


			for( $x = 0; $x < count($res); $x++) { 
				if($res[$x]->ubicacion == "BODEGA"){
					if($res[$x]->jerarquia == "NON FOOD"){
						$uni_b_nonfood = $res[$x]->unidades / $suma_uni_nonfood;
						$sol_b_nonfood = $res[$x]->soles / $suma_sol_nonfood;
					}
					if($res[$x]->jerarquia == "PERECEBLES"){
						$uni_b_perecederos = $res[$x]->unidades / $suma_uni_perecederos;
						$sol_b_perecederos = $res[$x]->soles / $suma_sol_perecederos;
					}
					if($res[$x]->jerarquia == "PGC"){
						$uni_b_pgc = $res[$x]->unidades / $suma_uni_pgc;
						$sol_b_pgc = $res[$x]->soles / $suma_sol_pgc;
					}
					if($res[$x]->jerarquia == "INSTITUCIONAL"){
						$uni_b_ins = $res[$x]->unidades / $suma_uni_ins;
						$sol_b_ins = $res[$x]->soles / $suma_sol_ins;
					}
					$uni_total_b = $uni_total_b + $res[$x]->unidades;
					$sol_total_b = $sol_total_b + $res[$x]->soles;
				}
				if($res[$x]->ubicacion == "SALA"){
					if($res[$x]->jerarquia == "NON FOOD"){
						$uni_s_nonfood = $res[$x]->unidades / $suma_uni_nonfood;
						$sol_s_nonfood = $res[$x]->soles / $suma_sol_nonfood;
					}
					if($res[$x]->jerarquia == "PERECEBLES"){
						$uni_s_perecederos = $res[$x]->unidades / $suma_uni_perecederos;
						$sol_s_perecederos = $res[$x]->soles / $suma_sol_perecederos;
					}
					if($res[$x]->jerarquia == "PGC"){
						$uni_s_pgc = $res[$x]->unidades / $suma_uni_pgc;
						$sol_s_pgc = $res[$x]->soles / $suma_sol_pgc;
					}
					if($res[$x]->jerarquia == "INSTITUCIONAL"){
						$uni_s_ins = $res[$x]->unidades / $suma_uni_ins;
						$sol_s_ins = $res[$x]->soles / $suma_sol_ins;
					}
					$uni_total_s = $uni_total_s + $res[$x]->unidades;
					$sol_total_s = $sol_total_s + $res[$x]->soles;
				}
			}


	
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E4',$suma_bodega)
				->setCellValue('E5',$suma_sala)
				->setCellValue('E6',$suma_bodega_sala)
				->setCellValue('E7',$suma_sku_total)
				->setCellValue('H4',($suma_bodega / $suma_sku_total))
				->setCellValue('H5',($suma_sala / $suma_sku_total))
				->setCellValue('H6',($suma_bodega_sala / $suma_sku_total))
				->setCellValue('H7',($suma_sku_total / $suma_sku_total))
				->setCellValue('C12',$uni_b_nonfood)
				->setCellValue('C13',$uni_b_perecederos)
				->setCellValue('C14',$uni_b_pgc)
				->setCellValue('C15',$uni_b_ins)
				->setCellValue('D12',$uni_s_nonfood)
				->setCellValue('D13',$uni_s_perecederos)
				->setCellValue('D14',$uni_s_pgc)
				->setCellValue('D15',$uni_s_ins)
				->setCellValue('H12',$sol_b_nonfood)
				->setCellValue('H13',$sol_b_perecederos)
				->setCellValue('H14',$sol_b_pgc)
				->setCellValue('H15',$sol_b_ins)
				->setCellValue('I12',$sol_s_nonfood)
				->setCellValue('I13',$sol_s_perecederos)
				->setCellValue('I14',$sol_s_pgc)
				->setCellValue('I15',$sol_s_ins)
				->setCellValue('C16',($uni_total_b / $suma_total_uni))
				->setCellValue('D16',($uni_total_s / $suma_total_uni))
				->setCellValue('H16',($sol_total_b / $suma_total_sol))
				->setCellValue('I16',($sol_total_s / $suma_total_sol));
			

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->setIncludeCharts(TRUE);
			$objWriter->save('reportes/reporte_grafico_uni_sol.xlsx');			

	}


}	
?>