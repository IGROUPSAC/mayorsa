<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceCerrarInventario extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function truncateBaseDatos($tablas){
		$truncate_tablas = explode(",", $tablas);

		for($i=0;$i<count($truncate_tablas);$i++){
			$sql="TRUNCATE TABLE ".$truncate_tablas[$i];
        	$res=$this->db->query($sql);
		}

		$carpetas = ['archivos_bodega','archivos_cierre','archivos_eliminados','archivos_generados','archivos_gondola','archivos_gondola_generados','archivos_maestro','archivos_pendientes','archivos_procesados','archivos_stock'];	

	    for($i = 0; $i < count($carpetas); $i++){

	    	$files = glob("../archivos_sistema/".$carpetas[$i]."/*.*");
			foreach($files as $file){
			    if(is_file($file))
			    unlink($file); //elimino el fichero
			}

	    }

	}

	function unicode_decode($str) {
    	return preg_replace_callback('/\\\\u([0-9a-f]{4})/i', 'replace_unicode_escape_sequence', $str);
    }

	function backupBaseDatos(){
	    //connect & select the database
	    $db = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME); 
	    $return = "";
	    //get all of the tables
	    $tables = DB_BACKUP_TABLES;
	    if($tables == '*'){
	        $tables = array();
	        $result = $db->query("SHOW TABLES");
	        while($row = $result->fetch_row()){
	            $tables[] = $row[0];
	        }
	    }else{
	        $tables = is_array($tables)?$tables:explode(',',$tables);
	    }

	    //loop through the tables
	    foreach($tables as $table){
	        $result = $db->query("SELECT * FROM $table");
	        $numColumns = $result->field_count;

	        $return .= "DROP TABLE $table;";

	        $result2 = $db->query("SHOW CREATE TABLE $table");
	        $row2 = $result2->fetch_row();

	        $return .= "\n\n".$row2[1].";\n\n";

	        for($i = 0; $i < $numColumns; $i++){
	            while($row = $result->fetch_row()){
	                $return .= "INSERT INTO $table VALUES(";
	                for($j=0; $j < $numColumns; $j++){
	                    $row[$j] = addslashes($row[$j]); //$row[$j];
	                    //$row[$j] = utf8_encode( $row[$j] );
	                    $row[$j] = str_replace("'"," ",$row[$j]);
	                    $row[$j] = str_replace('"','´´',$row[$j]);
	                    $row[$j] = str_replace("\n"," ",$row[$j]);
	                    //$row[$j] = str_replace("/"," ",$row[$j]);
	                    //$row[$j] = str_replace("#"," ",$row[$j]);
	                    //$row[$j] = utf8_encode( $row[$j] );
	                    //$row[$j] = unicode_decode( $row[$j] );
	                    if (isset($row[$j])) { $return .= '"'.$row[$j].'"' ; } else { $return .= '""'; }
	                    if ($j < ($numColumns-1)) { $return.= ','; }
	                }
	                $return .= ");\n";
	            }
	        }

	        $return .= "\n\n\n";
	    }

	    $numero = $this->getDato("numeroTienda","tienda","idTienda > 0 LIMIT 1");
	    $nombre = $this->getDato("nombreTienda","tienda","idTienda > 0 LIMIT 1");
	    $fecha = date("Y-m-d");
	    $hora = date("H-i-s");

	    //save file
	    $directorio = DB_BACKUP_DEVICE_USB;
	    $directorio_local = DB_BACKUP_DEVICE_LOCAL;

	    $existe_usb = $this->comprobarDispositivo();
	    if($existe_usb == 1){
	    	$handle = fopen($directorio.':\backup-'.$numero.'-'.$nombre.'-'.$fecha.'--'.$hora.'.sql','w+');
	    }else{
	    	$handle = fopen($directorio_local.':\backup-'.$numero.'-'.$nombre.'-'.$fecha.'--'.$hora.'.sql','w+');
	    }
	    fwrite($handle,$return);
	    fclose($handle);
	}

	function comprobarDispositivo(){
		$directorio = DB_BACKUP_DEVICE_USB;
		$nombre_fichero = $directorio.":\backup.txt";
		$retorno = 0;

		if (file_exists($nombre_fichero)) {
		    $retorno = 1;
		} else {
		    $retorno = 0;
		}

		return $retorno;

	}

	function restoreMysqlDB($backup,$dbHost,$dbUsername,$dbPassword,$dbName){
		$filePath = $_FILES["backup"]["name"];

		$conn = mysqli_connect($dbHost, $dbUsername, $dbPassword, $dbName); 
	    $sql = '';
	    $error = '';
	    
	    if (file_exists($filePath)) {
	        $lines = file($filePath);
	        
	        foreach ($lines as $line) {
	            
	            // Ignoring comments from the SQL script
	            if (substr($line, 0, 2) == '--' || $line == '') {
	                continue;
	            }
	            
	            $sql .= $line;
	            
	            if (substr(trim($line), - 1, 1) == ';') {
	                $result = mysqli_query($conn, $sql);
	                if (! $result) {
	                    $error .= mysqli_error($conn) . "\n";
	                }
	                $sql = '';
	            }
	        } // end foreach
	        
	        if ($error) {
	    		$response = 0;    	
	            /*$response = array(
	                "type" => "error",
	                "message" => $error
	            );*/
	        } else {
	        	$response = 1;
	            /*$response = array(
	                "type" => "success",
	                "message" => "Database Restore Completed Successfully."
	            );*/
	        }
	        exec('rm ' . $filePath);
	    } // end if file exists
	    
	    return $response;
	}

}	
?>