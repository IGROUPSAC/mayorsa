$(function () {
    $('#datetimepicker').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerDesde').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerHasta').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerDesdeR').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerFechaOP').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerFechaTramiteConvalidacion').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerPresupuestoFechaReporteDesde').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerPresupuestoFechaReporteHasta').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false
    });
    $('#datetimepickerFechaSP').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    $('#datetimepickerFechaPIN').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    $('#datetimepickerFechaFF').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    
    $('#datetimepickerFechaACTFI').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    $('#datetimepickerFechaACTFE').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    $('#datetimepickerFechaACTFR').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    $('#datetimepickerFechaOCA').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });
    $('#datetimepickerFechaTienda').datetimepicker({
        format: 'YYYY-MM-DD',
        language: 'pe',
        pickTime: false,
        closeOnDateSelect: true
    });

});