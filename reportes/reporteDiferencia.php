<?php
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");

    $service = new ServiceReportes();

    $monto = $_GET["monto"];
    $jera = $_GET["jera"];
    $subjera = $_GET["subjera"];
    $data = $service->getReporteDiferencia($monto,$jera,$subjera);
    $registros = $data;

    $dataarearango = $service->getReporteDiferenciaAreaRango();
    $regarearango = $dataarearango;

    $porcentaje = $service->getPorcentajeAvance();

    $tamanoLetra = 7; 

    $jerarquia = "";
    if($jera != '0'){
        $jerarquia = " - JERARQUIA [ ".TRIM($jera)." ]";
    }
    $subjerarquia = "";
    if($subjera != '0'){
        $subjerarquia = " - SUB JERARQUIA [ ".TRIM($subjera)." ]";
    }
    $reportName = "REPORTE DE DIFERENCIAS (".$porcentaje." %) - MONTO FIJADO S/. ".number_format($monto,0)."".$jerarquia."".$subjerarquia;

    include("plantilla_reporte_diferencia_horizontal.php");

    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'L', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->Ln(-9);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 280, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(9);

        $i = 0;
        foreach ($registros as $fila) {

            $i++;
            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, $i, $borde, 0, $alineacion);
            $pdf->Cell( 30, $altoFila, $fila->sku_stk, $borde, 0, $alineacion);
            $pdf->Cell( 30, $altoFila, $fila->barra_cap, $borde, 0, $alineacion);
            $pdf->Cell( 130, $altoFila, $fila->des_sku_stk, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->stock,0), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->contado,0), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->dif_cant,0), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($fila->dif_sol,3), $borde, 0, 'R');
            $pdf->Ln($altoFila);

            //GENERAR DETALLE DE BARRA EN TABLA CAPTURA
            $barra = $fila->sku_stk;
            $dataDetalle = $service->getReporteDiferenciaCapturaDetalle($barra);
            $detalle = $dataDetalle;

            if(count($detalle) > 0){
                foreach ($detalle as $lote) {
                    $detallearea = "";
                    //AQUI LA CONSULTA CON LA UBICACION DEL LOTE
                    foreach ($regarearango as $arearango) {
                        if( ( (float)$arearango->area_ini_ran <= (float)$lote->area_cap ) AND ( (float)$arearango->area_fin_ran >= (float)$lote->area_cap ) ){
                            $detallearea =  $arearango->des_area_ran;
                        }
                    }



                    $pdf->Cell( 5, $altoFila, '', $borde, 0, $alineacion);
                    $pdf->Cell( 30, $altoFila, $lote->sku_stk, $borde, 0, $alineacion);
                    $pdf->Cell( 30, $altoFila, $lote->barra_cap, $borde, 0, $alineacion);
                    $pdf->Cell( 130, $altoFila, 'Lote : '.$lote->area_cap." ==> ".$detallearea, $borde, 0, $alineacion);
                    $pdf->Cell( 20, $altoFila, 'Contado : ', 0, $alineacion);
                    $pdf->Cell( 20, $altoFila, number_format($lote->cant_cap,0), $borde, 0, $alineacion);
                    $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                    $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                    $pdf->Ln($altoFila);
                }

            }

        }

  $pdf->Output( "reporte_diferencia.pdf", "I" );



?>