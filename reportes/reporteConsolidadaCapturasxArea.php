<?php
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");

    $service = new ServiceReportes();

    $areacap = $_GET["area_cap"];
    $data = $service->getListaConsolidadaCapturasxArea($areacap);
    $registros = $data->registros;
    $areas = $data->areas;


    $tamanoLetra = 8;

    $reportName = "REPORTE CONSOLIDADO DE CAPTURAS";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 25, 5, 'LOTE CAP : '.$area->area_cap, 0, 0, 'R' );
        $pdf->Cell( 60, 5, 'CANTIDAD : '.$area->sum_cant, 0, 0, 'C' );
        $pdf->Ln(10);


        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 5, $altoFila, 'N', $borde, 0, $alineacion);
        $pdf->Cell( 25, $altoFila, 'BARRA_CAP', $borde, 0, $alineacion);
        $pdf->Cell( 18, $altoFila, 'SKU_CAP', $borde, 0, $alineacion);
        $pdf->Cell( 90, $altoFila, 'DES_BARRA', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'CANT_CAP', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'TIP_CAP', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, 'USUARIO', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $i = 0;
        foreach ($registros as $fila) {

            if($area->area_cap == $fila->area_cap){
                $i++;
                $pdf->SetFont( 'Arial', '', $tamanoLetra );
                $pdf->Cell( 5, $altoFila, $i, $borde, 0, $alineacion);
                $pdf->Cell( 25, $altoFila, $fila->barra_cap, $borde, 0, $alineacion);
                $pdf->Cell( 18, $altoFila, $fila->sku_cap, $borde, 0, $alineacion);
                $pdf->Cell( 90, $altoFila, $fila->des_barra, $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, $fila->sum_cant, $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, $fila->tip_cap, $borde, 0, $alineacion);
                $pdf->Cell( 40, $altoFila, $fila->nombreUsuario, $borde, 0, $alineacion);
                $pdf->Ln($altoFila);
            }

        }

    }




    $pdf->Output( "reporte_consolidado.pdf", "I" );



?>
