<?php 
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS

    $lotePend = array();

    $service = new ServiceReportes();

    $arearango = $_GET["idAreaRango"];
    $resultado = $service->getListaModalReporteAreaRango($arearango);
    $dataRangos = $resultado->rangos;
    $dataCapturas = $resultado->capturas;
    $dataJustificados = $resultado->justificados;

    $cuentaRangos = count($dataRangos);
    $lotesEncontrados = Array();
    $cuentaFila = 0;

    $tamanoLetra = 8;

    $reportName = "REPORTE LOTES PENDIENTES DE JUSTIFICACION";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);


        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 10, $altoFila, 'N', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'LOTE', $borde, 0, $alineacion);
        $pdf->Cell( 60, $altoFila, 'UBICACION', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);


        for ($y=0; $y<count($dataCapturas); $y++) {
            $valorArea = (int)$dataCapturas[$y]->area_cap;
            $lotesEncontrados[] = $valorArea;
        }

        for ($x=0; $x<count($dataJustificados); $x++) {
            $valorArea = (int)$dataJustificados[$x]->lote;
            $lotesEncontrados[] = $valorArea;
        }

        for($i=0 ; $i < $cuentaRangos ; $i++){

            $filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
            $filaDescripcion = $dataRangos[$i]->des_area_ran;
            $encontrado = 0;

            for ($z=$filaInicio; $z<=$filaFinal; $z++) {
                

                if (in_array($z, $lotesEncontrados)) {
                    //echo "Existe Irix <br>";
                }else{
                    $cuentaFila++;
                    $pdf->SetFont( 'Arial', '', $tamanoLetra );
                    $pdf->Cell( 10, $altoFila, $cuentaFila, $borde, 0, $alineacion);
                    $pdf->Cell( 20, $altoFila, $z, $borde, 0, $alineacion);
                    $pdf->Cell( 60, $altoFila, $filaDescripcion, $borde, 0, $alineacion);
                    $pdf->Ln($altoFila);

                }
                                

            }

        }




    $pdf->Output( "reporte_modal_area_rango.pdf", "I" );



?>
