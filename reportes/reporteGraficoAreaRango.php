<?php 
    include("plantilla_reporte_horizontal.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    //
    //

    $service = new ServiceReportes();

    $resultado = $service->getListaReporteAreaRango();
    $dataRangos = $resultado->rangos;
    $dataCapturas = $resultado->capturas;
    $dataJustificados = $resultado->justificados;

    $cuentaRangos = count($dataRangos);

    $rangos = Array();
    $valores = Array();


    for($i=0 ; $i < $cuentaRangos ; $i++){

        $filaInicio = $dataRangos[$i]->area_ini_ran;
        $filaFinal = $dataRangos[$i]->area_fin_ran;
        $filaDescripcion = $dataRangos[$i]->des_area_ran;

        $cantidad = 0;
        $avance = 0;
        $porcentaje = 0;

        for ($z=$filaInicio; $z<=$filaFinal; $z++) {
            $cantidad++;
            for ($y=0; $y<count($dataCapturas); $y++) {
                $valorArea = (int)$dataCapturas[$y]->area_cap;
                if ($z == $valorArea){
                    $avance++;
                }
            }
            for ($x=0; $x<count($dataJustificados); $x++) {
                $valorArea = (int)$dataJustificados[$x]->lote;
                if ($z == $valorArea){
                    $avance++;
                }
            }
        }

        $porcentaje = round(($avance / $cantidad)*100,2);

        $rangos[] = $filaDescripcion;
        $valores[] = array($porcentaje);


    }



    $tamanoLetra = 8;

    $reportName = "GRAFICO AVANCE DE CAPTURAS";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'L', 'mm', 'A4' );

    //foreach ($areas as $area) {


    $pdf->AddPage();
    $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
    $pdf->Cell( 280, 5, $reportName, 0, 0, 'C' );
    $pdf->Ln(10);

    $pdf->SetFont( 'Arial', 'B', $tamanoLetra );

    $rowLabels = $rangos;
    $chartXPos = 0;
    $chartYPos = 160;
    $chartWidth = 270;
    $chartHeight = 100;
    $chartXLabel = "RANGOS";
    $chartYLabel = "%";
    $chartYStep = 10;

    $data = $valores ;



    /***
      Create the chart
    ***/

    // Compute the X scale
    $xScale = count($rowLabels) / ( $chartWidth - 40 );

    // Compute the Y scale

    $maxTotal = 0;

    foreach ( $data as $dataRow ) {
      $totalSales = 0;
      foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;
      $maxTotal = ( $totalSales > $maxTotal ) ? $totalSales : $maxTotal;
    }

    $yScale = $maxTotal / $chartHeight;

    // Compute the bar width
    $barWidth = ( 1 / $xScale ) / 1.5;

    // Add the axes:

    $pdf->SetFont( 'Arial', '', 8 );

    // X axis
    $pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + $chartWidth, $chartYPos );

    for ( $i=0; $i < count( $rowLabels ); $i++ ) {
      $pdf->SetXY( $chartXPos + 40 +  $i / $xScale, $chartYPos );
      $s=sprintf('BT %.2F %.2F %.2F %.2F %.2F %.2F Tm (%s) Tj ET',0,1,-1,0,$barWidth,10,$rowLabels[$i]);
      $pdf->Cell( $barWidth, 10, $rowLabels[$i], 0, 0, 'C' );
      //$pdf->TextWithDirection(10, 10, $rowLabels[$i] ,'U');
    }

    // Y axis
    $pdf->Line( $chartXPos + 30, $chartYPos, $chartXPos + 30, $chartYPos - $chartHeight - 8 );

    for ( $i=0; $i <= $maxTotal; $i += $chartYStep ) {
      $pdf->SetXY( $chartXPos + 7, $chartYPos - 5 - $i / $yScale );
      $pdf->Cell( 20, 10, '' . number_format( $i ), 0, 0, 'R' );
      $pdf->Line( $chartXPos + 28, $chartYPos - $i / $yScale, $chartXPos + 30, $chartYPos - $i / $yScale );
    }

    // Add the axis labels
    $pdf->SetFont( 'Arial', 'B', 8 );
    $pdf->SetXY( $chartWidth / 2 + 20, $chartYPos + 8 );
    $pdf->Cell( 30, 10, $chartXLabel, 0, 0, 'C' );
    $pdf->SetXY( $chartXPos + 7, $chartYPos - $chartHeight - 12 );
    $pdf->Cell( 20, 10, $chartYLabel, 0, 0, 'R' );

    // Create the bars
    $xPos = $chartXPos + 40;
    $bar = 0;

    foreach ( $data as $dataRow ) {

      // Total up the sales figures for this product
      $totalSales = 0;
      foreach ( $dataRow as $dataCell ) $totalSales += $dataCell;

      // Create the bar
      $colourIndex = $bar % count( $chartColours );
      $pdf->SetFillColor( 232, 232, 232 );
      $pdf->Rect( $xPos, $chartYPos - ( $totalSales / $yScale ), $barWidth, $totalSales / $yScale, 'DF' );
      $xPos += ( 1 / $xScale );
      $bar++;
    }





    $pdf->Output( "reporte_grafico_area_rango.pdf", "I" );



?>
