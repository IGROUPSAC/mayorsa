<?php
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-d");

    $service = new ServiceReportes();

    $data = $service->getTotalContado();

    $registros_contados = $data->contados;
    $registros_eliminados = $data->eliminados;
    $registros_editados = $data->editados;
    $registros_eliminados_cliente = $data->eliminados_cliente;
    $registros_editados_cliente = $data->editados_cliente;
    //$registros_positivos = $data->positivos;

    $porcentaje = $service->getPorcentajeAvance();

    $tamanoLetra = 7;

    $reportName = "REPORTE TOTAL CONTADO (".$porcentaje." %)";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'DESCRIPCION', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'CONTADO', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'VALOR', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'TOTAL CONTADO', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, number_format($registros_contados[0]->contado,3), $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'S/.'.number_format($registros_contados[0]->valorado,3), $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'TOTAL MODIFICACIONES', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, number_format( ( $registros_editados[0]->contado + $registros_eliminados[0]->contado ) ,3), $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'S/.'.number_format( ( $registros_editados[0]->valorado + $registros_eliminados[0]->valorado ) ,3), $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'TOTAL MODIFICACIONES CLIENTE', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, number_format( ( $registros_editados_cliente[0]->contado + $registros_eliminados_cliente[0]->contado ) ,3), $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'S/.'.number_format( ( $registros_editados_cliente[0]->valorado + $registros_eliminados_cliente[0]->valorado ) ,3), $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'TOTAL GENERAL', $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, number_format( ( $registros_contados[0]->contado + $registros_editados_cliente[0]->contado + $registros_eliminados_cliente[0]->contado ) ,3), $borde, 0, $alineacion);
        $pdf->Cell( 50, $altoFila, 'S/.'.number_format( ( $registros_contados[0]->valorado + $registros_editados_cliente[0]->valorado + $registros_eliminados_cliente[0]->valorado ) ,3), $borde, 0, $alineacion);
        $pdf->Ln(40);





        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, '--------------------------------------------', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, '', $borde, 0, $alineacion);
        $pdf->Cell( 80, $altoFila, '--------------------------------------------', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);  
        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'Nombres :', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, '', $borde, 0, $alineacion);
        $pdf->Cell( 80, $altoFila, 'Nombres :', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);  
        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'Dni : ', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, '', $borde, 0, $alineacion);
        $pdf->Cell( 80, $altoFila, 'Dni : ', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);  
        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 80, $altoFila, 'SUPERVISOR IGROUP S.A.C.', $borde, 0, $alineacion);
        $pdf->Cell( 40, $altoFila, '', $borde, 0, $alineacion);
        $pdf->Cell( 80, $altoFila, 'CONTROL EXISTENCIAS TOTTUS.', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);  



  $pdf->Output( "reporte_total_contado.pdf", "I" );


?>