<?php
    include("plantilla_informe.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");

    $service = new ServiceReportes();

    $data = $service->getDatosInforme(1);
    $informe = $data[0];

    $resultadoTienda = $service->getListaReporteTienda();
    $numeroTienda = $resultadoTienda[0]->numeroTienda;
    $nombreTienda = $resultadoTienda[0]->nombreTienda;

    $tamanoLetra = 8;


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', '', $tamanoLetra );
        $pdf->Cell( 150, $altoFila, '', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 15, $altoFila, 'FECHA : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 10, $altoFila, $informe->fecha, $borde, 0, $alineacion);
        $pdf->Ln(5);
        $pdf->Cell( 150, $altoFila, '', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 15, $altoFila, 'N TIENDA : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 10, $altoFila, $numeroTienda, $borde, 0, $alineacion);
        $pdf->Ln(6);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, 'RESPONSABLE IGROUP : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 15, $altoFila, $informe->responsableIgroup, $borde, 0, $alineacion);
        $pdf->Ln(5);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, 'RESPONSABLE CLIENTE : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 15, $altoFila, $informe->responsableCliente, $borde, 0, $alineacion);
        $pdf->Ln(5);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, 'TIENDA/LOCAL : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 15, $altoFila, $nombreTienda, $borde, 0, $alineacion);
        $pdf->Ln(9);

        $pdf->SetFont( 'Arial', 'B', 10 );
        $pdf->Cell( 40, $altoFila, 'INVENTARIO - ALMACEN/BODEGA', $borde, 0, $alineacion);
        $pdf->Ln(9);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 40, $altoFila, 'ENCARGADO CLIENTE : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 90, $altoFila, $informe->encargadoCliente, $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, 'INICIO CONTEO : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, $informe->inicioConteo, $borde, 0, $alineacion);
        $pdf->Ln(5);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, 'SUPERVISOR IGROUP : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 90, $altoFila, $informe->supervisorIgroup, $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', 'B', $tamanoLetra);
        $pdf->Cell( 30, $altoFila, 'FIN DE CONTEO : ', $borde, 0, 'R');
        $pdf->SetFont( 'Arial', '', $tamanoLetra);
        $pdf->Cell( 40, $altoFila, $informe->finConteo, $borde, 0, $alineacion);
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', 8);
        $pdf->Cell( 40, $altoFila, utf8_decode('1. PREPARACIÓN DE INVENTARIO'), $borde, 0, $alineacion);
        $pdf->Ln(6);
        $pdf->Cell( 170, $altoFila, utf8_decode('DESCRIPCIÓN'), 1, 0,'C');
        $pdf->Cell( 20, $altoFila, utf8_decode('RESPUESTA'), 1, 0, 'C');
        $pdf->Ln($altoFila);

        $data = $service->getPreguntasCuestionario(1,1);
        $registros = $data;
        $pdf->SetFont( 'Arial', '', $tamanoLetra );

        foreach ($registros as $fila) {

            $i++;
            $x = $pdf->GetX();
            $y = $pdf->GetY();
            $pdf->MultiCell(170,$altoFila,utf8_decode($fila->pregunta),1,'LRB');
            $x += 170;
            $pdf->SetXY($x, $y);
            $pdf->MultiCell(20,$altoFila,utf8_decode($fila->respuesta),1,'C');
            $pdf->Ln($altoFila);

        }

        $pdf->Ln(1);
        $pdf->SetFont( 'Arial', 'B', 8);
        $pdf->Cell( 40, $altoFila, '2. PROCEDO DE CONTEO - INVENTARIO', $borde, 0, $alineacion);
        $pdf->Ln(6);
        $pdf->Cell( 170, $altoFila, utf8_decode('DESCRIPCIÓN'), 1, 0,'C');
        $pdf->Cell( 20, $altoFila, utf8_decode('RESPUESTA'), 1, 0, 'C');
        $pdf->Ln($altoFila);

        $data = $service->getPreguntasCuestionario(1,2);
        $registros = $data;
        $pdf->SetFont( 'Arial', '', $tamanoLetra );

        foreach ($registros as $fila) {

            $i++;
            $x = $pdf->GetX();
            $y = $pdf->GetY();
            $pdf->MultiCell(170,$altoFila,utf8_decode($fila->pregunta),1,'LRB');
            $x += 170;
            $pdf->SetXY($x, $y);
            $pdf->MultiCell(20,$altoFila,utf8_decode($fila->respuesta),1,'C');
            $pdf->Ln($altoFila);

        }


        $pdf->Ln(1);
        $pdf->SetFont( 'Arial', 'B', 8);
        $pdf->Cell( 40, $altoFila, 'OBSERVACIONES : ', $borde, 0, $alineacion);
        $pdf->SetFont( 'Arial', '', 8);
        $pdf->Ln(5);
        $pdf->MultiCell( 190, $altoFila, $informe->observaciones,0,'LRB');
        $pdf->Ln(5);


        $pdf->SetFont( 'Arial', 'B', 8);

        $pdf->SetXY(20, 268);
        $pdf->MultiCell(60,$altoFila,"_________________________________",0,'C');
        $pdf->SetXY(20, 272);
        $pdf->MultiCell(60,$altoFila,"ENCARGADO CLIENTE",0,'C');

        $pdf->SetXY(130, 268);
        $pdf->MultiCell(60,$altoFila,"_________________________________",0,'C');
        $pdf->SetXY(130, 272);
        $pdf->MultiCell(60,$altoFila,"SUPERVISOR IGROUP",0,'C');

  $pdf->Output( "reporte_almacen_bodega.pdf", "I" );



?>