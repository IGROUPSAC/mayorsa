<?php
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS
    $hora = date("h:i:s");
    $fecha = date("Y-m-j");
    $unitcli=$solescli=$unitigroup=$solesigroup=$difunit=$difsol=0.000;

    $service = new ServiceReportes();

    $data = $service->getReporteDiferenciaGrupos();
    $registros = $data;

    $porcentaje = $service->getPorcentajeAvance();

    $tamanoLetra = 7;

    $reportName = "REPORTE CONSOLIDADO DE DIFERENCIAS POR GRUPOS (".$porcentaje." %)";


    $borde = 0;
    $alineacion = "R";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 5, $altoFila, 'N', $borde, 0, "L");
        $pdf->Cell( 25, $altoFila, 'JERARQUIAS', $borde, 0, "L");
        $pdf->Cell( 45, $altoFila, 'DIVISION', $borde, 0, "L");
        $pdf->Cell( 20, $altoFila, 'UNIT CLI', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'SOLES CLI', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'UNIT IGROUP', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'SOLES IGROUP', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'DIF UNIT', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'DIF SOL', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);

        $i = 0;
        //foreach ($registros as $fila) {

        for($i=0;$i<count($registros);$i++) {
            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, ($i+1), $borde, 0, "L");
            $pdf->Cell( 25, $altoFila,$registros[$i]->jerarquias, $borde, 0, "L");
            $pdf->Cell( 45, $altoFila, $registros[$i]->division, $borde, 0, "L");
            $pdf->Cell( 20, $altoFila, number_format($registros[$i]->unidades_cliente,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($registros[$i]->soles_cliente,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($registros[$i]->unidades_igroup,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($registros[$i]->soles_igroup,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($registros[$i]->diferencia_unidades,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($registros[$i]->diferencia_soles,3), $borde, 0, $alineacion);
            $pdf->Ln($altoFila);

            $unitcli = $unitcli + $registros[$i]->unidades_cliente;
            $solescli = $solescli + $registros[$i]->soles_cliente;
            $unitigroup = $unitigroup + $registros[$i]->unidades_igroup;
            $solesigroup = $solesigroup + $registros[$i]->soles_igroup;
            $difunit = $difunit + $registros[$i]->diferencia_unidades;
            $difsol = $difsol + $registros[$i]->diferencia_soles;

            $stunitcli = $stunitcli + $registros[$i]->unidades_cliente;
            $stsolescli = $stsolescli + $registros[$i]->soles_cliente;
            $stunitigroup = $stunitigroup + $registros[$i]->unidades_igroup;
            $stsolesigroup = $stsolesigroup + $registros[$i]->soles_igroup;
            $stdifunit = $stdifunit + $registros[$i]->diferencia_unidades;
            $stdifsol = $stdifsol + $registros[$i]->diferencia_soles;

            if($registros[$i]->jerarquias != $registros[($i+1)]->jerarquias){
                $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
                $pdf->Cell( 5, $altoFila, '', $borde, 0, "L");
                $pdf->Cell( 25, $altoFila, '', $borde, 0, "L");
                $pdf->Cell( 45, $altoFila, 'SUB TOTAL', $borde, 0, "L");
                $pdf->Cell( 20, $altoFila, number_format($stunitcli,3), $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, number_format($stsolescli,3), $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, number_format($stunitigroup,3), $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, number_format($stsolesigroup,3), $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, number_format($stdifunit,3), $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, number_format($stdifsol,3), $borde, 0, $alineacion);
                $pdf->Ln($altoFila);

                $stunitcli = 0.00;
                $stsolescli = 0.00;
                $stunitigroup = 0.00;
                $stsolesigroup = 0.00;
                $stdifunit = 0.00;
                $stdifsol = 0.00;

            }

        }

            $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
            $pdf->Cell( 5, $altoFila, '', $borde, 0, "L");
            $pdf->Cell( 25, $altoFila, '', $borde, 0, "L");
            $pdf->Cell( 45, $altoFila, 'TOTALES', $borde, 0, "L");
            $pdf->Cell( 20, $altoFila, number_format($unitcli,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($solescli,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($unitigroup,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($solesigroup,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($difunit,3), $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, number_format($difsol,3), $borde, 0, $alineacion);
            $pdf->Ln($altoFila);



  $pdf->Output( "reporte_diferencia_grupos.pdf", "I" );



?>