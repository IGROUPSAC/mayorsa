                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="area_rango">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO DE AREA RANGO
                        </div>
                        <div class="panel-body">

                           

                                                    <br>
                                                        <div class="row contenedor opcionesAreaRango">
                                                            <div class="col-md-1 texto negrita derecha">LOTE INICIAL</div>
                                                            <div class="col-md-1">
                                                                <input type="number" id="rangoInicioFormularioAreaRango" placeholder="DESDE" class="form-control input-sm"></input>
                                                                <input type="hidden" id="codigoFormularioAreaRango"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">LOTE FINAL</div>
                                                            <div class="col-md-1">
                                                                <input type="number" id="rangoFinalFormularioAreaRango" placeholder="HASTA" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">DESCRIPCION</div>
                                                            <div class="col-md-4">
                                                                <textarea rows="1" id="descripcionFormularioAreaRango" name="buscar_descripcion" placeholder="DESCRIPCION" class="form-control input-sm"></textarea>
                                                            </div>
                                                            
                                                            <div class="col-md-1"><button id="botonGuardarAreaRango" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button></div>
                                                            <div class="col-md-2"><button id="botonListarImprimirAreaRango" type="button" class="btn btn-default btn-sm btn-block"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> IMPRIMIR REPORTE</button></div>

                                                        </div>
                                                        <div class="row contenedor opcionesAreaRango">
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-1 texto negrita derecha">UBICACION</div>
                                                            <div class="col-md-2">
                                                                <select id="ubicacionFormularioAreaRango" class="form-control input-sm">
                                                                 <option value='P'>PISO DE VENTA</option>
                                                                 <option value='B'>BODEGA</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1"><button id="botonCancelarAreaRango" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button></div>
                                                            <div class="col-md-2"><button id="botonListarPendientesImprimirAreaRango" type="button" class="btn btn-default btn-sm btn-block"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> IMPRIMIR PENDIENTES</button></div>
                                                        </div>

                                                        <div class="row contenedor opcionesAreaRango">
                                                            <div class="col-md-12"><button id="botonAlertaAreaRango" type="button" class="btn btn-warning btn-sm btn-block parpadea" style="display: none;">EXISTEN LOTES LEIDOS PERO NO REGISTRADOS</button>
                                                            </div>
                                                        </div>

                                                        <div class="row contenedor opcionesAreaRangoMasivo" style="display: none;">
                                                            <div class="col-md-5"></div>
                                                            <div class="col-md-2"><button id="botonEliminarMasivoFormularioAreaRango" type="button" class="btn btn-danger btn-sm btn-block">ELIMINAR SELECCIONADOS</button></div>
                                                            <div class="col-md-5"></div>
                                                        </div>

                                                        <br>
                                                        <table id="tablaResultadoFormularioAreaRango" width="100%" class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th width="3%">#</th>
                                                                    <th width="7%">LOTE INICIAL</th>
                                                                    <th width="7%">LOTE FINAL</th>
                                                                    <th width="10%">DESCRIPCION</th>
                                                                    <th width="5%">UBIC</th>
                                                                    <th width="8%">FECHA</th>
                                                                    <th width="5%">USUARIO</th>
                                                                    <th width="5%">CANTIDAD</th>
                                                                    <th width="5%">AVANCE</th>
                                                                    <th width="3%">%</th>
                                                                    <th width="5%">SELECCIONAR</th>
                                                                    <th width="7%">IMPORTANTE</th>
                                                                    <th width="15%">OPCIONES</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                </tr>                                              
                                                            </tbody>
                                                        </table>

                                                    
                                                             



                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    GRAFICO
                                </div>
                                <div class="panel-body">
                                    <button id="imprimirGraficoAvanceCaptura" type="button" class="btn btn-default btn-sm">IMPRIMIR GRAFICO</button>
                                    <button id="descargarGraficoAvanceCaptura" type="button" class="btn btn-success btn-sm">DESCARGAR GRAFICO A EXCEL</button>
                                    <div id="greportes" style="height: 900px; width: 99%">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>


                </div>
            </div>

