                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="archivos_reenviados">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO DE CARGA ARCHIVOS REENVIADOS PENDIENTES
                        </div>
                        <div class="panel-body">

                            <ul class="nav nav-tabs nav-justified">
                                <li class="active"><a href="#reenviados_pendientes" data-toggle="tab" aria-expanded="false">REENVIADOS PENDIENTES DE CARGA</a>
                                </li>
                                <li><a href="#reenviados_cargados" data-toggle="tab" aria-expanded="false">REENVIADOS CARGADOS</a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <br>
                                <div class="tab-pane fade active in" id="reenviados_pendientes">

                                    <div class="row">
                                        <div class="col-md-12 texto negrita">
                                            ARCHIVOS REENVIADOS PENDIENTES DE CARGA
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tablaListaArchivosReenviados" width="100%" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">#</th>
                                                        <th width="55%">ARCHIVO</th>
                                                        <th width="10%">FECHA</th>
                                                        <th width="10%">TAMAÑO</th>
                                                        <th width="20%">OPCIONES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    </tr>                                              
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                                <div class="tab-pane fade" id="reenviados_cargados">

                                    <div class="row">
                                        <div class="col-md-12 texto negrita">
                                            ARCHIVOS REENVIADOS CARGADOS
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="tablaListaArchivosReenviadosCargados" width="100%" class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th width="5%">#</th>
                                                        <th width="55%">ARCHIVO</th>
                                                        <th width="10%">REGISTROS</th>
                                                        <th width="10%">CONTADOS</th>
                                                        <th width="20%">OPCIONES</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                    </tr>                                              
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <br>
                            <br>                                                        



                        </div>
                    </div>
                </div>

