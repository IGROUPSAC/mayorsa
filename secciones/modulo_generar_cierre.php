                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="generar_cierre">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO DE REPORTES
                        </div>
                        <div class="panel-body">
                            <br>
                            
                            <!--    <div class="row">
                                    <div class="col-md-12">
                                    <button type="button" class="btn btn-default btn-block btn-sm generarcierre">GENERAR CIERRE</button>
                                    </div>
                                </div>
                            <br>
                            <br>-->
                            <div class="row">
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportegrupos">SECCIONES X JERARQUIAS</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportejerarquico">REPORTE JERARQUICO</button>
                                </div>
                                 <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportesubjerarquico">REPORTE SUBJERARQUICO</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-warning btn-block btn-sm mostrargraficoreportejerarquico">GRAFICO JERARQUICO</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm excelgraficoreportejerarquico">EXCEL REPORTE JERARQUICO</button>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-1 texto negrita derecha">JERARQUIA</div>
                                <div class="col-md-3">
                                    <select class="form-control input-sm listadojerarquias">
                                        <option value='0'>---NINGUNO---</option>
                                    </select>
                                </div>
                                <div class="col-md-1 texto negrita derecha">MONTO</div>
                                <div class="col-md-1">
                                    <input type="number" placeholder="MONTO" class="form-control input-sm montoreportediferencias"></input>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportediferencias">REPORTE DE DIFERENCIAS</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm excelgraficoreportediferencias">EXCEL REPORTE DIFERENCIAS</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm dercargarreportediferenciastotal">EXCEL TOTAL DIFERENCIAS</button>
                                </div>
                            </div>
                            <div class="row subjerarquia">
                                <div class="col-md-1 texto negrita derecha">SUB JERARQUIA</div>
                                <div class="col-md-3">
                                    <select class="form-control input-sm listadosubjerarquias">
                                        <option value='0'>---NINGUNO---</option>
                                    </select>
                                </div>
                            </div>
                            <br>
                            <br>
                            <div class="row">
                                <div class="col-md-5 texto negrita derecha">REPORTE POR SKU CON UN LIMITE DE REGISTROS</div>
                                <div class="col-md-1">
                                    <input type="number" placeholder="LIMITE" class="form-control input-sm limitereportediferencias"></input>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportediferenciassku">REPORTE DIFERENCIAS SKU</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm excelreportediferencias">EXCEL REPORTE DIFERENCIAS</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm excelgraficoreportediferenciassku">EXCEL REPORTE DIFERENCIAS SKU</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 texto negrita derecha">PRODUCTIVIDAD</div>
                                <!--<div class="col-md-2">
                                    <button type="button" class="btn btn-default btn-block btn-sm generarReporteProductividad">GENERAR REPORTE</button>
                                </div>
                                <div class="col-md-2">
                                </div>-->
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm imprimirReporteProductividad">IMPRIMIR REPORTE</button>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm imprimirReporteProductividadActualizado">IMPRIMIR REPORTE ACT</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 texto negrita derecha">TOTAL CONTADO</div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportetotalcontado">IMPRIMIR REPORTE</button>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-md-6 texto negrita derecha">REPORTE LOTIZACION</div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm excelreportelotizacion">DESCARGAR EXCEL</button>
                                </div>
                            </div>

                            <br>
                            <div class="row">
                                <div class="col-md-6 texto negrita derecha">REPORTE GRAFICO</div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-success btn-block btn-sm excelreportegrafico">DESCARGAR EXCEL</button>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

