                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="tienda">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO DE TIENDA
                        </div>
                        <div class="panel-body">

                           

                                                    <br>
                                                        <div class="row contenedor opcionestienda">
                                                            <div class="col-md-1 texto negrita derecha">NUMERO TIENDA</div>
                                                            <div class="col-md-1">
                                                                <input type="number" id="numeroFormularioTienda" placeholder="NUMERO" class="form-control input-sm"></input>
                                                                <input type="hidden" id="codigoFormularioTienda"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">TIENDA</div>
                                                            <div class="col-md-3">
                                                                <input id="nombreFormularioTienda" placeholder="NOMBRE COMPLETO" class="form-control input-sm"></input>
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">FECHA</div>
                                                            <div class="col-md-2">
                                                                <div class='input-group date' id='datetimepickerFechaTienda'>
                                                                    <input type='text' id="fechaFormularioTienda" class="form-control input-sm"></input>
                                                                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1"><button id="botonGuardarTienda" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button></div>
                                                            <div class="col-md-1">
                                                                <!--
                                                                <button id="botonListarImprimirTienda" type="button" class="btn btn-default btn-sm btn-block"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> IMPRIMIR</button>
                                                                -->
                                                            </div>

                                                        </div>
                                                        <div class="row contenedor opcionestienda">
                                                            <div class="col-md-1 texto negrita derecha"></div>
                                                            <div class="col-md-1">
                                                            </div>
                                                            <div class="col-md-1 texto negrita derecha">CLIENTE</div>
                                                            <div class="col-md-3">
                                                                <select id="idCliente" class="form-control input-sm">
                                                                    <option value=''>---SELECCIONAR---</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-4"></div>
                                                            <div class="col-md-1"><button id="botonCancelarTienda" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button></div>
                                                            <div class="col-md-1"></div>
                                                        </div>

                                                        <div class="row contenedor opcionestiendamasivo" style="display: none;">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>

                                                        <div class="row contenedor opcionestiendamasivo" style="display: none;">
                                                            <div class="col-md-5"></div>
                                                            <div class="col-md-2"><button id="botonEliminarMasivoFormularioTienda" type="button" class="btn btn-danger btn-sm btn-block">ELIMINAR SELECCIONADOS</button></div>
                                                            <div class="col-md-5"></div>
                                                        </div>

                                                        <br>
                                                        <table id="tablaResultadoFormularioTienda" width="100%" class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th width="5%">#</th>
                                                                    <th width="10%">NUMERO</th>
                                                                    <th width="35%">TIENDA</th>
                                                                    <th width="20%">CLIENTE</th>
                                                                    <th width="10%">FECHA</th>
                                                                    <th width="5%">MARCAR</th>
                                                                    <th width="15%">OPCIONES</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                </tr>                                              
                                                            </tbody>
                                                        </table>

                                                    <br>
                                                    <br>

                                                              



                    </div>
                </div>
            </div>

