                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="archivo_barra">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO ARCHIVO BARRA
                        </div>
                        <div class="panel-body">

                           

                            <br>
                            <div class="row contenedor">
                                <div class="col-md-12">
                                    <input type='file' id="archivoBarra" style='visibility:hidden; height:0'>
                                    <div class="form-group">
                                        <div class="input-group" name="FicheroBarra">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary boton-carga-barra btn-sm" type="button">BUSCAR ARCHIVO BARRA</button>
                                            </span>
                                            <input type="number" class="form-control campo-carga-barra input-sm" placeholder='NOMBRE ARCHIVO BARRA...' />
                                            <span class="input-group-btn">
                                                <button class="btn btn-default btn-sm" type="button" disabled>INGRESAR LOTE</button>
                                            </span>
                                            <input type="text" class="form-control campo-carga-lote input-sm" placeholder='INGRESAR LOTE...' />
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger boton-limpia-barra btn-sm" type="button">RESETEAR</button>
                                                <button class="btn btn-success submit-carga-barra btn-sm" type="button" disabled>CARGAR ARCHIVO</button>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="row contenedor progresoCargandoBarra" style="display:none">
                                <div class="col-md-12">
                                    <div class="progress">
                                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">CARGANDO REGISTROS A LA BASE DE DATOS</div>
                                    </div>                                
                                </div>
                            </div>

                            <table id="tablaListaArchivosBarraPendientes" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="55%">ARCHIVO</th>
                                        <th width="10%">FECHA</th>
                                        <th width="10%">TAMAÑO</th>
                                        <th width="20%">OPCIONES</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                    </tr>                                              
                                </tbody>
                            </table>

                            <br>                                                              



                    </div>
                </div>
            </div>

