                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="generar_archivos">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO PARA GENERAR ARCHIVOS DE MODULOS
                        </div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="panel panel-default usuarios">
                                        <div class="panel-heading">
                                            USUARIOS
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-info btn-sm btn-block generararchivo">GENERAR ARCHIVO</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargararchivo">DESARGAR ARCHIVO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="panel panel-default maestro">
                                        <div class="panel-heading">
                                            MAESTRO
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-info btn-sm btn-block generararchivo">GENERAR ARCHIVO</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargararchivo">DESARGAR ARCHIVO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="col-md-4">

                                    <div class="panel panel-default maestro-detalle">
                                        <div class="panel-heading">
                                            MAESTRO COMPLETO
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-info btn-sm btn-block generararchivo">GENERAR ARCHIVO</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargararchivo">DESARGAR ARCHIVO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>                        


                            <br><br>


                            <div class="row">
                                <div class="col-md-4">

                                    <div class="panel panel-default archivo-tottus-1">
                                        <div class="panel-heading">
                                            ARCHIVO CARGA TOTTUS
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-info btn-sm btn-block generararchivo">GENERAR ARCHIVO</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargararchivo">DESARGAR ARCHIVO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="col-md-4">

                                    <div class="panel panel-default archivo-tottus-2">
                                        <div class="panel-heading">
                                            ARCHIVO BODEGA - PISO DE VENTA
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-info btn-sm btn-block generararchivo">GENERAR ARCHIVO</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargararchivo">DESARGAR ARCHIVO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                <div class="col-md-4">

                                    <div class="panel panel-default archivo-estructura">
                                        <div class="panel-heading">
                                            ARCHIVO CLIENTE ESTRUCTURA
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-info btn-sm btn-block generararchivo">GENERAR ARCHIVO</button>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita">TAMAÑO</div>
                                                <div class="col-md-3 texto peso">0</div>
                                                <div class="col-md-3 texto negrita">REGISTROS</div>
                                                <div class="col-md-3 texto filas">0</div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3 texto negrita"></div>
                                                <div class="col-md-3 texto"></div>
                                                <div class="col-md-3 texto negrita">FECHA</div>
                                                <div class="col-md-3 texto fecha"></div>
                                            </div>
                                            <br>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <button type="button" class="btn btn-success btn-sm btn-block descargararchivo">DESARGAR ARCHIVO</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                            </div>   


                                                              



                    </div>
                </div>
            </div>

