<div class="modal fade" tabindex="-1" role="dialog" id="modal_cerrar_inventario_progress" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="list-group">
                    <br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active descripcionarchivogenerado" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">GENERANDO ARCHIVO</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalCerrarInventario">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header modal-danger">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">ELIMINAR BASE PARA NUEVO INVENTARIO</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">

                    <div class="row">
                        <div class="row">
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-danger btn-block botonesmenu btn-sm si">SI</button>
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-primary btn-block botonesmenu btn-sm" data-dismiss="modal">NO</button>
                            </div>
                            <div class="col-md-2">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer modal-danger">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalRestaurarInventario">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">RESTAURAR TABLAS DE LA BASE DE DATOS</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">

                            <div class="row contenedor">
                                <div class="col-md-12">
                                    <input type='file' id="archivoBackup" style='visibility:hidden; height:0'>
                                    <div class="form-group">
                                        <div class="input-group" name="FicheroRestaurar">
                                            <span class="input-group-btn">
                                                <button class="btn btn-primary boton-carga-backup btn-sm" type="button">BUSCAR ARCHIVO BACKUP</button>
                                            </span>
                                            <input type="text" class="form-control campo-carga-backup input-sm" placeholder='NOMBRE ARCHIVO BACKUP...' />
                                            <span class="input-group-btn">
                                                <button class="btn btn-danger boton-limpia-backup btn-sm" type="button">RESETEAR</button>
                                                <button class="btn btn-success submit-carga-backup btn-sm" type="button" disabled>CARGAR ARCHIVO</button>
                                            </span>
                                        </div>
                                    </div>

                                </div>
                            </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->