<div class="modal fade" tabindex="-1" role="dialog" id="modal_area_rango">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="row">
                    <div class="col-md-2"><img src="img/logoReporte.png" height="39" width="87"></div>
                    <div class="col-md-8 texto negrita centro"><h2 class="modal-title">JUSTIFICAR LOTE DE AREA RANGO</h2></div>
                    <div class="col-md-2"></div>
                </div>
            </div>
            <div class="modal-body">
                <div class="list-group">
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" class="form-control input-sm"></input>
                            <button id="imprimirJustificacionFormularioAreaRango" type="button" class="btn btn-info btn-sm">IMPRIMIR PENDIENTES DE JUSTIFICACION</button>
                        </div>
                        <br><br>
                        <div class="col-md-12">
                            
                            <table id="tablaModalJustificacionFormularioAreaRango" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="10%">LOTE</th>
                                        <th width="30%">UBICACION</th>
                                        <th width="20%">JUSTIFICACION</th>
                                        <th width="35%">DESCRIPCION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->


<div class="modal fade" tabindex="-1" role="dialog" id="modal_area_rango_confirmacion">
    <div class="modal-dialog  modal-sm" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">JUSTIFICAR AREA SIN CONTEO</h4>
            </div>-->
            <div class="modal-body">
                <div class="list-group">

                    <div class="row">
                        <div class="col-md-4 texto negrita">LOTE</div>
                        <div class="col-md-8"><label class="form-control input-sm centro"></label></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-4 texto negrita">TIPO</div>
                        <div class="col-md-8">
                            <select class="form-control input-sm valorJustificacion">
                                <option value="0">---SELECCIONAR---</option>
                                <option value="COMBINADO">COMBINADO</option>
                                <option value="VACIO">VACIO</option>
                                <option value="EXTRAVIADO">EXTRAVIADO</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 texto negrita">
                            JUSTIFICACIÓN
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" class="form-control input-sm"></input>
                            <textarea class="form-control" rows="3" placeholder="ESCRIBIR AQUI"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <button id="boton_guardar_modal_area_rango_confirmacion" type="button" class="btn btn-success btn-block">GUARDAR</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">CERRAR</button>
                        </div>
                    </div>

                </div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_area_rango_no_validas">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">JUSTIFICAR AREA SIN CONTEO</h4>
            </div>-->
            <div class="modal-body">
                <div class="list-group">
                    
                    <div class="row">
                        <div class="col-md-2"><img src="img/logoReporte.png" height="39" width="87"></div>
                        <div class="col-md-8 texto negrita centro"><h2 class="modal-title">LOTES CAPTURADOS PERO NO REGISTRADOS EN AREA RANGO</h2></div>
                        <div class="col-md-2"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            
                            <table id="tablaModalNoValidasFormularioAreaRango" width="100%" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th>AREA_CAP</th>

                                        <th>CANT_CAP</th>

                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
