<div class="modal fade" tabindex="-1" role="dialog" id="modal_imprimir_consolidada_capturas">
    <div class="modal-dialog  modal-sm" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">JUSTIFICAR AREA SIN CONTEO</h4>
            </div>-->
            <div class="modal-body">
                <div class="list-group">

                    <div class="row">
                        <div class="col-md-12 texto negrita">
                            IMPRIMIR AREA(S)
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input type="hidden" class="form-control input-sm"></input>
                            <textarea class="form-control" rows="3" placeholder="LISTAR AREAS A IMPRIMIR"></textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <button id="botonListarImprimirCapturas" type="button" class="btn btn-default btn-block">IMPRIMIR</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-success btn-block exportarCapturas" class="btn btn-default btn-block">EXPORTAR</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-danger btn-block" data-dismiss="modal">CERRAR</button>
                        </div>
                    </div>

                </div>
            </div>
            <!--<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>-->
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->