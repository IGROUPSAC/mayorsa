<div class="modal fade" tabindex="-1" role="dialog" id="modal_cierre_inventario">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">COMPARAR INVENTARIO - ADMINISTRADOR DE SISTEMA</h4>
            </div>
            <div class="modal-body">
                <div class="list-group">

                    <div class="row">
                        <div class="row">
                            <div class="col-md-12">
                                <button type="button" class="btn btn-primary btn-block btn-sm generarcierre">CRUZAR INVENTARIO</button>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12 texto negrita centro">
                                CIERRE GENERADO POR ULTIMA VEZ CON <p class="fecha"></p>
                            </div>
                        </div>
                    </div>
                    <BR><BR>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-12 texto negrita centro">
                                <button type="button" class="btn btn-success btn-block btn-sm sincronizarInventario">ENVIAR AVANCE AL SISTEMA WEB</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_cierre_progress" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="list-group">
                    <br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active descripcionarchivogenerado" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">GENERANDO CIERRE</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->